adminlte - 3.0.5 https://github.com/ColorlibHQ/AdminLTE

bootstrap - 4.4.1
bootstrap color picker 3.2 https://github.com/itsjavi/bootstrap-colorpicker
bootstrap slider - 10.6.2 https://github.com/seiyria/bootstrap-slider/
bootstrap switch - 3.3.4 https://github.com/Bttstrp/bootstrap-switch
bootstrap dualistbox - 4.0.1 https://github.com/istvan-ujjmeszaros/bootstrap-duallistbox
bootstrap custome file input - 1.3.4 https://github.com/Johann-S/bs-custom-file-input

chart js - 2.9.3 https://github.com/chartjs/Chart.js

datatable - 1.10.20 https://datatables.net/

ekko lightbox - 5.3 https://github.com/ashleydw/lightbox

fastclick - 1.0.6 https://github.com/ftlabs/fastclick
filterizr - 2.2.3 https://github.com/giotiskl/filterizr
flag icon css - https://github.com/lipis/flag-icon-css
flot - https://github.com/flot/flot
fontawesome - 5.13 https://fontawesome.com/
full calendar - 4.4 https://github.com/fullcalendar/fullcalendar

icheck bootstrap - 3.0.1 https://github.com/bantikyan/icheck-bootstrap
input mask - 5.0.3 https://github.com/RobinHerbots/Inputmask/
ion range slider - 2.3.1 https://github.com/IonDen/ion.rangeSlider

jquery - 3.4.1 https://jquery.com/
jquery knob - 1.2.13 https://github.com/aterrien/jQuery-Knob
jquery mapael - 2.2 https://github.com/neveldo/jQuery-Mapael
jquery mousewheel - 3.1.13 https://github.com/jquery/jquery-mousewheel
jquery ui - 1.12.1 https://github.com/jquery/jquery-ui
jquery validation - 1.19.1 https://github.com/jquery-validation/jquery-validation
jqvmap - 1.5.1 https://github.com/bbmumford/jqvmap
jsgrid - 1.5.3 https://github.com/tabalinas/jsgrid
jszip - 3.3 https://github.com/Stuk/jszip

moment - 2.24 https://github.com/moment/moment/

overlay scrollbar - 1.11 https://github.com/KingSora/OverlayScrollbars

pace progress - 0.7.8 https://github.com/lgaitan/pace
pdfmake - 0.1.65 https://github.com/bpampuch/pdfmake
popper - 1.16.1 https://github.com/popperjs/popper-core

raphael - 2.3 https://github.com/DmitryBaranovskiy/raphael

select2 - 4.0.13 https://github.com/select2/select2
sparkline - 1.2 https://github.com/mariusGundersen/sparkline
summernote - 0.8.18 https://github.com/summernote/summernote/
sweetalert2 - 9.10.8 https://github.com/sweetalert2/sweetalert2

tempus dominus bootstrap 4 - 5.1.2 https://github.com/tempusdominus/bootstrap-4
toastr - 2.1.4 https://github.com/CodeSeven/toastr
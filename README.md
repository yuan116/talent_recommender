**Requirement:**

1. [PHP 8](https://www.php.net/)
2. [MySQL 8](https://www.mysql.com/)
3. [Composer](https://getcomposer.org/)


**Step to install and setup this project:**

1. Clone this project to your machine.
2. Install [Composer](https://getcomposer.org/), if composer is not being install yet
3. Run command - `'composer install'` to install php libraries and setup project
4. If `'config.php'` is not exists at `'application/config'` directory
    1. Copy `'config_sample.php'` and paste at the same directory
    2. Rename the copied file to `'config.php'`
5. If `'database.php'` is not exists at `'application/config'` directory
    1. Copy `'database_sample.php'` and paste at the same directory
    2. Rename the copied file to `'database.php'`
    3. Open and edit `'database.php'`
        1. Change `'username'` and `'password'` according to your database setup
        2. Change `'database'` according to the database name in your database server
6. Create database according to the database name set in `'database.php'`
7. Run command - `'php index.php migrate --seed'` to run migration and seed test data
<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Yuan116\Ci3\Enhance\Libraries\Datatable\DatatableTrait;

class MY_Model extends \Yuan116\Ci3\Enhance\Core\MY_Model
{
    use DatatableTrait;

    public function __construct()
    {
        parent::__construct();
        $this->customizeTimestamp('by', $this->session->user_id);
    }
}

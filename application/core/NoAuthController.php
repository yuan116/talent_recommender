<?php

defined('BASEPATH') or exit('No direct script access allowed');

class NoAuthController extends MY_Controller
{
    protected string $redirect_link = 'home';

    public function __construct()
    {
        parent::__construct();
        if ($this->session->loggedin) {
            redirect($this->redirect_link);
        }
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AuthController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->checkAuth();
        $this->redirect_link = $this->session->loggedin ? 'home' : 'sign_in';
    }
}

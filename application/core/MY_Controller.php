<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Yuan116\Ci3\Enhance\Libraries\Password\Password;

class MY_Controller extends \Yuan116\Ci3\Enhance\Core\MY_Controller
{
    protected array $allow_user_type = [];
    protected bool $is_ajax = FALSE;
    protected string $redirect_link = 'sign_in';
    protected Password $password;

    public string $active_menu = '';
    public string $link = '/';
    public string $base_url = '';
    public string $site_url = '';

    public function __construct()
    {
        parent::__construct();
        $this->checkUrlName();
        $this->is_ajax = $this->input->is_ajax_request();
        $this->base_url = base_url('/');
        $this->site_url = site_url($this->link);
        $this->form_validation->set_error_delimiters('<span class="error invalid-feedback">', '</span>');
        $this->password = new Password();

        if ($this->session->loggedin) {
            if ($redirect_link = $this->session->auth_redirect_link) {
                $this->session->unset_userdata('auth_redirect_link');
                redirect($redirect_link);
            }

            $this->load->model(['CompanyModel', 'UserModel']);
            $this->session->set_userdata($this->UserModel->getLoginSessiondata($this->session->user_id));
            $this->saveLastLogin();

            if ($this->session->user_type_id == USER_TYPE_EMPLOYER) {
                $this->session->set_userdata('has_nav_user_body', TRUE);

                if (get_class($this) !== 'CompanyProfileController') {
                    $is_new_company = $this->CompanyModel->select('is_new')->row($this->session->company_id)->is_new;

                    if ($is_new_company) {
                        $this->setFlashToastr('info', 'Please complete company profile to continue using system.');
                        redirect('company_profile');
                    }
                }
            }

            if (empty($this->session->profile_pic) || (!is_file(FCPATH . $this->session->profile_pic))) {
                $this->session->set_userdata('profile_pic', DEFAULT_PROFILE_PIC);
            }
        }
    }

    protected function saveLastLogin(): void
    {
        $this->UserModel->save(['last_active' => date('Y-m-d H:i:s')], $this->session->user_id);
    }

    /**
     * Check Information
     *
     * checkUrlName
     * checkData
     */
    protected function checkUrlName(): void
    {
        if (stripos($this->uri->segment(1), 'controller') !== FALSE) {
            $this->forbiddenAccess();
        }
    }

    protected function checkData($id, $data): void
    {
        if (!empty($id) && $data === NULL) {
            if ($this->is_ajax) {
                $this->returnResponse([
                    'status' => FALSE,
                    'message' => 'No data found.'
                ], 404);
            } else {
                $this->setFlashMessage([
                    'type' => 'error',
                    'message' => 'No data found.'
                ]);
                $this->setFlashToastr('error', 'No data found.');
                show_404(current_url());
            }
        }
    }

    /**
     * Load Library
     *
     * sendEmail
     * loadUpload
     */
    protected function sendEmail(array $data, string $template, array $replace_data = []): bool
    {
        $this->load->library('email', [
            'useragent'      => 'CodeIgniter',
            'protocol'       => 'smtp',
            'mailpath'       => '/usr/sbin/sendmail',
            'smtp_host'      => 'smtp.gmail.com',
            'smtp_user'      => 'testsmtpde@gmail.com',
            'smtp_pass'      => 'uuzdvhmvlnxzzbbm',
            'smtp_port'      => 587,
            'smtp_timeout'   => 5,
            'smtp_keepalive' => FALSE,
            'smtp_crypto'    => 'tls',
            'wordwrap'       => TRUE,
            'wrapchars'      => 76,
            'mailtype'       => 'html',
            'charset'        => 'utf-8',
            'validate'       => FALSE,
            'priority'       => 1,
            'crlf'           => "\r\n",
            'newline'        => "\r\n",
            'bcc_batch_mode' => FALSE,
            'bcc_batch_size' => 200,
            'dsn'            => FALSE
        ]);
        $this->load->library('EmailTemplate', NULL, 'EmailTemplate');

        $template = $this->EmailTemplate->$template();
        foreach ($replace_data as $replace_key => $value) {
            $template = str_replace($replace_key, $value, $template);
        }
        $data['message'] = $template;
        $data['from'] = ['ihub@ihub.com', $this->config->item('title')];
        $data['cc'] = ['lim116@yahoo.com'];

        return $this->email->sendEmail($data);
    }

    protected function loadUpload(array $config): void
    {
        $upload_config = [
            'upload_path'            => 'uploads' . DIRECTORY_SEPARATOR,
            'allowed_types'          => '*',
            'file_name'              => '',
            'file_ext_tolower'       => TRUE,
            'overwrite'              => FALSE,
            'max_size'               => 0,
            'max_width'              => 0,
            'max_height'             => 0,
            'min_width'              => 0,
            'min_height'             => 0,
            'max_filename'           => 0,
            'max_filename_increment' => 100,
            'encrypt_name'           => TRUE,
            'remove_spaces'          => TRUE,
            'detect_mime'            => TRUE,
            'mod_mime_fix'           => TRUE
        ];

        if (!empty($config)) {
            $upload_config = array_replace($upload_config, $config);
        }

        $this->load->library('upload');
        $this->upload->initialize($upload_config);
    }

    /**
     * Form Validation Rules
     *
     * checkEmailExist
     * checkUserEmailExist
     * checkCompanyNameExist
     */
    public function checkEmailExist($email, $user_id = NULL): bool
    {
        if (trim($email) === '') {
            return TRUE;
        }

        $this->form_validation->set_message(__FUNCTION__, 'This Email is exist in our record.');
        return !$this->UserModel->emailExist($email, NULL, $user_id);
    }

    public function checkUserEmailExist($email): bool
    {
        if (trim($email) === '') {
            return TRUE;
        }

        $this->form_validation->set_message(__FUNCTION__, 'This Email is not exist in our record.');
        return $this->UserModel->emailExist($email);
    }

    public function checkCompanyNameExist($company_name, $company_id = NULL): bool
    {
        $where = ['company_name' => strtoupper($company_name)];

        if ($company_id !== NULL) {
            $where['id != '] = $company_id;
        }

        $this->form_validation->set_message(__FUNCTION__, 'This Company Name is exist in our record.');
        return !$this->CompanyModel->dataSeek($where);
    }

    /**
     * Response and Messages
     * 
     * checkAjax
     * returnResponse
     * successSaveData
     * successDeleteData
     * badRequest
     * forbiddenAccess
     * checkAuth
     */
    protected function checkAjax(): void
    {
        if (!$this->is_ajax) {
            $this->forbiddenAccess();
        }
    }

    protected function returnResponse(array $data, int $http_code = 200): void
    {
        if (!isset($data['status'])) {
            $data['status'] = TRUE;
        }

        $this->output->set_status_header($http_code)
            ->set_content_type('application/json')
            ->_display(json_encode($data, JSON_PRETTY_PRINT));
        exit();
    }

    protected function successSaveData(string $message = 'Save Successfully'): void
    {
        if ($this->is_ajax) {
            $this->returnResponse(['message' => $message]);
        } else {
            $this->setFlashToastr('success', $message);
            redirect($this->link);
        }
    }

    protected function successDeleteData(string $message = 'Delete Successfully'): void
    {
        if ($this->is_ajax) {
            $this->returnResponse(['message' => $message]);
        } else {
            $this->setFlashToastr('success', $message);
            redirect($this->link);
        }
    }

    protected function badRequest(string $message = 'Bad Request'): void
    {
        $this->returnResponse([
            'status' => FALSE,
            'message' => $message,
            'form_validation' => $this->form_validation->error_array()
        ], 400);
    }

    protected function unauthorized(string $message = 'Unauthorized Access'): void
    {
        if ($this->is_ajax) {
            $this->returnResponse([
                'status' => FALSE,
                'message' => $message
            ], 401);
        } else {
            $this->setFlashMessage([
                'type' => 'error',
                'message' => $message
            ]);
            $this->setFlashToastr('error', $message);
            redirect($this->redirect_link);
        }
    }

    protected function forbiddenAccess(string $message = 'No access allow'): void
    {
        if ($this->is_ajax) {
            $this->returnResponse([
                'status' => FALSE,
                'message' => $message
            ], 403);
        } else {
            $this->setFlashMessage([
                'type' => 'error',
                'message' => $message
            ]);
            $this->setFlashToastr('error', $message);
            redirect($this->redirect_link);
        }
    }

    protected function checkAuth(): void
    {
        if ($this->session->loggedin === NULL) {
            $auth_redirect_link = $this->uri->uri_string();

            $get_data = [];
            foreach ($this->input->get() as $key => $value) {
                $get_data[] = $key . '=' . $value;
            }

            $this->session->set_userdata('auth_redirect_link', $auth_redirect_link . '?' . implode('&', $get_data));

            $this->unauthorized();
        }

        if (!in_array($this->session->user_type_id, $this->allow_user_type)) {
            $this->forbiddenAccess();
        }
    }

    /**
     * Session Flash
     *
     * setFlashMessage
     * setFlashToastr
     */
    protected function setFlashMessage($value): void
    {
        $this->session->set_flashdata('message', $value);
    }

    protected function setFlashToastr(string $type, string $message): void
    {
        static $flash = [];

        if (empty($flash)) {
            $flash = $this->session->flashdata('toastr_message');
        }

        $flash[] = [
            'type' => $type,
            'message' => $message
        ];

        $this->session->set_flashdata('toastr_message', $flash);
    }
}

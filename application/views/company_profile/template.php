<div class="col-lg-12 mt-3 px-5">
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Picture</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo $this->site_url; ?>" method="post" enctype="multipart/form-data">
                        <div class="col-lg-12 text-center mb-4">
                            <img src="<?php echo $this->base_url . $data->company_logo; ?>" class="img-circle elevation-2" alt="User Profile Picture" width="15%" />
                        </div>

                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="company_logo" name="company_logo" accept=".png">
                                <label class="custom-file-label" for="company_logo">Choose file</label>
                            </div>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="save_picture_btn" value="save_picture">Save</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                        Colleague List <button type="button" class="btn btn-info btn-sm ml-5" data-target="#invite-user-modal" data-toggle="modal">Invite Colleague</button>
                    </h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-hover table-striped normal-datatable">
                        <thead>
                            <tr>
                                <th class="numbering-column">No.</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone Number</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($user_list as $row) : ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $row->name; ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo $row->phone_number; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Company Profile</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo $this->site_url; ?>" method="post" class="mx-1">
                        <div class="form-group row">
                            <label for="company_name" class="col-lg-3 control-label col-form-label label-required">Company Name</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control <?php echo fieldIsInvalid('company_name'); ?>" id="company_name" name="company_name" value="<?php echo set_value('company_name', $data->company_name); ?>" />
                                <?php echo fieldInvalidFeedback('company_name'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="company_email" class="col-lg-3 control-label col-form-label label-required">Company Email</label>
                            <div class="col-lg-9">
                                <input type="email" class="form-control <?php echo fieldIsInvalid('company_email'); ?>" id="company_email" name="company_email" value="<?php echo set_value('company_email', $data->company_email); ?>" />
                                <?php echo fieldInvalidFeedback('company_email'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_number" class="col-lg-3 control-label col-form-label label-required">Phone Number</label>
                            <div class="col-lg-9">
                                <input type="text" class="form-control <?php echo fieldIsInvalid('phone_number'); ?>" id="phone_number" name="phone_number" value="<?php echo set_value('phone_number', $data->phone_number); ?>" />
                                <?php echo fieldInvalidFeedback('phone_number'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="incorporate_date" class="col-lg-3 control-label col-form-label label-required">Incorporate Date</label>
                            <div class="col-lg-9 form-group input-group date datepicker" id="incorporate_date_datepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input <?php echo fieldIsInvalid('incorporate_date'); ?>" id="incorporate_date" name="incorporate_date" value="<?php echo set_value('incorporate_date', $data->incorporate_date); ?>" data-target="#incorporate_date_datepicker" data-toggle="datetimepicker" />
                                <div class="input-group-append" data-target="#incorporate_date_datepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text">
                                        <span class="fas fa-calendar icon-clickable" title="Show Calendar"></span>
                                    </div>
                                </div>
                                <?php echo fieldInvalidFeedback('incorporate_date'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-lg-3 control-label col-form-label label-required">Address</label>
                            <div class="col-lg-9">
                                <textarea class="form-control <?php echo fieldIsInvalid('address'); ?>" id="address" name="address" rows="3"><?php echo set_value('address', $data->address); ?></textarea>
                                <?php echo fieldInvalidFeedback('address'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="setting_state_id" class="col-lg-3 control-label col-form-label label-required">State</label>
                            <div class="col-lg-9">
                                <select class="form-control select2-select <?php echo fieldIsInvalid('setting_state_id'); ?>" id="setting_state_id" name="setting_state_id">
                                    <option value="">Please Select</option>
                                    <?php foreach ($state_list as $row) : ?>
                                        <option value="<?php echo $row->id; ?>" <?php echo setSelected('setting_state_id', $row->id, $data->setting_state_id); ?>><?php echo $row->description; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo fieldInvalidFeedback('setting_state_id'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="other_info" class="col-lg-3 control-label col-form-label">Other Info</label>
                            <div class="col-lg-9">
                                <textarea class="form-control <?php echo fieldIsInvalid('other_info'); ?>" id="other_info" name="other_info" rows="3"><?php echo set_value('other_info', $data->other_info); ?></textarea>
                                <?php echo fieldInvalidFeedback('other_info'); ?>
                            </div>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="save_profile_btn" value="save_profile">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="invite-user-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Invite Colleague Form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo $this->site_url . 'update'; ?>" method="post" id="invite-user-form">
                    <label for="name" class="control-label">Name</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" required="required" />
                        <?php echo fieldInvalidFeedback('name', TRUE); ?>
                    </div>

                    <label for="email" class="control-label">Email</label>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" required="required" />
                        <?php echo fieldInvalidFeedback('email', TRUE); ?>
                    </div>

                    <div class="form-group d-flex">
                        <button type="button" class="btn btn-primary ml-auto mr-2" id="save_btn" name="save_btn" value="save">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $('#save_btn').click(function() {
            let form_data = new FormData(document.getElementById('invite-user-form'));

            $('#invite-user-form').find('input').removeClass('is-invalid');
            $('#invite-user-form').find('span.invalid-feedback').html('');

            $.ajax({
                data: form_data,
                dataType: 'json',
                encode: true,
                contentType: false,
                processData: false,
                type: 'POST',
                url: '<?php echo $this->site_url . 'invite_user'; ?>'
            }).done((data, textStatus, jqXHR) => {
                if (data.status) {
                    $('#invite-user-modal').modal('hide');
                    toastr['success'](data.message);
                    setTimeout(() => {
                        window.location.reload();
                    }, 2000)
                }
            }).fail((jqXHR, textStatus, errorThrown) => {
                $.each(jqXHR.responseJSON.form_validation, (index, value) => {
                    $(`#${index}`).addClass('is-invalid');
                    $(`#${index}-error`).html(value);
                });
            });
        });
    });
</script>
<form action="<?php echo $this->site_url . @$data->id; ?>" method="post" class="mx-5 px-5">
    <?php if ($data !== NULL) : ?>
        <?php echo methodField('put'); ?>
    <?php endif; ?>

    <label for="title" class="control-label label-required">Title</label>
    <div class="form-group">
        <input type="text" class="form-control <?php echo fieldIsInvalid('title'); ?>" id="title" name="title" value="<?php echo set_value('title', @$data->title); ?>" />
        <?php echo fieldInvalidFeedback('title'); ?>
    </div>

    <label for="content" class="control-label">Content</label>
    <div class="form-group">
        <textarea class="form-control summernote-textarea" id="content" name="content"><?php echo set_value('content', @$data->content); ?></textarea>
    </div>

    <div class="form-group">
        <div class="row">
            <div class="col-lg-6">
                <label for="publish" class="control-label mr-5">Publish</label>
                <input type="checkbox" class="bootstrap-switch-checkbox" id="publish" name="publish" value="1" <?php echo setChecked('publish', '1', @$data->publish); ?> data-off-color="danger" data-off-text="No" data-on-color="success" data-on-text="Yes" />
            </div>

            <div class="col-lg-6">
                <label for="publish_to_outside" class="control-label mr-5">Publish to Outside</label>
                <input type="checkbox" class="bootstrap-switch-checkbox" id="publish_to_outside" name="publish_to_outside" value="1" <?php echo setChecked('publish_to_outside', '1', @$data->publish_to_outside); ?> data-off-color="danger" data-off-text="No" data-on-color="success" data-on-text="Yes" />
            </div>
        </div>
    </div>

    <label for="user_type_id" class="control-label">Show to</label>
    <div class="form-group">
        <select class="form-control select2-select" name="user_type_id[]" id="user_type_id" multiple>
            <?php foreach ($user_type_list as $row) : ?>
                <option value="<?php echo $row->id; ?>" <?php echo setMultiSelected('user_type_id', $row->id, @$user_type_data); ?>><?php echo $row->description; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group d-flex">
        <button type="submit" class="ml-auto btn btn-primary" name="save_btn" value="save">Save</button>
    </div>
</form>
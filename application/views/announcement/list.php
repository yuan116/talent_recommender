<div class="table-responsive">
    <table class="table table-bordered table-hover table-striped server-datatable" id="announcement-datatable">
        <thead>
            <tr>
                <th class="numbering-column">No.</th>
                <th>Title</th>
                <th>Publish</th>
                <th>Publish to Outside</th>
                <th class="action-column">Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        $('#announcement-datatable').DataTable({
            //Server Side Process
            processing: true,
            serverSide: true,
            ajax: {
                type: 'GET',
                url: '<?php echo $this->site_url . 'get_list'; ?>'
            },
            order: [
                [1, 'asc']
            ],
            //Other Config
            autoWidth: false,
            info: true,
            lengthChange: true,
            ordering: true,
            paging: true,
            searching: true,
            responsive: false,
            columns: [{
                    data: null,
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'title',
                    name: 'title'
                },
                {
                    data: 'publish',
                    name: 'publish',
                    orderable: false,
                    searchable: false,
                    render: (data, type, row) => {
                        let value = 'No';
                        let badge = 'danger';

                        if (!!window.parseInt(data)) {
                            value = 'Yes';
                            badge = 'success';
                        }

                        return `<span class="badge badge-${badge}">${value}</span>`
                    }
                },
                {
                    data: 'publish_to_outside',
                    name: 'publish_to_outside',
                    orderable: false,
                    searchable: false,
                    render: (data, type, row) => {
                        let value = 'No';
                        let badge = 'danger';

                        if (!!window.parseInt(data)) {
                            value = 'Yes';
                            badge = 'success';
                        }

                        return `<span class="badge badge-${badge}">${value}</span>`
                    }
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ],
            drawCallback: function(settings) {
                let datatable_api = this.api();

                datatable_api.column('.numbering-column', {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1 + datatable_api.page.info().start;
                });

                $('.title-tooltip').tooltip();
            }
        });
    });
</script>
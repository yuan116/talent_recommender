<style type="text/css">
    table tr{
        line-height: 50px;
    }
</style>

<div class="col">
    <div class="text-center">
        <h1 class="mt-3 text-uppercase"><strong>WELCOME TO <?php echo $this->config->item('title'); ?></strong></h1>
        <h4 class="mt-2">Explore and learn more about our advance features</h4>
    </div>

    <div class="card mt-3 mx-5">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <?php $contact_us_key = ['phone_number', 'email', 'address']; ?>
                    <table class="table">
                        <?php foreach ($contact_us_key as $key) : ?>
                            <tr>
                                <th><?php echo ucwords(str_replace('_', ' ', $key)); ?></th>
                                <td><?php echo $contact_us_data[$key]; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <div class="col-lg-6">
                    <iframe src="<?php echo $contact_us_data['address_iframe']; ?>" frameborder="0" width="100%" height="400px"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col">
    <div class="text-center">
        <h1 class="mt-3 text-uppercase"><strong>WELCOME TO <?php echo $this->config->item('title'); ?></strong></h1>
        <h4 class="mt-2">Explore and learn more about our advance features</h4>
    </div>

    <div class="card mt-3 mx-5">
        <div class="card-body">
            <div id="announcement-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <?php foreach ($announcement_index_list as $index) : ?>
                        <li data-target="#announcement-carousel" data-slide-to="<?php echo $index; ?>" class="<?php echo $index === 0 ? 'active' : ''; ?>"></li>
                    <?php endforeach; ?>
                </ol>

                <div class="carousel-inner px-5" style="min-height: 450px">
                    <?php foreach ($announcement_list as $key => $row) : ?>
                        <div class="carousel-item <?php echo $key === 0 ? 'active' : ''; ?>">
                            <div class="row overflow-auto" style="max-height: 400px;">
                                <div class="col-lg-12">
                                    <h3 class="text-center"><?php echo $row->title; ?></h3>
                                    <hr />
                                </div>
                                <div class="col-lg-12 px-5">
                                    <?php echo $row->content; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
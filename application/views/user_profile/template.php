<div class="col-lg-12 mt-3 px-5">
    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Picture</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="profile_picture" name="profile_picture" accept=".jpg, .jpeg">
                                <label class="custom-file-label" for="profile_picture">Choose file</label>
                            </div>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="save_picture_btn" value="save_picture">Save</button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Profile</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo current_url(); ?>" method="post" class="mx-1">
                        <div class="form-group row">
                            <label for="name" class="col-lg-4 control-label col-form-label label-required">Name</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control <?php echo fieldIsInvalid('name'); ?>" id="name" name="name" value="<?php echo set_value('name', $data->name); ?>" />
                                <?php echo fieldInvalidFeedback('name'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-lg-4 control-label col-form-label label-required">Gender</label>
                            <div class="col-lg-8">
                                <select class="form-control select2-select" id="gender" name="gender">
                                    <?php foreach (GENDER_LIST as $value => $text) : ?>
                                        <option value="<?php echo $value; ?>" <?php echo setSelected('gender', $value, $data->gender); ?>><?php echo $text; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo fieldInvalidFeedback('gender'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="identity_number" class="col-lg-4 control-label col-form-label label-required">Identity Number</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control <?php echo fieldIsInvalid('identity_number'); ?>" id="identity_number" name="identity_number" value="<?php echo set_value('identity_number', $data->identity_number); ?>" />
                                <?php echo fieldInvalidFeedback('identity_number'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone_number" class="col-lg-4 control-label col-form-label label-required">Phone Number</label>
                            <div class="col-lg-8">
                                <input type="text" class="form-control <?php echo fieldIsInvalid('phone_number'); ?>" id="phone_number" name="phone_number" value="<?php echo set_value('phone_number', $data->phone_number); ?>" />
                                <?php echo fieldInvalidFeedback('phone_number'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="date_of_birth" class="col-lg-4 control-label col-form-label label-required">Date of Birth</label>
                            <div class="col-lg-8 form-group input-group date datepicker" id="date_of_birth_datepicker" data-target-input="nearest">
                                <input type="text" class="form-control datetimepicker-input <?php echo fieldIsInvalid('date_of_birth'); ?>" id="date_of_birth" name="date_of_birth" value="<?php echo set_value('date_of_birth', $data->date_of_birth); ?>" data-target="#date_of_birth_datepicker" data-toggle="datetimepicker" />
                                <div class="input-group-append" data-target="#date_of_birth_datepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text">
                                        <span class="fas fa-calendar icon-clickable" title="Show Calendar"></span>
                                    </div>
                                </div>
                                <?php echo fieldInvalidFeedback('date_of_birth'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address" class="col-lg-4 control-label col-form-label label-required">Address</label>
                            <div class="col-lg-8">
                                <textarea class="form-control <?php echo fieldIsInvalid('address'); ?>" id="address" name="address" rows="3"><?php echo set_value('address', $data->address); ?></textarea>
                                <?php echo fieldInvalidFeedback('address'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="setting_state_id" class="col-lg-4 control-label col-form-label label-required">State</label>
                            <div class="col-lg-8">
                                <select class="form-control select2-select <?php echo fieldIsInvalid('setting_state_id'); ?>" id="setting_state_id" name="setting_state_id">
                                    <option value="">Please Select</option>
                                    <?php foreach ($state_list as $row) : ?>
                                        <option value="<?php echo $row->id; ?>" <?php echo setSelected('setting_state_id', $row->id, $data->setting_state_id); ?>><?php echo $row->description; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <?php echo fieldInvalidFeedback('setting_state_id'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="profile_password" class="col-lg-4 control-label col-form-label label-required">Current Password</label>
                            <div class="col-lg-8 form-group">
                                <input type="password" class="form-control <?php echo fieldIsInvalid('profile_password'); ?>" id="profile_password" name="profile_password" value="<?php echo set_value('profile_password'); ?>" />
                                <?php echo fieldInvalidFeedback('profile_password'); ?>
                            </div>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="save_profile_btn" value="save_profile">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card login-card-body p-0">
                <div class="card-header">
                    <h3 class="card-title">Email</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo current_url(); ?>" method="post" class="mx-1">
                        <div class="form-group row">
                            <label class="col-lg-4 control-label col-form-label">Current Email</label>
                            <div class="col-lg-8">
                                <input type="email" class="form-control" value="<?php echo $data->email; ?>" disabled />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-lg-4 control-label col-form-label label-required">New Email</label>
                            <div class="col-lg-8">
                                <input type="email" class="form-control <?php echo fieldIsInvalid('email'); ?>" id="email" name="email" value="<?php echo set_value('email', $this->session->tempdata('email')); ?>" />
                                <?php echo fieldInvalidFeedback('email'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-lg-4 control-label col-form-label label-required">Current Password</label>
                            <div class="col-lg-8 form-group">
                                <input type="password" class="form-control <?php echo fieldIsInvalid('email_password'); ?>" id="email_password" name="email_password" value="<?php echo set_value('email_password'); ?>" />
                                <?php echo fieldInvalidFeedback('email_password'); ?>
                            </div>
                        </div>

                        <div class="form-group d-flex">
                            <?php if ($this->session->tempdata('resend_btn')) : ?>
                                <button type="submit" class="ml-auto btn btn-primary" name="resend_btn" value="resend">Resend</button>
                            <?php else : ?>
                                <button type="submit" class="ml-auto btn btn-primary" name="save_email_btn" value="save_email">Save</button>
                            <?php endif; ?>
                        </div>
                    </form>
                </div>
            </div>

            <div class="card login-card-body p-0">
                <div class="card-header">
                    <h3 class="card-title">Password</h3>
                </div>
                <div class="card-body">
                    <form action="<?php echo current_url(); ?>" method="post">
                        <div class="form-group row">
                            <label for="current_password" class="col-lg-4 control-label col-form-label label-required">Current Password</label>
                            <div class="col-lg-8 form-group">
                                <input type="password" class="form-control <?php echo fieldIsInvalid('current_password'); ?>" id="current_password" name="current_password" value="<?php echo set_value('current_password'); ?>" />
                                <?php echo fieldInvalidFeedback('current_password'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new_password" class="col-lg-4 control-label col-form-label label-required">New Password</label>
                            <div class="col-lg-8 form-group">
                                <input type="password" class="form-control <?php echo fieldIsInvalid('new_password'); ?>" id="new_password" name="new_password" value="<?php echo set_value('new_password'); ?>" />
                                <?php echo fieldInvalidFeedback('new_password'); ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="confirm_password" class="col-lg-4 control-label col-form-label label-required">Confirm Password</label>
                            <div class="col-lg-8 form-group">
                                <input type="password" class="form-control <?php echo fieldIsInvalid('confirm_password'); ?>" id="confirm_password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>" />
                                <?php echo fieldInvalidFeedback('confirm_password'); ?>
                            </div>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="save_password_btn" value="save_password">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
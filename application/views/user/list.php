<div class="table-responsive">
    <table class="table table-bordered table-hover table-striped server-datatable" id="user-datatable">
        <thead>
            <tr>
            <th class="numbering-column">No.</th>
                <th>Name</th>
                <th>Email</th>
                <th>Activated</th>
                <th>User Type</th>
                <th>State</th>
                <th>Company Name</th>
                <th class="action-column">Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        $('#user-datatable').DataTable({
            //Server Side Process
            processing: true,
            serverSide: true,
            ajax: {
                type: 'GET',
                url: '<?php echo $this->site_url . 'get_list'; ?>'
            },
            order: [
                [1, 'asc']
            ],
            //Other Config
            autoWidth: false,
            info: true,
            lengthChange: true,
            ordering: true,
            paging: true,
            searching: true,
            responsive: false,
            columns: [{
                    data: null,
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'name',
                    name: 'a.name'
                },
                {
                    data: 'email',
                    name: 'a.email'
                },
                {
                    data: 'activated',
                    name: 'a.activated',
                    orderable: false,
                    searchable: false,
                    render: (data, type, row) => {
                        let value = 'No';
                        let badge = 'danger';

                        if (!!window.parseInt(data)) {
                            value = 'Yes';
                            badge = 'success';
                        }

                        return `<span class="badge badge-${badge} navbar">${value}</span>`
                    }
                },
                {
                    data: 'user_type_description',
                    name: 'b.description'
                },
                {
                    data: 'state_description',
                    name: 'c.description'
                },
                {
                    data: 'company_name',
                    name: 'd.company_name'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ],
            drawCallback: function(settings) {
                let datatable_api = this.api();

                datatable_api.column('.numbering-column', {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1 + datatable_api.page.info().start;
                });

                $('.title-tooltip').tooltip();
            }
        });
    });
</script>
<div class="col-lg-12 mt-3">
    <div class="card card-primary card-tabs">
        <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" role="tablist">
                <li class="pt-2 px-3">
                    <h3 class="card-title">User</h3>
                </li>
                <li class="nav-item">
                    <a href="#list" class="nav-link <?php echo setTabActive(1, $tab); ?>" id="list-tab" data-toggle="pill" role="tab" aria-controls="list" aria-selected="<?php echo setTabActive(1, $tab, FALSE) ? 'true' : 'false'; ?>">List</a>
                </li>
                <li class="nav-item">
                    <a href="#form" class="nav-link <?php echo setTabActive(2, $tab); ?>" id="form-tab" data-toggle="pill" role="tab" aria-controls="form" aria-selected="<?php echo setTabActive(2, $tab, FALSE) ? 'true' : 'false'; ?>">Form <?php echo empty($data) ? '(Staff)' : ''; ?></a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane fade <?php echo setTabActive(1, $tab); ?>" id="list" role="tabpanel" aria-labelledby="list-tab">
                    <?php $this->load->view('user/list'); ?>
                </div>
                <div class="tab-pane fade <?php echo setTabActive(2, $tab); ?>" id="form" role="tabpanel" aria-labelledby="form-tab">
                    <?php $this->load->view('user/form'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="<?php echo $this->site_url . @$data->id; ?>" method="post" class="mx-5 px-5">
    <?php if ($data !== NULL) : ?>
        <?php echo methodField('put'); ?>
    <?php endif; ?>

    <div class="form-group row">
        <label for="name" class="col-lg-3 control-label col-form-label label-required">Name</label>
        <div class="col-lg-7">
            <input type="text" class="form-control <?php echo fieldIsInvalid('name'); ?>" id="name" name="name" value="<?php echo set_value('name', @$data->name); ?>" />
            <?php echo fieldInvalidFeedback('name'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-lg-3 control-label col-form-label label-required">Email</label>
        <div class="col-lg-7">
            <input type="email" class="form-control <?php echo fieldIsInvalid('email'); ?>" id="email" name="email" value="<?php echo set_value('email', @$data->email); ?>" />
            <?php echo fieldInvalidFeedback('email'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="email" class="col-lg-3 control-label col-form-label">Gender</label>
        <div class="col-lg-7">
            <select class="form-control select2-select" id="gender" name="gender">
                <?php foreach (GENDER_LIST as $value => $text) : ?>
                    <option value="<?php echo $value; ?>" <?php echo setSelected('gender', $value, @$data->gender); ?>><?php echo $text; ?></option>
                <?php endforeach; ?>
            </select>
            <?php echo fieldInvalidFeedback('gender'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="identity_number" class="col-lg-3 control-label col-form-label">Identity Number</label>
        <div class="col-lg-7">
            <input type="text" class="form-control <?php echo fieldIsInvalid('identity_number'); ?>" id="identity_number" name="identity_number" value="<?php echo set_value('identity_number', @$data->identity_number); ?>" />
            <?php echo fieldInvalidFeedback('identity_number'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="phone_number" class="col-lg-3 control-label col-form-label">Phone Number</label>
        <div class="col-lg-7">
            <input type="text" class="form-control <?php echo fieldIsInvalid('phone_number'); ?>" id="phone_number" name="phone_number" value="<?php echo set_value('phone_number', @$data->phone_number); ?>" />
            <?php echo fieldInvalidFeedback('phone_number'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="date_of_birth" class="col-lg-3 control-label col-form-label label-required">Date of Birth</label>
        <div class="col-lg-7 form-group input-group date datepicker" id="date_of_birth_datepicker" data-target-input="nearest">
            <input type="text" class="form-control datetimepicker-input <?php echo fieldIsInvalid('date_of_birth'); ?>" id="date_of_birth" name="date_of_birth" value="<?php echo set_value('date_of_birth', @$data->date_of_birth); ?>" data-target="#date_of_birth_datepicker" data-toggle="datetimepicker" />
            <div class="input-group-append" data-target="#date_of_birth_datepicker" data-toggle="datetimepicker">
                <div class="input-group-text">
                    <span class="fas fa-calendar icon-clickable" title="Show Calendar"></span>
                </div>
            </div>
            <?php echo fieldInvalidFeedback('date_of_birth'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="address" class="col-lg-3 control-label col-form-label">Address</label>
        <div class="col-lg-7">
            <textarea class="form-control <?php echo fieldIsInvalid('address'); ?>" id="address" name="address" rows="3"><?php echo set_value('address', @$data->address); ?></textarea>
            <?php echo fieldInvalidFeedback('address'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="setting_state_id" class="col-lg-3 control-label col-form-label label-required">State</label>
        <div class="col-lg-7">
            <select class="form-control select2-select <?php echo fieldIsInvalid('setting_state_id'); ?>" id="setting_state_id" name="setting_state_id">
                <option value="">Please Select</option>
                <?php foreach ($state_list as $row) : ?>
                    <option value="<?php echo $row->id; ?>" <?php echo setSelected('setting_state_id', $row->id, @$data->setting_state_id); ?>><?php echo $row->description; ?></option>
                <?php endforeach; ?>
            </select>
            <?php echo fieldInvalidFeedback('setting_state_id'); ?>
        </div>
    </div>

    <?php if (@$data->user_type_id == USER_TYPE_EMPLOYER) : ?>
        <div class="form-group row">
            <label for="company_id" class="col-lg-3 control-label col-form-label label-required">Company</label>
            <div class="col-lg-7">
                <select class="form-control select2-select <?php echo fieldIsInvalid('company_id'); ?>" id="company_id" name="company_id">
                    <option value="">Please Select</option>
                    <?php foreach ($company_list as $row) : ?>
                        <option value="<?php echo $row->id; ?>" <?php echo setSelected('company_id', $row->id, @$data->company_id); ?>><?php echo $row->company_name; ?></option>
                    <?php endforeach; ?>
                </select>
                <?php echo fieldInvalidFeedback('company_id'); ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group col-lg-10 d-flex">
        <button type="submit" class="ml-auto btn btn-primary" name="save_btn" value="save">Save</button>
    </div>
</form>
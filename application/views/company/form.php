<form action="<?php echo $this->site_url . @$data->id; ?>" method="post" class="mx-5 px-5">
    <?php if ($data !== NULL) : ?>
        <?php echo methodField('put'); ?>
    <?php endif; ?>

    <div class="form-group row">
        <label for="company_name" class="col-lg-3 control-label col-form-label label-required">Company Name</label>
        <div class="col-lg-7">
            <input type="text" class="form-control <?php echo fieldIsInvalid('company_name'); ?>" id="company_name" name="company_name" value="<?php echo set_value('company_name', @$data->company_name); ?>" />
            <?php echo fieldInvalidFeedback('company_name'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="company_email" class="col-lg-3 control-label col-form-label">Company Email</label>
        <div class="col-lg-7">
            <input type="email" class="form-control <?php echo fieldIsInvalid('company_email'); ?>" id="company_email" name="company_email" value="<?php echo set_value('company_email', @$data->company_email); ?>" />
            <?php echo fieldInvalidFeedback('company_email'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="phone_number" class="col-lg-3 control-label col-form-label">Phone Number</label>
        <div class="col-lg-7">
            <input type="text" class="form-control <?php echo fieldIsInvalid('phone_number'); ?>" id="phone_number" name="phone_number" value="<?php echo set_value('phone_number', @$data->phone_number); ?>" />
            <?php echo fieldInvalidFeedback('phone_number'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="incorporate_date" class="col-lg-3 control-label col-form-label">Incorporate Date</label>
        <div class="col-lg-7 form-group input-group date datepicker" id="incorporate_date_datepicker" data-target-input="nearest">
            <input type="text" class="form-control datetimepicker-input <?php echo fieldIsInvalid('incorporate_date'); ?>" id="incorporate_date" name="incorporate_date" value="<?php echo set_value('incorporate_date', @$data->incorporate_date); ?>" data-target="#incorporate_date_datepicker" data-toggle="datetimepicker" />
            <div class="input-group-append" data-target="#incorporate_date_datepicker" data-toggle="datetimepicker">
                <div class="input-group-text">
                    <span class="fas fa-calendar icon-clickable" title="Show Calendar"></span>
                </div>
            </div>
            <?php echo fieldInvalidFeedback('incorporate_date'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="address" class="col-lg-3 control-label col-form-label">Address</label>
        <div class="col-lg-7">
            <textarea class="form-control <?php echo fieldIsInvalid('address'); ?>" id="address" name="address" rows="3"><?php echo set_value('address', @$data->address); ?></textarea>
            <?php echo fieldInvalidFeedback('address'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="setting_state_id" class="col-lg-3 control-label col-form-label label-required">State</label>
        <div class="col-lg-7">
            <select class="form-control select2-select <?php echo fieldIsInvalid('setting_state_id'); ?>" id="setting_state_id" name="setting_state_id">
                <option value="">Please Select</option>
                <?php foreach ($state_list as $row) : ?>
                    <option value="<?php echo $row->id; ?>" <?php echo setSelected('setting_state_id', $row->id, @$data->setting_state_id); ?>><?php echo $row->description; ?></option>
                <?php endforeach; ?>
            </select>
            <?php echo fieldInvalidFeedback('setting_state_id'); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="other_info" class="col-lg-3 control-label col-form-label">Other Info</label>
        <div class="col-lg-7">
            <textarea class="form-control <?php echo fieldIsInvalid('other_info'); ?>" id="other_info" name="other_info" rows="3"><?php echo set_value('other_info', @$data->other_info); ?></textarea>
            <?php echo fieldInvalidFeedback('other_info'); ?>
        </div>
    </div>

    <div class="form-group col-lg-10 d-flex">
        <button type="submit" class="ml-auto btn btn-primary" name="save_btn" value="save">Save</button>
    </div>
</form>

<?php if (!empty($data)) : ?>
    <div class="card mx-5 px-5">
        <div class="card-header">
            <h3 class="card-title">User in this company</h3>
        </div>
        <div class="card-body">
            <table class="table table-bordered table-hover table-striped normal-datatable">
                <thead>
                    <tr>
                        <th class="numbering-column">No.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($user_list as $row) : ?>
                        <tr>
                            <th></th>
                            <th><?php echo $row->name; ?></th>
                            <th><?php echo $row->email; ?></th>
                            <th><?php echo $row->phone_number; ?></th>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif; ?>
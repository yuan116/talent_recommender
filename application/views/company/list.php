<div class="table-responsive">
    <table class="table table-bordered table-hover table-striped server-datatable" id="company-datatable">
        <thead>
            <tr>
                <th class="numbering-column">No.</th>
                <th>Company Name</th>
                <th>Company Email</th>
                <th>Phone Number</th>
                <th>State</th>
                <th class="action-column">Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

<script type="text/javascript">
    $(function() {
        $('#company-datatable').DataTable({
            //Server Side Process
            processing: true,
            serverSide: true,
            ajax: {
                type: 'GET',
                url: '<?php echo $this->site_url . 'get_list'; ?>'
            },
            order: [
                [1, 'asc']
            ],
            //Other Config
            autoWidth: false,
            info: true,
            lengthChange: true,
            ordering: true,
            paging: true,
            searching: true,
            responsive: false,
            columns: [{
                    data: null,
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'company_name',
                    name: 'a.company_name'
                },
                {
                    data: 'company_email',
                    name: 'a.company_email'
                },
                {
                    data: 'phone_number',
                    name: 'a.phone_number'
                },
                {
                    data: 'state_description',
                    name: 'b.description'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false
                }
            ],
            drawCallback: function(settings) {
                let datatable_api = this.api();

                datatable_api.column('.numbering-column', {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1 + datatable_api.page.info().start;
                });

                $('.title-tooltip').tooltip();
            }
        });
    });
</script>
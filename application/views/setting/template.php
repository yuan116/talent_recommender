<div class="col-lg-12 mt-3">
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="nav flex-column h-100" id="setting-tab" role="tablist" aria-orientation="vertical">
                    <?php $site_url = site_url('setting/'); ?>
                    <a href="<?php echo $site_url . 'contact_us'; ?>" class="nav-link <?php echo setTabActive('contact_us', $this->setting_tab); ?>" role="tab" aria-selected="<?php echo setTabActive('contact_us', $this->setting_tab, FALSE) ? 'true' : 'false'; ?>">Contact Us</a>
                    <a href="<?php echo $site_url . 'state'; ?>" class="nav-link <?php echo setTabActive('state', $this->setting_tab); ?>" role="tab" aria-selected="<?php echo setTabActive('state', $this->setting_tab, FALSE) ? 'true' : 'false'; ?>">State</a>
                    <a href="<?php echo $site_url . 'language'; ?>" class="nav-link <?php echo setTabActive('language', $this->setting_tab); ?>" role="tab" aria-selected="<?php echo setTabActive('language', $this->setting_tab, FALSE) ? 'true' : 'false'; ?>">Langauge</a>
                    <a href="<?php echo $site_url . 'field_of_study'; ?>" class="nav-link <?php echo setTabActive('field_of_study', $this->setting_tab); ?>" role="tab" aria-selected="<?php echo setTabActive('field_of_study', $this->setting_tab, FALSE) ? 'true' : 'false'; ?>">Field Of Study</a>
                    <a href="<?php echo $site_url . 'skill'; ?>" class="nav-link <?php echo setTabActive('skill', $this->setting_tab); ?>" role="tab" aria-selected="<?php echo setTabActive('skill', $this->setting_tab, FALSE) ? 'true' : 'false'; ?>">Skill</a>
                </div>
            </div>
        </div>

        <div class="col-lg-9">
            <div class="tab-content">
                <div class="tab-pane text-left show active" role="tabpanel">
                    <?php if ($this->setting_tab !== '') : ?>
                        <?php $this->load->view('setting/' . $this->setting_tab); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
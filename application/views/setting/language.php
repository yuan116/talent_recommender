<div class="col-lg-12">
    <div class="card card-primary card-tabs">
        <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" role="tablist">
                <li class="pt-2 px-3">
                    <h3 class="card-title">Language</h3>
                </li>
                <li class="nav-item">
                    <a href="#list" class="nav-link <?php echo setTabActive(1, $tab); ?>" id="list-tab" data-toggle="pill" role="tab" aria-controls="list" aria-selected="<?php echo setTabActive(1, $tab, FALSE) ? 'true' : 'false'; ?>">List</a>
                </li>
                <li class="nav-item">
                    <a href="#form" class="nav-link <?php echo setTabActive(2, $tab); ?>" id="form-tab" data-toggle="pill" role="tab" aria-controls="form" aria-selected="<?php echo setTabActive(2, $tab, FALSE) ? 'true' : 'false'; ?>">Form</a>
                </li>
            </ul>
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane fade <?php echo setTabActive(1, $tab); ?>" id="list" role="tabpanel" aria-labelledby="list-tab">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped normal-datatable">
                            <thead>
                                <tr>
                                    <th class="numbering-column">No.</th>
                                    <th>Description</th>
                                    <th class="action-column">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($state_list as $row) : ?>
                                    <tr>
                                        <td></td>
                                        <td><?php echo $row->description; ?></td>
                                        <td>
                                            <a href="<?php echo $this->site_url . $row->id; ?>" class="btn btn-xs btn-info title-tooltip" title="Edit"><span class="fas fa-edit"></span></a>&nbsp;
                                            <a href="<?php echo $this->site_url . $row->id; ?>" class="btn btn-xs btn-danger list-delete-btn title-tooltip" title="Delete"><span class="fas fa-trash-alt"></span></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane fade <?php echo setTabActive(2, $tab); ?>" id="form" role="tabpanel" aria-labelledby="form-tab">
                    <form action="<?php echo $this->site_url . @$data->id; ?>" method="post" class="mx-5 px-5">
                        <?php if ($data !== NULL) : ?>
                            <?php echo methodField('put'); ?>
                        <?php endif; ?>

                        <div class="form-group row">
                            <label for="description" class="col-lg-3 control-label col-form-label label-required">Description</label>
                            <div class="col-lg-7">
                                <input type="text" class="form-control <?php echo fieldIsInvalid('description'); ?>" id="description" name="description" value="<?php echo set_value('description', @$data->description); ?>" />
                                <?php echo fieldInvalidFeedback('description'); ?>
                            </div>
                        </div>

                        <div class="form-group col-lg-10 d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="save_btn" value="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
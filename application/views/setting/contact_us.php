<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Contact Us</h3>
        </div>
        <div class="card-body">
            <form action="<?php echo current_url(); ?>" method="post" class="mx-5">
                <div class="form-group row">
                    <label for="phone_number" class="col-lg-3 control-label col-form-label">Phone Number</label>
                    <div class="col-lg-9">
                        <input type="text" class="form-control <?php echo fieldIsInvalid('phone_number'); ?>" id="phone_number" name="phone_number" value="<?php echo set_value('phone_number', @$data['phone_number']); ?>" />
                        <?php echo fieldInvalidFeedback('phone_number'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-lg-3 control-label col-form-label">Email</label>
                    <div class="col-lg-9">
                        <input type="email" class="form-control <?php echo fieldIsInvalid('email'); ?>" id="email" name="email" value="<?php echo set_value('email', @$data['email']); ?>" />
                        <?php echo fieldInvalidFeedback('email'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address" class="col-lg-3 control-label col-form-label">Address</label>
                    <div class="col-lg-9">
                        <textarea class="form-control <?php echo fieldIsInvalid('address'); ?>" id="address" name="address" rows="3"><?php echo set_value('address', @$data['address']); ?></textarea>
                        <?php echo fieldInvalidFeedback('address'); ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="address_iframe" class="col-lg-3 control-label col-form-label">Address Iframe</label>
                    <div class="col-lg-9">
                        <textarea class="form-control <?php echo fieldIsInvalid('address_iframe'); ?>" id="address_iframe" name="address_iframe" rows="5"><?php echo set_value('address_iframe', @$data['address_iframe']); ?></textarea>
                        <?php echo fieldInvalidFeedback('address_iframe'); ?>
                    </div>
                </div>

                <div class="form-group d-flex">
                    <button type="submit" class="ml-auto btn btn-primary" name="save_btn" value="save">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="edit-file-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit File Form</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?php echo $this->site_url . 'update'; ?>" method="post" id="edit-file-form">
                    <input type="hidden" id="id" name="id" value="" />
                    <label for="filename" class="control-label">Filename</label>
                    <div class="form-group">
                        <input type="text" class="form-control" id="filename" name="filename" value="" required="required" />
                    </div>

                    <div class="form-group d-flex">
                        <button type="submit" class="btn btn-primary ml-auto mr-2" name="save_btn" value="save">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function() {
        $.validator.setDefaults({
            submitHandler: (form) => {
                form.submit();
            }
        });

        $('#edit-file-form').validate({
            rules: {
                filename: {
                    required: true,
                    maxlength: 50
                }
            },
            messages: {
                filename: {
                    required: 'The Filename field is required.',
                    maxlength: 'The Filename field cannot exceed 50 characters in length.'
                }
            },
            errorElement: 'span',
            errorPlacement: (error, element) => {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: (element, errorClass, validClass) => {
                $(element).addClass('is-invalid');
            },
            unhighlight: (element, errorClass, validClass) => {
                $(element).removeClass('is-invalid');
            }
        });
    });

    function edit(user_file_id, filename, as_resume) {
        $('#edit-file-form input[name="id"]').val(user_file_id);
        $('#edit-file-form input[name="filename"]').val(filename);

        $('#edit-file-modal').modal('show');
    }
</script>
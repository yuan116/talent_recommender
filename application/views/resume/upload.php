<style type="text/css">
    #upload_resume_container {
        height: 200px;
        border: lightblue 5px dashed;
        font-size: 20px;
        cursor: pointer;
    }
</style>

<div class="modal fade" id="upload-resume-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload Resume</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center text-muted font-weight-bold py-5" id="upload_resume_container">
                    Only accept pdf <br />
                    Click here to upload files<br />
                    Drag & Drop files to upload
                </div>
                <div class="d-none">
                    <input type="file" id="file" name="file[]" accept=".pdf" multiple />
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    let uploading_file = false;
    let upload_file_ajax = null;

    $(function() {
        //Upload file using drag and drop
        //preventing page from redirecting
        $('html').on('dragover', function(event) {
            event.preventDefault();
            event.stopPropagation();

            if ($('body').children('div.overlay-wrapper').length === 0) {
                $('body').append(`
                <div class="overlay-wrapper">
                    <div class="overlay" id="overlay-dropzone">
                        <span class="fas fa-2x fa-file-upload"></span>
                        <h2>Drop here to upload</h2>
                    </div>
                </div>`);
            }
        }).on('drop', function(event) {
            event.preventDefault();

            $('body').children('div.overlay-wrapper').remove();
            uploadFile(event.originalEvent.dataTransfer.files);
        }).on({
            dragleave: function(event) {
                event.preventDefault();
                event.stopPropagation();

                $('body').children('div.overlay-wrapper').remove();
            }
        }, 'div.overlay-wrapper');

        $('#upload_resume_container').click(function() {
            $('#file').trigger('click');
        });

        $('#file').change(function(event) {
            uploadFile(this.files);

            $(this).val('');
        })

        // Sending AJAX request and upload file
        function uploadFile(files) {
            let form_data = new FormData();
            form_data.append('<?php echo $this->security->get_csrf_token_name(); ?>', '<?php echo $this->security->get_csrf_hash(); ?>');

            let file_length = files.length;
            for (let count = 0; count < file_length; count++) {
                uploading_file = true;

                form_data.append('file', files[count]);
                form_data.append('count', count);

                upload_file_ajax = $.ajax({
                    data: form_data,
                    dataType: 'json',
                    encode: true,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    url: '<?php echo $this->site_url . 'upload'; ?>'
                }).done((data, textStatus, jqXHR) => {
                    uploading_file = false;

                    if (data.status) {
                        toastr['success'](data.message);
                    } else {
                        toastr['error'](data.message);
                    }

                    reloadDatatable('resume-datatable');
                }).fail((jqXHR, textStatus, errorThrown) => {
                    uploading_file = false;
                    toastr['error']('Fail to upload resume.');
                });
            }

            upload_file_ajax = null;
            $('#upload-resume-modal').modal('hide');
        }

        //Window close, leave/reload other page will cancel ajax request
        $(window).on('unload', () => {
            if (uploading_file) {
                upload_file_ajax.abort();
            }
        });

        $(window).on('beforeunload', (event) => {
            event.preventDefault();
            if (uploading_file) {
                return 'Your uploading file will be cancel, if you leave/reload page.';
            }
        });
    });
</script>
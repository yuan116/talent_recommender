<div class="col-lg-3 mt-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Filter</h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <form method="get">
                    <label for="cgpa">CGPA</label>
                    <div class="form-group mx-3">
                        <div class="slider-blue">
                            <input type="text" class="form-control" id="cgpa" name="cgpa">
                        </div>
                    </div>

                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="has_referee" value="1" />
                        <label class="form-check-label" for="has_referee">Has Referee</label>
                    </div>

                    <label for="setting_field_of_study_id">Field Of Study</label>
                    <div class="form-group">
                        <select class="form-control select2-select" id="setting_field_of_study_id" name="setting_field_of_study_id[]" multiple>
                            <?php foreach ($field_of_study_list as $field_of_study_id => $field_of_study_description) : ?>
                                <option value="<?php echo $field_of_study_id; ?>"><?php echo $field_of_study_description; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <label for="setting_skill_id">Skill</label>
                    <div class="form-group">
                        <select class="form-control select2-select" id="setting_skill_id" name="setting_skill_id[]" multiple>
                            <?php foreach ($skill_list as $field_of_study_id => $skill_row) : ?>
                                <optgroup label="<?php echo $field_of_study_list[$field_of_study_id]; ?>">
                                    <?php foreach ($skill_row as $skill_id => $skill_description) : ?>
                                        <option value="<?php echo $skill_id; ?>"><?php echo $skill_description; ?></option>
                                    <?php endforeach; ?>
                                </optgroup>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <label for="setting_language_id">Language</label>
                    <div class="form-group">
                        <select class="form-control select2-select" id="setting_language_id" name="setting_language_id[]" multiple>
                            <?php foreach ($language_list as $language_row) : ?>
                                <option value="<?php echo $language_row->id; ?>"><?php echo $language_row->description; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="reset" class="btn btn-primary float-left" id="clear_filter_btn" name="clear_filter_btn" value="clear_filter">Clear Filter</button>
                        <button type="button" class="btn btn-primary float-right" id="filter_btn" name="filter_btn" value="filter">Filter</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-9 mt-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                My File
                <button type="button" class="btn btn-primary btn-sm" data-target="#upload-resume-modal" data-toggle="modal">Upload Resume</button>
                <button type="button" class="btn btn-primary btn-sm" id="similar_btn" name="similar_btn" value="similar">See Similarity</button>
                <button type="button" class="btn btn-primary btn-sm" id="clear_checkbox_btn" name="clear_checkbox_btn" value="clear_checkbox">Clear Checkbox</button>
            </h3>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped server-datatable" id="resume-datatable">
                    <thead>
                        <tr>
                            <th rowspan="2"></th>
                            <th class="numbering-column" rowspan="2">No.</th>
                            <th rowspan="2">Filename</th>
                            <th rowspan="2">CGPA</th>
                            <th rowspan="2">Field</th>
                            <th colspan="<?php echo count($field_of_study_list); ?>" class="text-center">Weightage (%)</th>
                            <th rowspan="2">Uploaded Datetime</th>
                            <th class="action-column" rowspan="2">Action</th>
                        </tr>
                        <tr>
                            <?php foreach ($field_of_study_list as $field_of_study_description) : ?>
                                <th><?php echo $field_of_study_description; ?></th>
                            <?php endforeach; ?>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resume-similarity-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Resume Similarity</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" id="similar-table">
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$this->load->view('resume/form');
$this->load->view('resume/upload');
?>

<script type="text/javascript">
    let field_of_study_list = <?php echo json_encode($field_of_study_list); ?>;
    let skill_list = <?php echo json_encode($skill_list); ?>;
    let resume_id = [];
    let button_click = false;

    $(function() {
        let resume_datatable = $('#resume-datatable').DataTable({
            //Server Side Process
            processing: true,
            serverSide: true,
            ajax: {
                type: 'GET',
                url: '<?php echo $this->site_url . 'get_list'; ?>',
                data: function(d) {
                    d.cgpa = $('#cgpa').val();
                    d.setting_field_of_study_id = $('#setting_field_of_study_id').val();
                    d.setting_skill_id = $('#setting_skill_id').val();
                    d.setting_language_id = $('#setting_language_id').val();
                    d.has_referee = $('#has_referee').is(':checked');
                    d.filter_button_click = button_click;
                    d.resume_id = resume_id;
                }
            },
            order: [
                [3, 'desc']
            ],
            //Other Config
            autoWidth: false,
            info: true,
            lengthChange: true,
            ordering: true,
            paging: true,
            searching: true,
            responsive: false,
            columns: [{
                    data: 'checkbox',
                    orderable: false,
                    searchable: false
                },
                {
                    data: null,
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'filename',
                    name: 'a.filename'
                },
                {
                    data: 'cgpa',
                    name: 'a.cgpa',
                    render: (data, type, row) => {
                        return data === null ? 'N/A' : data;
                    }
                },
                {
                    data: 'field_of_study_description',
                    name: 'b.description',
                    render: (data, type, row) => {
                        return data === null ? 'Unknown' : data;
                    }
                },
                <?php foreach ($field_of_study_list as $field_of_study_id => $field_of_study_description) : ?> {
                        className: 'fos_<?php echo $field_of_study_id; ?>',
                        data: '<?php echo str_replace(' ', '', strtolower($field_of_study_description)); ?>_weightage',
                        render: (data, type, row) => {
                            return data === null ? '' : data;
                        }
                    },
                <?php endforeach; ?> {
                    data: 'uploaded_datetime',
                    name: 'a.created_at',
                    orderable: true,
                    searchable: true,
                }, {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                }
            ],
            drawCallback: function(settings) {
                let datatable_api = this.api();

                datatable_api.column('.numbering-column', {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1 + datatable_api.page.info().start;
                });

                $('.title-tooltip').tooltip();
            }
        });

        $('#cgpa').slider({
            min: 2.00,
            max: 4.00,
            step: 0.01,
            value: [2.00, 4.00],
            orientation: 'horizontal',
            tooltip: 'always'
        });

        $('#setting_field_of_study_id').change(function(event) {
            //hide and show column
            // resume_datatable.columns('[class*="fos"]').visible(false)

            // $.each($(this).val(), (index, value) => {
            //     resume_datatable.columns(`.fos_${value}`).visible(true);
            // });

            //change dropdown option for skill
            let selected_field_of_study_value = $(this).val();

            $('#setting_skill_id').empty();
            let option_string = '';
            $.each(skill_list, (field_of_study_id, skill_row) => {
                if (selected_field_of_study_value.indexOf(field_of_study_id) !== -1) {
                    option_string += `<optgroup label="${field_of_study_list[field_of_study_id]}">\n`;
                    $.each(skill_row, (skill_id, skill_description) => {
                        option_string += `<option value="${skill_id}">${skill_description}</option>`;
                    });
                    option_string += '</optgroup>\n';
                }
            });

            $('#setting_skill_id').html(option_string);
        });

        $('#filter_btn').click(function(event) {
            button_click = true;
            reloadDatatable('resume-datatable');
        });

        $('#clear_filter_btn').click(function(event) {
            button_click = false;
            $('#cgpa').slider('setValue', [2.00, 4.00]);
            $('#setting_field_of_study_id').val('').trigger('change');
            $('#setting_language_id').val('').trigger('change');
            if ($('#has_referee').is(':checked')) {
                $('#has_referee').trigger('change');
            }
            reloadDatatable('resume-datatable');
        });

        $('#resume-datatable').on({
            change: function(event) {
                if ($(this).is(':checked')) {
                    resume_id.push(this.value);
                } else {
                    resume_id = resume_id.filter((value) => {
                        return value !== this.value
                    });
                }
            }
        }, '.resume_checkbox');

        $('#clear_checkbox_btn').click(function(event) {
            resume_id = [];
            reloadDatatable('resume-datatable');
        });

        $('#similar_btn').click(function() {
            if (resume_id.length > 2) {
                window.alert('Only 2 resumes can be compare.');
            } else if (resume_id.length != 2) {
                window.alert('Please select 2 resumes to be compare.');
            } else {
                $.ajax({
                    data: {
                        'resume_id': resume_id
                    },
                    dataType: 'json',
                    encode: true,
                    type: 'GET',
                    url: '<?php echo $this->site_url . 'compare'; ?>'
                }).done((data, textStatus, jqXHR) => {
                    if (data.status) {
                        let count = 1;
                        let size = data.data.length;
                        let table = '';
                        $.each(data.data, function(index, text) {
                            if (((count - 1) % 3) === 0) {
                                table += '<tr>';
                            }

                            table += `   <td>${text}</td>`;

                            if (count % 3 === 0 || count === size) {
                                table += '</tr>';
                            }

                            count++;
                        });

                        $('#similar-table tbody').empty().html(table);
                        $('#resume-similarity-modal').modal('show');
                    }
                }).fail((jqXHR, textStatus, errorThrown) => {
                    toastr['error']('Fail to compare resume.');
                });
            }
        });
    });
</script>
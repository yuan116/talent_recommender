<div class="col-lg-10 offset-lg-1">
    <div class="text-center">
        <h1 class="my-4 text-uppercase"><strong>WELCOME TO <?php echo $this->config->item('title'); ?></strong></h1>
    </div>
    <div class="card">
        <div class="card-body login-card-body p-0">
            <div class="row">
                <?php $this->load->view('login/left_form_image'); ?>
                <div class="col-lg-6">
                    <h3 class="text-center text-primary m-4"><u>Sign In</u></h3>

                    <?php if ($message = $this->session->flashdata('message')) : ?>
                        <div class="alert alert-<?php echo $message['type']; ?> alert-dismissible mx-3">
                            <?php echo $message['message']; ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo $this->site_url . 'sign_in'; ?>" method="post" class="mx-3">
                        <label for="email" class="control-label">Email</label>
                        <div class="form-group">
                            <input type="email" class="form-control <?php echo fieldIsInvalid('email'); ?>" id="email" name="email" value="<?php echo set_value('email'); ?>" />
                            <?php echo fieldInvalidFeedback('email'); ?>
                        </div>

                        <label for="password" class="control-label">Password</label>
                        <div class="form-group">
                            <input type="password" class="form-control <?php echo fieldIsInvalid('password'); ?>" id="password" name="password" value="<?php echo set_value('password'); ?>" />
                            <?php echo fieldInvalidFeedback('password'); ?>
                        </div>

                        <div class="form-group d-flex">
                            <div class="form-check col-xs-5">
                                <input type="checkbox" class="form-check-input" id="remember_me" name="remember_me" value="yes" />
                                <label class="form-check-label" for="remember_me">
                                    Remember Me
                                </label>
                            </div>

                            <button type="submit" class="ml-auto btn btn-primary" name="sign_in_btn" value="sign_in">Sign In</button>
                        </div>
                    </form>

                    <div class="mx-3 my-4">
                        Don't have account? <a href="<?php echo $this->site_url . 'sign_up'; ?>" class="card-link">Sign Up</a> <br />
                        <a href="<?php echo $this->site_url . 'forgot_password'; ?>" class="card-link">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-10 offset-lg-1">
    <div class="text-center">
        <h1 class="my-4 text-uppercase"><strong>WELCOME TO <?php echo $this->config->item('title'); ?></strong></h1>
    </div>
    <div class="card">
        <div class="card-body login-card-body">
            <h3 class="text-center text-primary m-4"><u>User Profile</u></h3>

            <form action="<?php echo current_url(); ?>" method="post">
                <div class="row">
                    <div class="col-lg-6">
                        <label for="name" class="control-label">Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control <?php echo fieldIsInvalid('name'); ?>" id="name" name="name" value="<?php echo set_value('name', $data->name); ?>" />
                            <?php echo fieldInvalidFeedback('name'); ?>
                        </div>

                        <label for="email" class="control-label">Email</label>
                        <div class="form-group">
                            <input type="email" class="form-control" value="<?php echo $data->email; ?>" disabled />
                        </div>

                        <label for="identity_number" class="control-label">Identity Number</label>
                        <div class="form-group">
                            <input type="text" class="form-control <?php echo fieldIsInvalid('identity_number'); ?>" id="identity_number" name="identity_number" value="<?php echo set_value('identity_number', $data->identity_number); ?>" />
                            <?php echo fieldInvalidFeedback('identity_number'); ?>
                        </div>

                        <label for="gender" class="control-label">Gender</label>
                        <div class="form-group">
                            <select class="form-control select2-select" id="gender" name="gender">
                                <?php foreach (GENDER_LIST as $value => $text) : ?>
                                    <option value="<?php echo $value; ?>" <?php echo setSelected('gender', $value, $data->gender); ?>><?php echo $text; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?php echo fieldInvalidFeedback('gender'); ?>
                        </div>

                        <label for="phone_number" class="control-label">Phone Number</label>
                        <div class="form-group">
                            <input type="text" class="form-control <?php echo fieldIsInvalid('phone_number'); ?>" id="phone_number" name="phone_number" value="<?php echo set_value('phone_number', $data->phone_number); ?>" />
                            <?php echo fieldInvalidFeedback('phone_number'); ?>
                        </div>

                        <label for="date_of_birth" class="control-label">Date of Birth</label>
                        <div class="form-group input-group date datepicker" id="date_of_birth_datepicker" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input <?php echo fieldIsInvalid('date_of_birth'); ?>" id="date_of_birth" name="date_of_birth" value="<?php echo set_value('date_of_birth', @$data->date_of_birth); ?>" />
                            <?php echo fieldInvalidFeedback('date_of_birth'); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <label for="address" class="control-label">Address</label>
                        <div class="form-group">
                            <textarea class="form-control <?php echo fieldIsInvalid('address'); ?>" id="address" name="address" rows="3"><?php echo set_value('address', $data->address); ?></textarea>
                            <?php echo fieldInvalidFeedback('address'); ?>
                        </div>

                        <label for="address" class="control-label">State</label>
                        <div class="form-group">
                            <select class="form-control select2-select" name="setting_state_id" id="setting_state_id">
                                <option value="">Please Select</option>
                                <?php foreach ($state_list as $row) : ?>
                                    <option value="<?php echo $row->id; ?>" <?php echo setSelected('setting_state_id', $row->id, $data->setting_state_id); ?>><?php echo $row->description; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <?php echo fieldInvalidFeedback('setting_state_id'); ?>
                        </div>

                        <label for="password" class="control-label">Password</label>
                        <div class="form-group">
                            <input type="password" class="form-control <?php echo fieldIsInvalid('password'); ?>" id="password" name="password" value="<?php echo set_value('password'); ?>" />
                            <?php echo fieldInvalidFeedback('password'); ?>
                        </div>

                        <label for="confirm_password" class="control-label">Confirm Password</label>
                        <div class="form-group">
                            <input type="password" class="form-control <?php echo fieldIsInvalid('confirm_password'); ?>" id="confirm_password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>" />
                            <?php echo fieldInvalidFeedback('confirm_password'); ?>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="activate_btn" value="activate">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
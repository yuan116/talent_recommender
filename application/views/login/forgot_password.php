<div class="col-lg-10 offset-lg-1">
    <div class="text-center">
        <h1 class="my-4 text-uppercase"><strong>WELCOME TO <?php echo $this->config->item('title'); ?></strong></h1>
    </div>
    <div class="card">
        <div class="card-body p-0">
            <div class="row">
                <?php $this->load->view('login/left_form_image'); ?>
                <div class="col-lg-6">
                    <h3 class="text-center text-primary m-4"><u>Forgot Password</u></h3>

                    <form action="<?php echo $this->site_url . 'forgot_password'; ?>" method="post" class="mx-3">
                        <label for="email" class="control-label">Email</label>
                        <div class="form-group">
                            <input type="email" class="form-control <?php echo fieldIsInvalid('email'); ?>" id="email" name="email" value="<?php echo set_value('email'); ?>" />
                            <?php echo fieldInvalidFeedback('email'); ?>
                        </div>

                        <div class="form-group">
                            <p class="form-text">System will send a link to the email for reset password.</p>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="forgot_password_btn" value="forgot_password">Submit</button>
                        </div>
                    </form>

                    <div class="mx-1 my-4">
                        Remember password? <a href="<?php echo $this->site_url . 'sign_in'; ?>" class="card-link">Sign In</a> <br />
                        Can't rememeber? <a href="<?php echo  $this->site_url . 'sign_up'; ?>" class="card-link">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-10 offset-lg-1">
    <div class="text-center">
        <h1 class="my-4 text-uppercase"><strong>WELCOME TO <?php echo $this->config->item('title'); ?></strong></h1>
    </div>
    <div class="card">
        <div class="card-body p-0">
            <div class="row">
                <?php $this->load->view('login/left_form_image'); ?>
                <div class="col-lg-6">
                    <h3 class="text-center text-primary m-4"><u>Sign Up</u></h3>

                    <?php if ($message = $this->session->flashdata('message')) : ?>
                        <div class="alert alert-<?php echo $message['type']; ?> alert-dismissible mx-3">
                            <?php echo $message['message']; ?>
                        </div>
                    <?php endif; ?>
                    <form action="<?php echo $this->site_url . 'sign_up'; ?>" method="post" class="mx-3">
                        <label for="company_name" class="control-label">Company Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control <?php echo fieldIsInvalid('company_name'); ?>" id="company_name" name="company_name" value="<?php echo set_value('company_name', $this->session->tempdata('company_name')); ?>" />
                            <?php echo fieldInvalidFeedback('company_name'); ?>
                        </div>

                        <label for="name" class="control-label">Name</label>
                        <div class="form-group">
                            <input type="text" class="form-control <?php echo fieldIsInvalid('name'); ?>" id="name" name="name" value="<?php echo set_value('name', $this->session->tempdata('name')); ?>" />
                            <?php echo fieldInvalidFeedback('name'); ?>
                        </div>

                        <label for="email" class="control-label">Email</label>
                        <div class="form-group">
                            <input type="email" class="form-control <?php echo fieldIsInvalid('email'); ?>" id="email" name="email" value="<?php echo set_value('email', $this->session->tempdata('email')); ?>" />
                            <?php echo fieldInvalidFeedback('email'); ?>
                        </div>
                        <div class="form-group">
                            <p class="form-text">System will send a link to the email for verify and create account.</p>
                        </div>

                        <div class="form-group d-flex">
                            <?php if ($this->session->tempdata('resend_btn')) : ?>
                                <button type="submit" class="ml-auto btn btn-primary" name="resend_btn" value="resend">Resend</button>
                            <?php else : ?>
                                <button type="submit" class="ml-auto btn btn-primary" name="sign_up_btn" value="sign_up">Sign Up</button>
                            <?php endif; ?>
                        </div>
                    </form>

                    <div class="mx-1 my-4">
                        Have account? <a href="<?php echo $this->site_url . 'sign_in'; ?>" class="card-link">Sign In</a> <br />
                        <a href="<?php echo $this->site_url . 'forgot_password'; ?>" class="card-link">Forgot password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-10 offset-lg-1">
    <div class="text-center">
        <h1 class="my-4 text-uppercase"><strong>WELCOME TO <?php echo $this->config->item('title'); ?></strong></h1>
    </div>
    <div class="card">
        <div class="card-body login-card-body p-0">
            <div class="row">
                <?php $this->load->view('login/left_form_image'); ?>
                <div class="col-lg-6">
                    <h3 class="text-center text-primary m-4"><u>Reset Password</u></h3>

                    <form action="<?php echo $this->site_url . 'reset_password'; ?>" method="post" class="mx-3">
                        <label for="password" class="control-label">Password</label>
                        <div class="form-group">
                            <input type="password" class="form-control <?php echo fieldIsInvalid('password'); ?>" id="password" name="password" value="<?php echo set_value('password'); ?>" />
                            <?php echo fieldInvalidFeedback('password'); ?>
                        </div>

                        <label for="confirm_password" class="control-label">Confirm Password</label>
                        <div class="form-group">
                            <input type="password" class="form-control <?php echo fieldIsInvalid('confirm_password'); ?>" id="confirm_password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>" />
                            <?php echo fieldInvalidFeedback('confirm_password'); ?>
                        </div>

                        <div class="form-group d-flex">
                            <button type="submit" class="ml-auto btn btn-primary" name="reset_password_btn" value="reset_password">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
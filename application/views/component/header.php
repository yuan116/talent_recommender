<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->config->item('title'); ?></title>

    <?php
    /* Application styling */
    echo htmlLink($this->base_url . 'assets/app.css');

    /* JQuery */
    echo htmlScript($this->base_url . 'assets/plugins/jquery/jquery.min.js');
    ?>
</head>
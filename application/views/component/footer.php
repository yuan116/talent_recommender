<?php
/* ---- SCRIPT ----- */
/* JQuery UI 1.11.4 */
// echo htmlScript($this->base_url . 'assets/plugins/jquery-ui/jquery-ui.min.js');

/* Bootstrap 4 */
echo htmlScript($this->base_url . 'assets/plugins/bootstrap/js/bootstrap.bundle.min.js');
/* Bootstrap4 Duallistbox */
// echo htmlScript($this->base_url . 'assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js');
/* Bootstrap Color Picker */
// echo htmlScript($this->base_url . 'assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js');
/* Bootstrap Custom File Input */
echo htmlScript($this->base_url . 'assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js');
/* Bootstrap Slider */
echo htmlScript($this->base_url . 'assets/plugins/bootstrap-slider/bootstrap-slider.min.js');
/* Bootstrap Switch */
echo htmlScript($this->base_url . 'assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js');

/* ChartJS */
// echo htmlScript($this->base_url . 'assets/plugins/chart.js/Chart.min.js');

/* DataTables */
echo htmlScript($this->base_url . 'assets/plugins/datatables/jquery.dataTables.min.js');
echo htmlScript($this->base_url . 'assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');
echo htmlScript($this->base_url . 'assets/plugins/datatables-responsive/js/dataTables.responsive.min.js');
echo htmlScript($this->base_url . 'assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');
/* Daterange Picker */
// echo htmlScript($this->base_url . 'assets/plugins/daterangepicker/daterangepicker.js);

/* Ekko Lightbox */
// echo htmlScript($this->base_url . 'assets/plugins/ekko-lightbox/ekko-lightbox.min.js');

/* Filterizr */
// echo htmlScript($this->base_url . 'assets/plugins/filterizr/jquery.filterizr.min.js');
/* Flot Chart */
// echo htmlScript($this->base_url . 'assets/plugins/flot/jquery.flot.js');
/* Flot Pie Plugin - also used to draw donut charts */
// echo htmlScript($this->base_url . 'assets/plugins/flot-old/jquery.flot.pie.min.js');
/* Flot Resize Plugin - allows the chart to redraw when the window is resized */
// echo htmlScript($this->base_url . 'assets/plugins/flot-old/jquery.flot.resize.min.js');
/* FullCalendar */
// echo htmlScript($this->base_url . 'assets/plugins/fullcalendar/main.min.js');
// echo htmlScript($this->base_url . 'assets/plugins/fullcalendar-daygrid/main.min.js');
// echo htmlScript($this->base_url . 'assets/plugins/fullcalendar-timegrid/main.min.js');
// echo htmlScript($this->base_url . 'assets/plugins/fullcalendar-interaction/main.min.js');
// echo htmlScript($this->base_url . 'assets/plugins/fullcalendar-bootstrap/main.min.js');

/* InputMask */
// echo htmlScript($this->base_url . 'assets/plugins/inputmask/min/jquery.inputmask.bundle.min.js');
/* Ion Slider */
// echo htmlScript($this->base_url . 'assets/plugins/ion-rangeslider/js/ion.rangeSlider.min.js');

/* Jquery Validation */
echo htmlScript($this->base_url . 'assets/plugins/jquery-validation/jquery.validate.min.js');
echo htmlScript($this->base_url . 'assets/plugins/jquery-validation/additional-methods.min.js');
/* JQVMap */
//echo htmlScript($this->base_url . 'assets/plugins/jqvmap/jquery.vmap.min.js'); 
/* JQuery Knob Chart */
// echo htmlScript($this->base_url . 'assets/plugins/jquery-knob/jquery.knob.min.js');
/* JQuery MouseWheel */
// echo htmlScript($this->base_url . 'assets/plugins/jquery-mousewheel/jquery.mousewheel.js');
/* JQuery Mapael */
// echo htmlScript($this->base_url . 'assets/plugins/raphael/raphael.min.js');
// echo htmlScript($this->base_url . 'assets/plugins/jquery-mapael/jquery.mapael.min.js');
// echo htmlScript($this->base_url . 'assets/plugins/jquery-mapael/maps/usa_states.min.js');
/* JSGrid */
// echo htmlScript($this->base_url . 'assets/plugins/jsgrid/jsgrid.min.js');

/* Moment */
echo htmlScript($this->base_url . 'assets/plugins/moment/moment.min.js');

/* OverlayScrollbars */
// echo htmlScript($this->base_url . 'assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');

/* Pace Progress */
// echo htmlScript($this->base_url . 'assets/plugins/pace-progress/pace.min.js');

/* Select2 */
echo htmlScript($this->base_url . 'assets/plugins/select2/js/select2.full.min.js');
/* Sparkline */
// echo htmlScript('assets/plugins/sparklines/sparkline.js');
/* SweetAlert2 */
echo htmlScript($this->base_url . 'assets/plugins/sweetalert2/sweetalert2.min.js');
/* Summernote */
echo htmlScript($this->base_url . 'assets/plugins/summernote/summernote-bs4.min.js');

/* Tempusdominus Bootstrap 4 */
echo htmlScript($this->base_url . 'assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');
/* Toastr */
echo htmlScript($this->base_url . 'assets/plugins/toastr/toastr.min.js');

/* AdminLTE App */
echo htmlScript($this->base_url . 'assets/dist/js/adminlte.js');
/* AdminLTE dashboard demo (This is only for demo purposes) */
// echo htmlScript($this->base_url . 'assets/dist/js/pages/dashboard.js');
// echo htmlScript($this->base_url . 'assets/dist/js/pages/dashboard2.js');
// echo htmlScript($this->base_url . 'assets/dist/js/pages/dashboard3.js');
// echo htmlScript($this->base_url . 'assets/dist/js/demo.js');
?>

<script type="text/javascript">
    $(document).on({
        ajaxStart: function(event) {
            if ($('body').children('div.overlay-wrapper').length === 0) {
                $('body').append(`
                <div class="overlay-wrapper">
                    <div class="overlay">
                        <span class="fas fa-2x fa-sync-alt fa-spin"></span>
                    </div>
                </div>`);
            }
        },
        ajaxStop: function(event) {
            $('body').children('div.overlay-wrapper').remove();
        }
    });

    /* Resolve conflict in jQuery UI tooltip with Bootstrap tooltip */
    // $.widget.bridge('uibutton', $.ui.button)

    //This is similar to $(document).ready(function())
    $(function() {
        //Element with class alert-dismissiable will prepend close button;
        $('.alert-dismissible').prepend('<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>');

        //Auto add autocomplete attribute and append crsf field to form
        $('form').attr('autocomplete', 'off')
            .append(`<?php echo csrfField(); ?>`);

        $('input[type="password"]').after(`
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-eye icon-clickable password-icon" title="Show Password"></span>
                </div>
            </div>`)
            .closest('div.form-group').addClass('input-group');

        //Hide or Show Password
        $('.password-icon').click(function(event) {
            let password_input = $(this).closest('div.input-group').children('input');

            if (password_input.attr('type') === 'password') {
                $(this).removeClass('fa-eye').addClass('fa-eye-slash')
                    .attr('data-original-title', 'Hide Password');
                password_input.attr('type', 'text');
            } else {
                $(this).removeClass('fa-eye-slash').addClass('fa-eye')
                    .attr('data-original-title', 'Show Password')
                password_input.attr('type', 'password');
            }
        });

        //Datepicker Config
        $('.datepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });

        //Element with class icon-clickable will have tooltip
        $('.icon-clickable, .title-tooltip').tooltip();

        $('.tab-pane.active').addClass('show');

        //Bootstrap Switch Config
        $('input[type="checkbox"].bootstrap-switch-checkbox').bootstrapSwitch();

        //Bootstrap File Input
        bsCustomFileInput.init();

        //Select2 Config
        $('.select2-select').select2({
            width: '100%',
            dropdownAutoWidth: true
        });

        //Summernote Config
        $('.summernote-textarea').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['fontStyle', ['style', 'bold', 'italic', 'underline', 'clear', 'strikethrough', 'superscript', 'subscript', 'fontname', 'fontsize']],
                ['color', ['forecolor', 'backcolor']],
                ['para', ['ul', 'ol', 'paragraph', 'height']],
                ['insert', [ /* 'picture',  */ 'link', /*  'video',  */ 'hr', 'table']],
                ['view', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
            ],
            codemirror: {
                theme: 'monokai'
            },
            height: 350
        });

        //Toastr Config
        toastr.options = {
            closeButton: false,
            debug: false,
            newestOnTop: true,
            progressBar: true,
            positionClass: 'toast-bottom-right',
            preventDuplicates: false,
            onclick: null,
            showDuration: '300',
            hideDuration: '1000',
            timeOut: '5000',
            extendedTimeOut: '1000',
            showEasing: 'swing',
            hideEasing: 'linear',
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut'
        }

        //Show Toastr
        <?php if ($flash_toastr_list = $this->session->flashdata('toastr_message')) : ?>
            <?php foreach ($flash_toastr_list as $row) : ?>
                toastr['<?php echo $row['type']; ?>']('<?php echo $row['message']; ?>');
            <?php endforeach; ?>
        <?php endif; ?>

        $('table').addClass('table-sm');

        $('table.server-datatable').on({
            click: function(event) {
                event.preventDefault();

                Swal.fire({
                    title: 'Are you sure to delete this record?',
                    showCancelButton: true,
                    cancelButtonText: 'No',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            dataType: 'json',
                            data: {
                                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                            },
                            encode: true,
                            type: 'DELETE',
                            url: $(this).attr('href')
                        }).done((data, textStatus, jqXHR) => {
                            if (data.status) {
                                try {
                                    toastr['success'](data.message);
                                    reloadDatatable($(this).closest('table.server-datatable').attr('id'));
                                } catch (error) {
                                    setTimeout(() => {
                                        window.location.reload();
                                    }, 2000)
                                }
                            } else {
                                toastr['error'](data.message);
                            }
                        }).fail((jqXHR, textStatus, errorThrown) => {
                            toastr['error']('Fail to delete record.');
                        });
                    }
                });
            }
        }, '.list-delete-btn');

        $('table.normal-datatable').DataTable({
            processing: false,
            serverSide: false,
            order: [
                [1, 'asc']
            ],
            //Other Config
            autoWidth: false,
            info: true,
            lengthChange: true,
            ordering: true,
            paging: true,
            searching: true,
            responsive: false,
            columnDefs: [{
                    targets: 'numbering-column',
                    orderable: false,
                    searchable: false,
                    width: '10px'
                },
                {
                    targets: 'action-column',
                    orderable: false,
                    searchable: false
                }
            ],
            drawCallback: function(settings) {
                let normal_datatable_api = this.api();

                normal_datatable_api.column(0, {
                    search: 'applied',
                    order: 'applied'
                }).nodes().each(function(cell, i) {
                    cell.innerHTML = i + 1
                });
            }
        });

        $('table.normal-datatable').on({
            click: function(event) {
                event.preventDefault();

                Swal.fire({
                    title: 'Are you sure to delete this record?',
                    showCancelButton: true,
                    cancelButtonText: 'No',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            dataType: 'json',
                            data: {
                                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                            },
                            encode: true,
                            type: 'DELETE',
                            url: $(this).attr('href')
                        }).done((data, textStatus, jqXHR) => {
                            if (data.status) {
                                toastr['success'](data.message);
                                setTimeout(() => {
                                    window.location.reload();
                                }, 2000)
                            } else {
                                toastr['error'](data.message);
                            }
                        }).fail((jqXHR, textStatus, errorThrown) => {
                            toastr['error']('Fail to delete record.');
                        });
                    }
                });
            }
        }, '.list-delete-btn');

        $('.label-required').append(' <span class="text-danger">*</span>');
    });

    function reloadDatatable(element_id) {
        $(`#${element_id}`).DataTable().ajax.reload();
    }
</script>
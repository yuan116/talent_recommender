
<?php

require_once BASEPATH . 'core/Controller.php';
$ci = get_instance() ?? new MY_Controller;

$data['heading'] = $heading;
$data['message'] = $message;
$data['content'] = $ci->load->view('template/error_404', $data, TRUE);

if ($ci->session->loggedin) {
    echo $ci->load->view('template/inner', $data, TRUE);
} else {
    echo $ci->load->view('template/outer', $data, TRUE);
}

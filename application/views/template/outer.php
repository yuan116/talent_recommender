<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('component/header'); ?>

<body class="hold-transition layout-navbar-fixed layout-top-nav">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div class="container">
                <a href="<?php echo $this->site_url; ?>" class="navbar-brand">
                    <!-- <img src="" alt="Site Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
                    <span class="brand-text font-weight-light"><?php echo $this->config->item('title'); ?></span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav">
                        <li class="nav-item text-center">
                            <a href="<?php echo $this->site_url . 'home'; ?>" class="nav-link <?php echo setTabActive('home', $this->active_menu); ?>">Home</a>
                        </li>
                        <li class="nav-item text-center">
                            <a href="<?php echo $this->site_url . 'contact_us'; ?>" class="nav-link <?php echo setTabActive('contact_us', $this->active_menu); ?>">Contact Us</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item text-center">
                            <a href="<?php echo $this->site_url . 'sign_in'; ?>" class="nav-link <?php echo setTabActive('sign_in', $this->active_menu); ?>">Sign In</a>
                        </li>
                        <li class="nav-item text-center">
                            <a href="<?php echo $this->site_url . 'sign_up'; ?>" class="nav-link <?php echo setTabActive('sign_up', $this->active_menu); ?>">Sign Up</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="position-absolute" id="website-background"></div>

        <div class="content-wrapper">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>

        <footer class="main-footer">
            <div class="container">
                <div class="float-right d-none d-sm-inline">
                </div>
                <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"><?php echo $this->config->item('title'); ?></a>.</strong> All rights reserved.
            </div>
        </footer>
    </div>

    <?php $this->load->view('component/footer'); ?>
</body>

</html>
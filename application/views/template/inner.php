<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('component/header'); ?>

<body class="hold-transition layout-navbar-fixed layout-top-nav">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
            <div class="container">
                <a href="<?php echo site_url(); ?>" class="navbar-brand">
                    <!-- <img src="" alt="Site Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
                    <span class="brand-text font-weight-light"><?php echo $this->config->item('title'); ?></span>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav">
                        <li class="nav-item text-center">
                            <a href="<?php echo site_url('home'); ?>" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item text-center">
                            <a href="<?php echo site_url('contact_us'); ?>" class="nav-link">Contact Us</a>
                        </li>
                    </ul>

                    <ul class="navbar-nav ml-auto">
                        <?php if (in_array($this->session->user_type_id, [USER_TYPE_ADMINISTRATOR, USER_TYPE_STAFF])) : ?>
                            <li class="nav-item text-center">
                                <a href="<?php echo site_url('announcement'); ?>" class="nav-link">Announcement</a>
                            </li>
                            <li class="nav-item text-center">
                                <a href="<?php echo site_url('company'); ?>" class="nav-link">Company</a>
                            </li>
                            <li class="nav-item text-center">
                                <a href="<?php echo site_url('user'); ?>" class="nav-link">User</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a href="<?php echo site_url('setting'); ?>" class="nav-link">Setting</a>
                            </li>
                        <?php elseif ($this->session->user_type_id == USER_TYPE_EMPLOYER) : ?>
                            <li class="nav-item dropdown">
                                <a href="<?php echo site_url('resume'); ?>" class="nav-link">Resume</a>
                            </li>
                        <?php endif; ?>
                        <li class="nav-item dropdown user-menu">
                            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                <img src="<?php echo $this->base_url . $this->session->profile_pic; ?>" class="user-image img-circle elevation-2" alt="User Profile Picture" />
                                <span class="d-none d-md-inline"><?php echo $this->session->name; ?></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px;">
                                <li class="user-header bg-default">
                                    <img src="<?php echo $this->base_url . $this->session->profile_pic; ?>" class="img-circle elevation-2" alt="User Profile Picture" />
                                    <p>
                                        <?php echo $this->session->name; ?>
                                    </p>
                                </li>
                                <?php if ($this->session->has_nav_user_body) : ?>
                                    <li class="user-body">
                                        <div class="row">
                                            <?php if ($this->session->user_type_id == USER_TYPE_EMPLOYER) : ?>
                                                <div class="col-4 text-center">
                                                    <a href="<?php echo site_url('company_profile'); ?>">Company Profile</a>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <li class="user-footer">
                                    <a href="<?php echo site_url('user_profile'); ?>" class="btn btn-default btn-flat">Update Profile</a>
                                    <form action="<?php echo site_url('logout'); ?>" method="post" class="float-right">
                                        <button type="submit" class="btn btn-default btn-flat" name="logout_btn" value="logout">Logout</button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="position-absolute" id="website-background"></div>

        <div class="content-wrapper">
            <div class="content">
                <div class="container">
                    <div class="row">
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </div>

        <footer class="main-footer">
            <div class="container">
                <div class="float-right d-none d-sm-inline">
                </div>
                <strong>Copyright &copy; <?php echo date('Y'); ?> <a href="#"><?php echo $this->config->item('title'); ?></a>.</strong> All rights reserved.
            </div>
        </footer>
    </div>

    <?php $this->load->view('component/footer'); ?>
</body>

</html>
<div class="col-lg-10 offset-lg-1 mt-5 pt-5">
    <div class="card card-default mt-5 py-5">
        <div class="error-page">
            <h2 class="headline text-danger">500</h2>

            <div class="error-content px-3">
                <h3><span class="fas fa-exclamation-triangle text-danger"></span>Oops! Something went wrong.</h3>

                <?php echo $message; ?>
                <p>
                    We will work on fixing that right away.
                    Meanwhile, you may <a href="<?php echo site_url('home'); ?>">return to home</a> <!-- or try using the search form -->.
                </p>

                <!-- <form class="search-form" method="post"> -->
                <!-- <div class="input-group"> -->
                <!-- <input type="text" name="search" class="form-control" placeholder="Search"> -->

                <!-- <div class="input-group-append"> -->
                <!-- <button type="submit" name="submit" class="btn btn-warning"><i class="fas fa-search"></i> -->
                <!-- </button> -->
                <!-- </div> -->
                <!-- </div> -->
                <!-- </form> -->
            </div>
        </div>
    </div>
</div>
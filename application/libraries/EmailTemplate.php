<?php

defined('BASEPATH') or exit('No direct script access allowed');

class EmailTemplate
{
    public function activation(): string
    {
        return trim('
            Welcome to {__TITLE__}. <br /><br />
            Hi {__NAME__},<br /><br />
            Click below link to proceed register and activate your account. <br />
            <a href="{__LINK__}" target="_blank">{__TITLE__} Register & Activate Account</a> <br /><br />
            If above link is does not work, copy the following link to your browser address bar. <br />
            <a href="{__LINK__}" target="_blank">{__LINK__}</a>
        ');
    }

    public function forgotPassword(): string
    {
        return trim('
            Hi {__NAME__},<br /><br />
            It seem like you have forgot your password.<br />
            Click below link to reset your password.<br />
            <a href="{__LINK__}">{__TITLE__} Reset Password</a> <br /><br />
            If above link is does not work, copy the following link to your browser address bar. <br />
            <a href="{__LINK__}" target="_blank">{__LINK__}</a>
        ');
    }

    public function changeEmail(): string
    {
        return trim('
            Hi {__NAME__},<br /><br />
            You are going to change email for {__TITLE__} login access.<br />
            Click below link to verify and change your email.<br />
            <a href="{__LINK__}">{__TITLE__} Change Email</a> <br /><br />
            If above link is does not work, copy the following link to your browser address bar. <br />
            <a href="{__LINK__}" target="_blank">{__LINK__}</a> <br /><br />
            If you do not remember changing your email, please kindly ignore and delete this email.
        ');
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CompanyRule extends Rule
{
    public function getRules(): array
    {
        return [
            'index' => $this->index()
        ];
    }

    public function index(): array
    {
        return [
            [
                'field' => 'company_name',
                'label' => 'Name',
                'rules' => "trim|required|callback_checkCompanyNameExist[{$this->data['company_id']}]"
            ],
            [
                'field' => 'company_email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ],
            [
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|numeric'
            ],
            [
                'field' => 'incorporate_date',
                'label' => 'Incorporate Date',
                'rules' => 'trim'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim'
            ],
            [
                'field' => 'setting_state_id',
                'label' => 'State',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'other_info',
                'label' => 'Other Info',
                'rules' => 'trim'
            ]
        ];
    }
}

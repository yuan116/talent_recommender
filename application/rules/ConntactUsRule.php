<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ConntactUsRule extends Rule
{
    public function getRules(): array
    {
        return [
            'index' => $this->index()
        ];
    }

    public function index(): array
    {
        return [
            [
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|numeric'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim'
            ],
            [
                'field' => 'address_iframe',
                'label' => 'Address Iframe',
                'rules' => 'trim'
            ]
        ];
    }
}

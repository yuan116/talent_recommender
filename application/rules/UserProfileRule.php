<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserProfileRule extends Rule
{
    public function getRules(): array
    {
        return [
            'profile' => $this->profile(),
            'email' => $this->email(),
            'emailPassword' => $this->emailPassword(),
            'password' => $this->password()
        ];
    }

    public function profile(): array
    {
        return array_merge($this->currentPassword('profile_password'), [
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'identity_number',
                'label' => 'Identity Number',
                'rules' => 'trim|required|numeric'
            ],
            [
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ],
            [
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|numeric|required'
            ],
            [
                'field' => 'date_of_birth',
                'label' => 'Date of Birth',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'setting_state_id',
                'label' => 'State',
                'rules' => 'trim|required'
            ]
        ]);
    }

    public function email(): array
    {
        return [[
            'field' => 'email',
            'label' => 'New Email',
            'rules' => 'trim|required|valid_email|callback_checkEmailExist'
        ]];
    }

    public function emailPassword(): array
    {
        return array_merge($this->email(), $this->currentPassword('email_password'));
    }

    public function password(): array
    {
        return array_merge($this->currentPassword('current_password'), [
            [
                'field' => 'new_password',
                'label' => 'New Password',
                'rules' => 'trim|required|min_length[5]|max_length[15]|alpha_numeric'
            ],
            [
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[new_password]'
            ]
        ]);
    }

    protected function currentPassword(string $field): array
    {
        return [[
            'field' => $field,
            'label' => 'Current Password',
            'rules' => 'trim|required|callback_checkCurrentPassword'
        ]];
    }
}

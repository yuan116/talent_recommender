<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FieldOfStudyRule extends Rule
{
    public function getRules(): array
    {
        return [
            'index' => $this->index()
        ];
    }

    public function index()
    {
        return [[
            'field' => 'description',
            'label' => 'Description',
            'rules' => 'trim|required'
        ]];
    }
}

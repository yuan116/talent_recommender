<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LoginRule extends Rule
{
    public function getRules(): array
    {
        return [
            'signUp' => $this->signUp(),
            'activate' => $this->activate(),
            'signIn' => $this->signIn(),
            'forgotPassword' => $this->forgotPassword(),
            'resetPassword' => $this->resetPassword()
        ];
    }

    public function signUp(): array
    {
        return [
            [
                'field' => 'company_name',
                'label' => 'Company Name',
                'rules' => 'trim|required|callback_checkCompanyNameExist'
            ],
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|callback_checkEmailExist'
            ]
        ];
    }

    public function activate(): array
    {
        return [
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'identity_number',
                'label' => 'Identity Number',
                'rules' => 'trim|required|numeric'
            ],
            [
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ],
            [
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|required|numeric'
            ],
            [
                'field' => 'date_of_birth',
                'label' => 'Date of Birth',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'setting_state_id',
                'label' => 'State',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[5]|max_length[15]|alpha_numeric'
            ],
            [
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[password]'
            ]
        ];
    }

    public function signIn(): array
    {
        return [
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|callback_checkUserEmailExist'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required'
            ]
        ];
    }

    public function forgotPassword(): array
    {
        return [[
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|callback_checkEmailExist'
        ]];
    }

    public function resetPassword(): array
    {
        return [
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[5]|max_length[12]|alpha_numeric'
            ],
            [
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[password]'
            ]
        ];
    }
}

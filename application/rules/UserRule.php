<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserRule extends Rule
{
    public function getRules(): array
    {
        return [
            'user' => $this->user(),
            'employer' => $this->employer()
        ];
    }

    public function user(): array
    {
        return [
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => "trim|required|valid_email|callback_checkEmailExist[{$this->data['user_id']}]"
            ],
            [
                'field' => 'identity_number',
                'label' => 'Identity Number',
                'rules' => 'trim|numeric'
            ],
            [
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ],
            [
                'field' => 'identity_number',
                'label' => 'Identity Number',
                'rules' => 'trim|numeric'
            ],
            [
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|numeric'
            ],
            [
                'field' => 'date_of_birth',
                'label' => 'Date of Birth',
                'rules' => 'trim'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim'
            ],
            [
                'field' => 'setting_state_id',
                'label' => 'State',
                'rules' => 'trim|required'
            ],
        ];
    }

    public function employer(): array
    {
        return array_merge($this->user(), [[
            'field' => 'company_id',
            'label' => 'Company',
            'rules' => 'trim|required'
        ]]);
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CompanyProfileRule extends Rule
{
    public function getRules(): array
    {
        return [
            'index' => $this->index(),
            'inviteUser' => $this->inviteUser()
        ];
    }

    public function index(): array
    {
        return [
            [
                'field' => 'company_name',
                'label' => 'Name',
                'rules' => "trim|required|callback_checkCompanyNameExist[{$this->data['company_id']}]"
            ],
            [
                'field' => 'company_email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email'
            ],
            [
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'trim|required|numeric'
            ],
            [
                'field' => 'incorporate_date',
                'label' => 'Incorporate Date',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'setting_state_id',
                'label' => 'State',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'other_info',
                'label' => 'Other Info',
                'rules' => 'trim'
            ]
        ];
    }

    public function inviteUser(): array
    {
        return [
            [
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|callback_checkEmailExist'
            ]
        ];
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AnnouncementRule extends Rule
{
    public function getRules(): array
    {
        return [
            'index' => $this->index()
        ];
    }

    public function index(): array
    {
        return [
            [
                'field' => 'title',
                'label' => 'Title',
                'rules' => 'trim|required'
            ],
            [
                'field' => 'content',
                'label' => 'Content',
                'rules' => 'trim'
            ]
        ];
    }
}

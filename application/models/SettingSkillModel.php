<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingSkillModel extends MY_Model
{
    protected string $table = 'setting_skill';
    protected string $primary_key = 'id';
    protected bool $has_timestamp = TRUE;
    protected bool $soft_delete = TRUE;
    protected array $proctected_fields = ['id'];

    public function __construct()
    {
        parent::__construct();
    }

    public function getList()
    {
        return $this->select('id, description')->result();
    }

    public function getListByFieldOfStudy($field_of_study_id)
    {
        $this->from("{$this->table} AS a");
        $this->join('setting_field_of_study_skill_rel AS b', 'a.id = b.setting_skill_id');

        return $this->getValuePairList(
            ['a.id', 'id'],
            ['a.description', 'description'],
            ['b.setting_field_of_study_id' => $field_of_study_id]
        );
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ResumeModel extends MY_Model
{
    protected string $table = 'resume';
    protected string $primary_key = 'id';
    protected bool $has_timestamp = TRUE;
    protected bool $soft_delete = TRUE;
    protected array $proctected_fields = ['id'];

    public function __construct()
    {
        parent::__construct();
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AnnouncementModel extends MY_Model
{
    protected string $table = 'announcement';
    protected string $primary_key = 'id';
    protected bool $has_timestamp = TRUE;
    protected bool $soft_delete = TRUE;
    protected array $proctected_fields = ['id'];

    public function __construct()
    {
        parent::__construct();
    }
}

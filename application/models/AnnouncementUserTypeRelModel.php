<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AnnouncementUserTypeRelModel extends MY_Model
{
    protected string $table = 'announcement_user_type_rel';
    protected string $primary_key = '';
    protected bool $has_timestamp = FALSE;
    protected bool $soft_delete = FALSE;
    // protected array $proctected_fields = [];

    public function __construct()
    {
        parent::__construct();
    }
}

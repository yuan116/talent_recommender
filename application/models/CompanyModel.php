<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CompanyModel extends MY_Model
{
    protected string $table = 'company';
    protected string $primary_key = 'id';
    protected bool $has_timestamp = TRUE;
    protected bool $soft_delete = TRUE;
    protected array $proctected_fields = ['id'];

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($company_id)
    {
        return $this->select('id, company_name, company_email, phone_number, incorporate_date, address, setting_state_id, other_info, company_logo')->row($company_id);
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserTypeModel extends MY_Model
{
    protected string $table = 'user_type';
    protected string $primary_key = 'id';
    protected bool $has_timestamp = FALSE;
    protected bool $soft_delete = FALSE;
    // protected array $proctected_fields = [];

    public function __construct()
    {
        parent::__construct();
    }
}

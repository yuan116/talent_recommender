<?php

defined('BASEPATH') or exit('No direct script access allowed');

class EmailVerificationModel extends MY_Model
{
    protected string $table = 'email_verification';
    protected string $primary_key = '';
    protected bool $has_timestamp = FALSE;
    protected bool $soft_delete = FALSE;
    // protected array $proctected_fields = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function getEmailData(array $where)
    {
        return $this->select('a.user_id, a.token, a.data, b.name')
            ->from("{$this->table} AS a")
            ->join('user AS b', 'a.user_id = b.id')
            ->rowArray($where);
    }
}

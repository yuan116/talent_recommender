<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserModel extends MY_Model
{
    protected string $table = 'user';
    protected string $primary_key = 'id';
    protected bool $has_timestamp = TRUE;
    protected bool $soft_delete = TRUE;
    protected array $proctected_fields = ['id'];

    public function __construct()
    {
        parent::__construct();
    }

    public function emailExist(string $email, ?bool $activated = NULL, $user_id = NULL): bool
    {
        if (is_bool($activated)) {
            $this->where('activated', $activated);
        }

        if ($user_id !== NULL) {
            $this->where('id !=', $user_id);
        }

        return $this->dataSeek(['email' => $email]);
    }

    public function getPassword(array $where): string
    {
        return $this->select('password')->where('activated', TRUE)->row($where)->password ?? '';
    }

    public function getLoginData($email)
    {
        return $this->select('id, password')->rowArray([
            'email' => $email,
            'activated' => TRUE
        ]);
    }

    public function getData($user_id)
    {
        return $this->select('id, user_type_id, name, email, identity_number, gender, phone_number, date_of_birth, address, setting_state_id, company_id')->row($user_id);
    }

    public function getLoginSessiondata($user_id)
    {
        return $this->select('id AS user_id, user_type_id, name, email, company_id, profile_pic, 1 AS loggedin')->rowArray($user_id);
    }
}

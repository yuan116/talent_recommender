<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingLanguageModel extends MY_Model
{
    protected string $table = 'setting_language';
    protected string $primary_key = 'id';
    protected bool $has_timestamp = TRUE;
    protected bool $soft_delete = TRUE;
    // protected array $proctected_fields = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function getList()
    {
        return $this->select('id, description')->result();
    }
}

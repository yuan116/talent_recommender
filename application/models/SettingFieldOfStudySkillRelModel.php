<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingFieldOfStudySkillRelModel extends MY_Model
{
    protected string $table = 'setting_field_of_study_skill_rel';
    protected string $primary_key = '';
    protected bool $has_timestamp = FALSE;
    protected bool $soft_delete = FALSE;
    // protected array $proctected_fields = [];

    public function __construct()
    {
        parent::__construct();
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('isPost')) {
    function isPost(string $key = ''): bool
    {
        return empty($key) ? get_instance()->input->method() === 'post' : isset($_POST[$key]);
    }
}

if (!function_exists('isGet')) {
    function isGet(string $key = ''): bool
    {
        return empty($key) ? get_instance()->input->method() === 'get' : isset($_GET[$key]);
    }
}

if (!function_exists('setChecked')) {
    function setChecked(string $field, $field_value, $form_value = NULL): string
    {
        return set_checkbox($field, $field_value, $field_value == $form_value);
    }
}

if (!function_exists('setSelected')) {
    function setSelected(string $field, $field_value, $form_value = NULL): string
    {
        return set_select($field, $field_value, $field_value == $form_value);
    }
}

if (!function_exists('setMultiSelected')) {
    function setMultiSelected(string $field, $field_value, ?array $form_value = NULL): string
    {
        return set_select($field, $field_value, in_array($field_value, (array) $form_value));
    }
}

if (!function_exists('fieldIsInvalid')) {
    function fieldIsInvalid(string $field): string
    {
        static $field_error_message_list = [];

        if (isset($field_error_message_list[$field])) {
            return $field_error_message_list[$field];
        }

        $field_error_message_list[$field] = form_error($field);

        if ($field_error_message_list[$field] !== '') {
            return 'is-invalid';
        }

        return '';
    }
}

if (!function_exists('fieldInvalidFeedback')) {
    function fieldInvalidFeedback(string $field): string
    {
        return fieldIsInvalid($field);
    }
}

if (!function_exists('methodField')) {
    /**
     * Generate a form field to spoof the HTTP verb used by forms.
     *
     * @param  string  $method
     * @return string
     */
    function methodField(string $method): string
    {
        return form_hidden('_method', strtolower($method));
    }
}

if (!function_exists('csrfField')) {
    /**
     * Generate a form field to spoof the HTTP verb used by forms.
     *
     * @return string
     */
    function csrfField(): string
    {
        $ci = get_instance();

        return form_hidden($ci->security->get_csrf_token_name(), $ci->security->get_csrf_hash());
    }
}

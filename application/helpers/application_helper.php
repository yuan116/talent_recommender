<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('htmlLink')) {
    /**
     * htmlLink
     *
     * @param string $path URL to the plugins
     * @return string Link tag with URL path
     */
    function htmlLink(string $path): string
    {
        return '<link rel="stylesheet" type="text/css" href="' . $path . '" />';
    }
}

if (!function_exists('htmlScript')) {
    /**
     * htmlScript
     *
     * @param string $path URL to the plugins
     * @return string Script tag with URL path
     */
    function htmlScript(string $path): string
    {
        return '<script type="text/javascript" src="' . $path . '"></script>';
    }
}

if (!function_exists('setTabActive')) {
    function setTabActive($tab_value, $value, bool $return_string = TRUE)
    {
        if ($return_string) {
            return ($tab_value === $value) ? 'active' : '';
        } else {
            return ($tab_value === $value);
        }
    }
}

if (!function_exists('unsetReturn')) {
    function unsetReturn(array &$data, $index)
    {
        $value = $data[$index] ?? NULL;
        unset($data[$index]);

        return $value;
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateResumeSettingSkillRelMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ResumeSettingSkillRelModel');
    }

    public function up()
    {
        if (!$this->ResumeSettingSkillRelModel->tableExists()) {
            $this->load->model(['ResumeModel', 'SettingSkillModel']);

            $this->ResumeSettingSkillRelModel->addField([
                'resume_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE
                ],
                'setting_skill_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE
                ]
            ])
                ->addForeignKey('resume_id', $this->ResumeModel->getTable(), $this->ResumeModel->getPrimaryKey(), 'cascade', 'cascade')
                ->addForeignKey('setting_skill_id', $this->SettingSkillModel->getTable(), $this->SettingSkillModel->getPrimaryKey(), 'cascade', 'cascade')
                ->createTable();
        }
    }

    public function down()
    {
    }
}

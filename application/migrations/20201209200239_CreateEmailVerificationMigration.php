<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateEmailVerificationMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('EmailVerificationModel');
    }

    public function up()
    {
        if (!$this->EmailVerificationModel->tableExists()) {
            $type_enum = implode('\', \'', array_keys(EMAIL_VERIFICATION_LIST));
            $this->load->model('UserModel');

            $this->EmailVerificationModel->addField([
                'user_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                ],
                'type' => [
                    'type' => "ENUM('{$type_enum}')",
                    'unsigned' => TRUE,
                ],
                'token' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255
                ],
                'data' => [
                    'type' => 'JSON',
                    'null' => TRUE
                ],
                '`datetime` DATETIME default CURRENT_TIMESTAMP'
            ])
                ->addForeignKey('user_id', $this->UserModel->getTable(), $this->UserModel->getPrimaryKey(), 'cascade', 'cascade')
                ->createTable();
        }
    }

    public function down()
    {
    }
}

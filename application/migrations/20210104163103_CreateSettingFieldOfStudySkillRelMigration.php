<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateSettingFieldOfStudySkillRelMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingFieldOfStudySkillRelModel');
    }

    public function up()
    {
        if (!$this->SettingFieldOfStudySkillRelModel->tableExists()) {
            $this->load->model(['SettingFieldOfStudyModel', 'SettingSkillModel']);

            $this->SettingFieldOfStudySkillRelModel->addField([
                'setting_field_of_study_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE
                ],
                'setting_skill_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE
                ]
            ])
                ->addForeignKey('setting_field_of_study_id', $this->SettingFieldOfStudyModel->getTable(), $this->SettingFieldOfStudyModel->getPrimaryKey(), 'cascade', 'cascade')
                ->addForeignKey('setting_skill_id', $this->SettingSkillModel->getTable(), $this->SettingSkillModel->getPrimaryKey(), 'cascade', 'cascade')
                ->createTable();
        }
    }

    public function down()
    {
    }
}

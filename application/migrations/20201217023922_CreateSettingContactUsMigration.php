<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateSettingContactUsMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingContactUsModel');
    }

    public function up()
    {
        if (!$this->SettingContactUsModel->tableExists()) {
            $this->SettingContactUsModel->addField([
                'key' => [
                    'type' => 'VARCHAR',
                    'constraint' => 150
                ],
                'value' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ]
            ])->createTable();
        }
    }

    public function down()
    {
    }
}

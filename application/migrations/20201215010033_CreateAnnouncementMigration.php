<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateAnnouncementMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AnnouncementModel');
    }

    public function up()
    {
        if (!$this->AnnouncementModel->tableExists()) {
            $this->AnnouncementModel->addField([
                'id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'title' => [
                    'type' => 'VARCHAR',
                    'constraint' => 200,
                    'null' => TRUE
                ],
                'content' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ],
                'publish' => [
                    'type' => 'TINYINT',
                    'default' => 1
                ],
                'publish_to_outside' => [
                    'type' => 'TINYINT',
                    'default' => 1
                ],
                'created_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'updated_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'deleted_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->addTimestamp()->addDelete()
                ->createTable();
        }
    }

    public function down()
    {
    }
}

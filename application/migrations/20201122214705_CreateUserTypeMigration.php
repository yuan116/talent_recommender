<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateUserTypeMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserTypeModel');
    }

    public function up()
    {
        if (!$this->UserTypeModel->tableExists()) {
            $this->UserTypeModel->addField([
                'id' => [
                    'type' => 'INT',
                    'auto_increment' => TRUE
                ],
                'description' => [
                    'type' => 'VARCHAR',
                    'constraint' => 100,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->createTable();
        }
    }

    public function down()
    {
    }
}

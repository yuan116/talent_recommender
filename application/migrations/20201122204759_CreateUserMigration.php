<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateUserMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
    }

    public function up()
    {
        if (!$this->UserModel->tableExists()) {
            $gender_enum = implode('\', \'', array_keys(GENDER_LIST));
            $this->load->model(['UserTypeModel', 'SettingStateModel', 'CompanyModel']);

            $this->UserModel->addField([
                'id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'user_type_id' => [
                    'type' => 'INT',
                    'null' => TRUE
                ],
                'email' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'password' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'activated' => [
                    'type' => 'TINYINT',
                    'default' => 0
                ],
                'name' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'identity_number' => [
                    'type' => 'VARCHAR',
                    'constraint' => 15,
                    'null' => TRUE
                ],
                'gender' => [
                    'type' => "ENUM('{$gender_enum}')",
                    'null' => TRUE
                ],
                'phone_number' => [
                    'type' => 'VARCHAR',
                    'constraint' => 20,
                    'null' => TRUE
                ],
                'date_of_birth' => [
                    'type' => 'DATE',
                    'null' => TRUE
                ],
                'address' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ],
                'setting_state_id' => [
                    'type' => 'INT',
                    'null' => TRUE
                ],
                'company_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'profile_pic' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ],
                'last_active' => [
                    'type' => 'DATETIME',
                    'null' => TRUE
                ],
                'created_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'updated_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'deleted_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->addTimestamp()->addDelete()
                ->addForeignKey('user_type_id', $this->UserTypeModel->getTable(), $this->UserTypeModel->getPrimaryKey())
                ->addForeignKey('setting_state_id', $this->SettingStateModel->getTable(), $this->SettingStateModel->getPrimaryKey())
                ->addForeignKey('company_id', $this->CompanyModel->getTable(), $this->CompanyModel->getPrimaryKey())
                ->createTable();
        }
    }

    public function down()
    {
    }
}

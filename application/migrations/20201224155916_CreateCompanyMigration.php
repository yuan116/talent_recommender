<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateCompanyMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CompanyModel');
    }

    public function up()
    {
        if (!$this->CompanyModel->tableExists()) {
            $this->load->model('SettingStateModel');

            $this->CompanyModel->addField([
                'id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'registration_number' => [
                    'type' => 'VARCHAR',
                    'constraint' => 50,
                    'null' => TRUE
                ],
                'company_name' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'company_email' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'phone_number' => [
                    'type' => 'VARCHAR',
                    'constraint' => 20,
                    'null' => TRUE
                ],
                'incorporate_date' => [
                    'type' => 'DATE',
                    'null' => TRUE
                ],
                'address' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ],
                'setting_state_id' => [
                    'type' => 'INT',
                    'null' => TRUE
                ],
                'other_info' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ],
                'company_logo' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ],
                'is_new' => [
                    'type' => 'TINYINT',
                    'default' => 1
                ],
                'created_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'updated_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'deleted_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->addTimestamp()->addDelete()
                ->addForeignKey('setting_state_id', $this->SettingStateModel->getTable(), $this->SettingStateModel->getPrimaryKey(), 'cascade', 'cascade')
                ->createTable();
        }
    }

    public function down()
    {
    }
}

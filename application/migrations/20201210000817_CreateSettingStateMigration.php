<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateSettingStateMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingStateModel');
    }

    public function up()
    {
        if (!$this->SettingStateModel->tableExists()) {
            $this->SettingStateModel->addField([
                'id' => [
                    'type' => 'INT',
                    'auto_increment' => TRUE
                ],
                'description' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'created_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'updated_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'deleted_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->addTimestamp()->addDelete()
                ->createTable();
        }
    }

    public function down()
    {
    }
}

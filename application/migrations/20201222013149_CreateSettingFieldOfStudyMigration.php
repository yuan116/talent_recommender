<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateSettingFieldOfStudyMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingFieldOfStudyModel');
    }

    public function up()
    {
        if (!$this->SettingFieldOfStudyModel->tableExists()) {
            $this->SettingFieldOfStudyModel->addField([
                'id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'description' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'created_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'updated_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'deleted_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->addTimestamp()->addDelete()
                ->createTable();
        }
    }

    public function down()
    {
    }
}

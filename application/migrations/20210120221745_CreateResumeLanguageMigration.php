<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateResumeLanguageMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ResumeLanguageRelModel');
    }

    public function up()
    {
        if (!$this->ResumeLanguageRelModel->tableExists()) {
            $this->load->model(['ResumeModel', 'SettingLanguageModel']);

            $this->ResumeLanguageRelModel->addField([
                'resume_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE
                ],
                'setting_language_id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE
                ]
            ])
                ->addForeignKey('resume_id', $this->ResumeModel->getTable(), $this->ResumeModel->getPrimaryKey(), 'cascade', 'cascade')
                ->addForeignKey('setting_language_id', $this->SettingLanguageModel->getTable(), $this->SettingLanguageModel->getPrimaryKey(), 'cascade', 'cascade')
                ->createTable();
        }
    }

    public function down()
    {
    }
}

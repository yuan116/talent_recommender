<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateSettingLanguageMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingLanguageModel');
    }

    public function up()
    {
        if (!$this->SettingLanguageModel->tableExists()) {
            $this->SettingLanguageModel->addField([
                'id' => [
                    'type' => 'INT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'description' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'created_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'updated_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'deleted_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->addTimestamp()->addDelete()
                ->createTable();
        }
    }

    public function down()
    {
    }
}

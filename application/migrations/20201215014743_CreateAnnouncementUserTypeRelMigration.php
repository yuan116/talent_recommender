<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateAnnouncementUserTypeRelMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('AnnouncementUserTypeRelModel');
    }

    public function up()
    {
        if (!$this->AnnouncementUserTypeRelModel->tableExists()) {
            $this->load->model(['AnnouncementModel', 'UserTypeModel']);

            $this->AnnouncementUserTypeRelModel->addField([
                'announcement_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE
                ],
                'user_type_id' => [
                    'type' => 'INT'
                ]
            ])
                ->addForeignKey('announcement_id', $this->AnnouncementModel->getTable(), $this->AnnouncementModel->getPrimaryKey(), 'cascade', 'cascade')
                ->addForeignKey('user_type_id', $this->UserTypeModel->getTable(), $this->UserTypeModel->getPrimaryKey(), 'cascade', 'cascade')
                ->createTable();
        }
    }

    public function down()
    {
    }
}

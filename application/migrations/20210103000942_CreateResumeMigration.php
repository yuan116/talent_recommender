<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CreateResumeMigration extends MY_Migration
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ResumeModel');
    }

    public function up()
    {
        if (!$this->ResumeModel->tableExists()) {
            $this->load->model(['CompanyModel', 'SettingFieldOfStudyModel']);

            $this->ResumeModel->addField([
                'id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'auto_increment' => TRUE
                ],
                'company_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE
                ],
                'filename' => [
                    'type' => 'VARCHAR',
                    'constraint' => 255,
                    'null' => TRUE
                ],
                'filepath' => [
                    'type' => 'TEXT',
                    'null' => TRUE
                ],
                'setting_field_of_study_id' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'cgpa' => [
                    'type' => 'DECIMAL',
                    'constraint' => '3,2',
                    'null' => TRUE
                ],
                'data' => [
                    'type' => 'JSON',
                    'null' => TRUE
                ],
                'created_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'updated_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ],
                'deleted_by' => [
                    'type' => 'BIGINT',
                    'unsigned' => TRUE,
                    'null' => TRUE
                ]
            ])
                ->addKey('id', TRUE)
                ->addTimestamp()->addDelete()
                ->addForeignKey('company_id', $this->CompanyModel->getTable(), $this->CompanyModel->getPrimaryKey(), 'cascade', 'cascade')
                ->addForeignKey('setting_field_of_study_id', $this->SettingFieldOfStudyModel->getTable(), $this->SettingFieldOfStudyModel->getPrimaryKey(), 'cascade', 'cascade')
                ->createTable();
        }
    }

    public function down()
    {
    }
}

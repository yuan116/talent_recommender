<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FlashMessage
{
    public function destroyFlashMessage(): void
    {
        $ci = get_instance();

        $key = ['message', 'toastr_message'];
        $ci->session->unmark_flash($key);
        $ci->session->unset_userdata($key);
    }
}

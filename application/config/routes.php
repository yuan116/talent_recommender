<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This user_file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/userguide3/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = is_cli() ? 'CliController' : 'PublicController/home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

if (is_cli()) {
    $route['(.+)'] = 'CliController/index';
} else {
    $route['support_log']['get'] = 'LogController/index';
    $route['support_log']['post'] = 'LogController/index';

    //PublicContoller
    $route['home']['get'] = 'PublicController/home';
    $route['contact_us']['get'] = 'PublicController/contactUs';
    $route['logout']['post'] = 'PublicController/logout';
    $route['force_logout']['get'] = 'PublicController/forceLogout';
    $route['verify_email']['get'] = 'PublicController/verifyEmail';
    $route['change_email/(:num)']['get'] = 'PublicController/changeEmail/$1';

    //LoginContoller
    $route['sign_up']['get'] = 'LoginController/signUp';
    $route['sign_up']['post'] = 'LoginController/signUp';
    $route['activate/(:num)']['get'] = 'LoginController/activate/$1';
    $route['activate/(:num)']['post'] = 'LoginController/activate/$1';
    $route['sign_in']['get'] = 'LoginController/signIn';
    $route['sign_in']['post'] = 'LoginController/signIn';
    $route['forgot_password']['get'] = 'LoginController/forgotPassword';
    $route['forgot_password']['post'] = 'LoginController/forgotPassword';
    $route['reset_password/(:num)']['get'] = 'LoginController/resetPassword/$1';
    $route['reset_password/(:num)']['post'] = 'LoginController/resetPassword/$1';

    //AnnouncementController
    $route['announcement']['get'] = 'AnnouncementController/index';
    $route['announcement/get_list']['get'] = 'AnnouncementController/getList';
    $route['announcement']['get'] = 'AnnouncementController/index';
    $route['announcement']['post'] = 'AnnouncementController/index';
    $route['announcement/(:num)']['get'] = 'AnnouncementController/index/$1';
    $route['announcement/(:num)']['put'] = 'AnnouncementController/index/$1';
    $route['announcement/(:num)']['delete'] = 'AnnouncementController/delete/$1';

    //CompanyController
    $route['company']['get'] = 'CompanyController/index';
    $route['company/get_list']['get'] = 'CompanyController/getList';
    $route['company']['get'] = 'CompanyController/index';
    $route['company']['post'] = 'CompanyController/index';
    $route['company/(:num)']['get'] = 'CompanyController/index/$1';
    $route['company/(:num)']['put'] = 'CompanyController/index/$1';
    $route['company/(:num)']['delete'] = 'CompanyController/delete/$1';

    //CompanyProfileController
    $route['company_profile']['get'] = 'CompanyProfileController/index';
    $route['company_profile']['post'] = 'CompanyProfileController/index';
    $route['company_profile/invite_user']['post'] = 'CompanyProfileController/inviteUser';

    //ResumeController
    $route['resume']['get'] = 'ResumeController/index';
    $route['resume/get_list']['get'] = 'ResumeController/getList';
    $route['resume/upload']['post'] = 'ResumeController/upload';
    $route['resume/update']['post'] = 'ResumeController/update';
    $route['resume/compare']['get'] = 'ResumeController/compare';
    $route['resume/(:num)']['delete'] = 'ResumeController/delete/$1';

    //UserController
    $route['user']['get'] = 'UserController/index';
    $route['user/get_list']['get'] = 'UserController/getList';
    $route['user']['get'] = 'UserController/index';
    $route['user']['post'] = 'UserController/index';
    $route['user/(:num)']['get'] = 'UserController/index/$1';
    $route['user/(:num)']['put'] = 'UserController/index/$1';
    $route['user/(:num)']['delete'] = 'UserController/delete/$1';

    //UserProfileController
    $route['user_profile']['get'] = 'UserProfileController/index';
    $route['user_profile']['post'] = 'UserProfileController/index';

    //SettingController
    $route['setting']['get'] = 'SettingController/setting';

    //setting/ContactUsController
    $route['setting/contact_us']['get'] = 'setting/ContactUsController/index';
    $route['setting/contact_us']['post'] = 'setting/ContactUsController/index';

    //setting/FieldOfStudyController
    $route['setting/field_of_study']['get'] = 'setting/FieldOfStudyController/index';
    $route['setting/field_of_study']['post'] = 'setting/FieldOfStudyController/index';
    $route['setting/field_of_study/(:num)']['get'] = 'setting/FieldOfStudyController/index/$1';
    $route['setting/field_of_study/(:num)']['put'] = 'setting/FieldOfStudyController/index/$1';
    $route['setting/field_of_study/(:num)']['delete'] = 'setting/FieldOfStudyController/delete/$1';

    //setting/LanguageController
    $route['setting/language']['get'] = 'setting/LanguageController/index';
    $route['setting/language']['post'] = 'setting/LanguageController/index';
    $route['setting/language/(:num)']['get'] = 'setting/LanguageController/index/$1';
    $route['setting/language/(:num)']['put'] = 'setting/LanguageController/index/$1';
    $route['setting/language/(:num)']['delete'] = 'setting/LanguageController/delete/$1';

    //setting/StateController
    $route['setting/state']['get'] = 'setting/StateController/index';
    $route['setting/state']['post'] = 'setting/StateController/index';
    $route['setting/state/(:num)']['get'] = 'setting/StateController/index/$1';
    $route['setting/state/(:num)']['put'] = 'setting/StateController/index/$1';
    $route['setting/state/(:num)']['delete'] = 'setting/StateController/delete/$1';

    //setting/SkillController
    $route['setting/skill']['get'] = 'setting/SkillController/index';
    $route['setting/skill/get_list']['get'] = 'setting/SkillController/getList';
    $route['setting/skill']['post'] = 'setting/SkillController/index';
    $route['setting/skill/(:num)']['get'] = 'setting/SkillController/index/$1';
    $route['setting/skill/(:num)']['put'] = 'setting/SkillController/index/$1';
    $route['setting/skill/(:num)']['delete'] = 'setting/SkillController/delete/$1';
}

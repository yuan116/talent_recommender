<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingFieldOfStudySeeder extends Seeder
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingFieldOfStudyModel');
    }

    public function run()
    {
        $this->SettingFieldOfStudyModel->setForeignKeyCheck(FALSE);
        $this->SettingFieldOfStudyModel->truncate();

        $this->SettingFieldOfStudyModel->insert(['description' => 'Management']);
        $this->SettingFieldOfStudyModel->insert(['description' => 'Engineering']);
        $this->SettingFieldOfStudyModel->insert(['description' => 'Information Technology']);
        $this->SettingFieldOfStudyModel->insert(['description' => 'Multimedia']);

        $this->SettingFieldOfStudyModel->setForeignKeyCheck(TRUE);
    }
}

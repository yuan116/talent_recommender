<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingStateSeeder extends Seeder
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingStateModel');
    }

    public function run()
    {
        $this->SettingStateModel->setForeignKeyCheck(FALSE);
        $this->SettingStateModel->truncate();

        $this->SettingStateModel->insert(['description' => 'Johor']);
        $this->SettingStateModel->insert(['description' => 'Kedah']);
        $this->SettingStateModel->insert(['description' => 'Kelantan']);
        $this->SettingStateModel->insert(['description' => 'Kuala Lumpur']);
        $this->SettingStateModel->insert(['description' => 'Labuan']);
        $this->SettingStateModel->insert(['description' => 'Melaka']);
        $this->SettingStateModel->insert(['description' => 'Negeri Sembilan']);
        $this->SettingStateModel->insert(['description' => 'Pahang']);
        $this->SettingStateModel->insert(['description' => 'Perak']);
        $this->SettingStateModel->insert(['description' => 'Perlis']);
        $this->SettingStateModel->insert(['description' => 'Pulau Pinang']);
        $this->SettingStateModel->insert(['description' => 'Putrajaya']);
        $this->SettingStateModel->insert(['description' => 'Sabah']);
        $this->SettingStateModel->insert(['description' => 'Sarawak']);
        $this->SettingStateModel->insert(['description' => 'Selangor']);

        $this->SettingStateModel->setForeignKeyCheck(TRUE);
    }
}

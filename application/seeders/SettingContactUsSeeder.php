<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingContactUsSeeder extends Seeder
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingContactUsModel');
    }

    public function run()
    {
        $this->SettingContactUsModel->setForeignKeyCheck(FALSE);
        $this->SettingContactUsModel->truncate();

        $this->SettingContactUsModel->insert([
            'key' => 'phone_number',
            'value' => '0123456789'
        ]);
        $this->SettingContactUsModel->insert([
            'key' => 'email',
            'value' => 'support@localhost.com'
        ]);
        $this->SettingContactUsModel->insert([
            'key' => 'address',
            'value' => 'Persiaran Multimedia, 63100 Cyberjaya, Selangor'
        ]);
        $this->SettingContactUsModel->insert([
            'key' => 'address_iframe',
            'value' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.6104793630134!2d101.63971171471938!3d2.927771497867588!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdb6e4a9d3b7a1%3A0xd0f74e8ad10f1129!2sMultimedia%20University%20-%20MMU%20Cyberjaya!5e0!3m2!1sen!2smy!4v1608466722421!5m2!1sen!2smy"'
        ]);

        $this->SettingContactUsModel->setForeignKeyCheck(TRUE);
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingFieldOfStudySkillRelSeeder extends Seeder
{
    protected const FIELD_MANAGEMENT = 1;
    protected const FIELD_ENGINEERING = 2;
    protected const FIELD_IT = 3;
    protected const FIELD_MULTIMEDIA = 4;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingFieldOfStudySkillRelModel');
    }

    public function run()
    {
        $this->SettingFieldOfStudySkillRelModel->setForeignKeyCheck(FALSE);
        $this->SettingFieldOfStudySkillRelModel->truncate();

        $microsoft_skill_list = range(1, 8);
        $engineering_iT_skill_list = range(38, 41);
        $engineering_multimedia_skill_list = [85];
        $management_multimedia_skill_list = [17];

        $management_skill_list = array_merge($microsoft_skill_list, range(9, 16), $management_multimedia_skill_list);
        foreach ($management_skill_list as $skill_id) {
            $this->SettingFieldOfStudySkillRelModel->insert([
                'setting_field_of_study_id' => self::FIELD_MANAGEMENT,
                'setting_skill_id' => $skill_id
            ]);
        }

        $engineering_skill_list = array_merge($microsoft_skill_list, range(18, 37), $engineering_iT_skill_list, $engineering_multimedia_skill_list);
        foreach ($engineering_skill_list as $skill_id) {
            $this->SettingFieldOfStudySkillRelModel->insert([
                'setting_field_of_study_id' => self::FIELD_ENGINEERING,
                'setting_skill_id' => $skill_id
            ]);
        }

        $it_skill_list = array_merge($microsoft_skill_list, $engineering_iT_skill_list, range(42, 84));
        foreach ($it_skill_list as $skill_id) {
            $this->SettingFieldOfStudySkillRelModel->insert([
                'setting_field_of_study_id' => self::FIELD_IT,
                'setting_skill_id' => $skill_id
            ]);
        }

        $multimedia_skill_list = array_merge($microsoft_skill_list, $management_multimedia_skill_list, $engineering_multimedia_skill_list, range(86, 102));
        foreach ($multimedia_skill_list as $skill_id) {
            $this->SettingFieldOfStudySkillRelModel->insert([
                'setting_field_of_study_id' => self::FIELD_MULTIMEDIA,
                'setting_skill_id' => $skill_id
            ]);
        }

        $this->SettingFieldOfStudySkillRelModel->setForeignKeyCheck(TRUE);
    }
}

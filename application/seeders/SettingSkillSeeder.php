<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingSkillSeeder extends Seeder
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingSkillModel');
    }

    public function run()
    {
        $this->SettingSkillModel->setForeignKeyCheck(FALSE);
        $this->SettingSkillModel->truncate();

        $this->SettingSkillModel->insert(['description' => 'Microsoft']); //id => 1
        $this->SettingSkillModel->insert(['description' => 'Microsoft Office']); //id => 2
        $this->SettingSkillModel->insert(['description' => 'Microsoft Excel']); //id => 3
        $this->SettingSkillModel->insert(['description' => 'Microsoft PowerPoint']); //id => 4
        $this->SettingSkillModel->insert(['description' => 'Microsoft Word']); //id => 5
        $this->SettingSkillModel->insert(['description' => 'Microsoft Project']); //id => 6
        $this->SettingSkillModel->insert(['description' => 'Microsoft Visio']); //id => 7
        $this->SettingSkillModel->insert(['description' => 'Microsoft Publisher']); //id => 8
        $this->SettingSkillModel->insert(['description' => 'Accounting']); //id => 9
        $this->SettingSkillModel->insert(['description' => 'Risk Analysis']);
        $this->SettingSkillModel->insert(['description' => 'Investment Analysis']);
        $this->SettingSkillModel->insert(['description' => 'Marketing Analysis']);
        $this->SettingSkillModel->insert(['description' => 'Marketing Strategy']);
        $this->SettingSkillModel->insert(['description' => 'Information Analysis']);
        $this->SettingSkillModel->insert(['description' => 'Financial Analysis']);
        $this->SettingSkillModel->insert(['description' => 'Information Analysis']); //id => 16
        $this->SettingSkillModel->insert(['description' => 'Advertising']); //id => 17

        $this->SettingSkillModel->insert(['description' => 'MATLAB']); //id => 18
        $this->SettingSkillModel->insert(['description' => 'Arduino']);
        $this->SettingSkillModel->insert(['description' => 'Electronics and Telecommunication']);
        $this->SettingSkillModel->insert(['description' => 'Digital Wireless Communications']);
        $this->SettingSkillModel->insert(['description' => 'Telecommunications Network']);
        $this->SettingSkillModel->insert(['description' => 'Mechanical']);
        $this->SettingSkillModel->insert(['description' => 'Thermo']);
        $this->SettingSkillModel->insert(['description' => 'Chemical']);
        $this->SettingSkillModel->insert(['description' => 'Manufacturing']);
        $this->SettingSkillModel->insert(['description' => 'Fusion 360']);
        $this->SettingSkillModel->insert(['description' => 'Pascal']);
        $this->SettingSkillModel->insert(['description' => 'AutoDesk']);
        $this->SettingSkillModel->insert(['description' => 'Proteus']);
        $this->SettingSkillModel->insert(['description' => 'LTspice']);
        $this->SettingSkillModel->insert(['description' => 'PSpice']);
        $this->SettingSkillModel->insert(['description' => 'CATIA']);
        $this->SettingSkillModel->insert(['description' => 'MikroC']);
        $this->SettingSkillModel->insert(['description' => 'MPLAB']);
        $this->SettingSkillModel->insert(['description' => 'Aspen HYSYS']);
        $this->SettingSkillModel->insert(['description' => 'Computer Networking']); //id => 37

        $this->SettingSkillModel->insert(['description' => 'IOT']); //id => 38
        $this->SettingSkillModel->insert(['description' => 'C']); //id => 39
        $this->SettingSkillModel->insert(['description' => 'C++']); //id => 40
        $this->SettingSkillModel->insert(['description' => 'Python']); //id => 41
        $this->SettingSkillModel->insert(['description' => 'PHP']); //id => 42
        $this->SettingSkillModel->insert(['description' => 'HTML']);
        $this->SettingSkillModel->insert(['description' => 'CSS']);
        $this->SettingSkillModel->insert(['description' => 'Javascript']);
        $this->SettingSkillModel->insert(['description' => 'Java']);
        $this->SettingSkillModel->insert(['description' => 'C#']);
        $this->SettingSkillModel->insert(['description' => 'Linux']);
        $this->SettingSkillModel->insert(['description' => 'Git']);
        $this->SettingSkillModel->insert(['description' => 'MySQL']);
        $this->SettingSkillModel->insert(['description' => 'Firebase']);
        $this->SettingSkillModel->insert(['description' => 'Xamarin']);
        $this->SettingSkillModel->insert(['description' => 'Codeigniter']);
        $this->SettingSkillModel->insert(['description' => 'Laravel']);
        $this->SettingSkillModel->insert(['description' => 'React Native']);
        $this->SettingSkillModel->insert(['description' => 'Vue JS']);
        $this->SettingSkillModel->insert(['description' => 'Swift']);
        $this->SettingSkillModel->insert(['description' => 'Flutter']);
        $this->SettingSkillModel->insert(['description' => 'Microsoft Access']);
        $this->SettingSkillModel->insert(['description' => 'Android Studio']);
        $this->SettingSkillModel->insert(['description' => 'R Programming']);
        $this->SettingSkillModel->insert(['description' => 'FreeMAT']);
        $this->SettingSkillModel->insert(['description' => 'Parallel Programming']);
        $this->SettingSkillModel->insert(['description' => 'Network Configuration']);
        $this->SettingSkillModel->insert(['description' => 'Deep Learning']);
        $this->SettingSkillModel->insert(['description' => 'Machine Learning']);
        $this->SettingSkillModel->insert(['description' => 'Artificial Intelligence']);
        $this->SettingSkillModel->insert(['description' => 'Data Analysis']);
        $this->SettingSkillModel->insert(['description' => 'SQL']);
        $this->SettingSkillModel->insert(['description' => 'MongoDB']);
        $this->SettingSkillModel->insert(['description' => 'Oracle']);
        $this->SettingSkillModel->insert(['description' => 'NoSQL']);
        $this->SettingSkillModel->insert(['description' => 'SQLite']);
        $this->SettingSkillModel->insert(['description' => 'SFML']);
        $this->SettingSkillModel->insert(['description' => 'Visual Basic']);
        $this->SettingSkillModel->insert(['description' => 'Data Mining']);
        $this->SettingSkillModel->insert(['description' => 'Tensorflow']);
        $this->SettingSkillModel->insert(['description' => 'Sklearn']);
        $this->SettingSkillModel->insert(['description' => 'Hadoop']);
        $this->SettingSkillModel->insert(['description' => 'Octave']);
        $this->SettingSkillModel->insert(['description' => 'Dart']);
        $this->SettingSkillModel->insert(['description' => 'Cisco']);
        $this->SettingSkillModel->insert(['description' => 'Keras']);
        $this->SettingSkillModel->insert(['description' => 'Docker']); //id => 84

        $this->SettingSkillModel->insert(['description' => 'AutoCAD']); //id => 85
        $this->SettingSkillModel->insert(['description' => 'Adobe Photoshop']); //id => 86
        $this->SettingSkillModel->insert(['description' => 'Adobe Premiere Pro']);
        $this->SettingSkillModel->insert(['description' => 'Adobe Lightroom']);
        $this->SettingSkillModel->insert(['description' => 'Adobe XD']);
        $this->SettingSkillModel->insert(['description' => 'Adobe Illustrator']);
        $this->SettingSkillModel->insert(['description' => 'After Effects']);
        $this->SettingSkillModel->insert(['description' => 'Shooting']);
        $this->SettingSkillModel->insert(['description' => 'Video Editing']);
        $this->SettingSkillModel->insert(['description' => 'Sound Design']);
        $this->SettingSkillModel->insert(['description' => 'Film']);
        $this->SettingSkillModel->insert(['description' => 'Screenwriting']);
        $this->SettingSkillModel->insert(['description' => 'Graphic Design']);
        $this->SettingSkillModel->insert(['description' => 'Animation']);
        $this->SettingSkillModel->insert(['description' => 'Broadcast']);
        $this->SettingSkillModel->insert(['description' => 'Branding']);
        $this->SettingSkillModel->insert(['description' => 'Scriptwriting']);
        $this->SettingSkillModel->insert(['description' => 'Media Social']); //id => 102

        $this->SettingSkillModel->setForeignKeyCheck(TRUE);
    }
}

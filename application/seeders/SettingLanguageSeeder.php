<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingLanguageSeeder extends Seeder
{
    protected string $table = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('SettingLanguageModel');
    }

    public function run()
    {
        $this->SettingLanguageModel->setForeignKeyCheck(FALSE);
        $this->SettingLanguageModel->truncate();

        $this->SettingLanguageModel->insert(['description' => 'Malay']);
        $this->SettingLanguageModel->insert(['description' => 'English']);
        $this->SettingLanguageModel->insert(['description' => 'Chinese']);
        $this->SettingLanguageModel->insert(['description' => 'Tamil']);
        $this->SettingLanguageModel->insert(['description' => 'Hokkien']);
        $this->SettingLanguageModel->insert(['description' => 'Hakka']);
        $this->SettingLanguageModel->insert(['description' => 'Cantonese']);
        $this->SettingLanguageModel->insert(['description' => 'Korean']);
        $this->SettingLanguageModel->insert(['description' => 'Japanese']);

        $this->SettingLanguageModel->setForeignKeyCheck(TRUE);
    }
}

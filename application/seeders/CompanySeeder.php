<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CompanySeeder extends Seeder
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('CompanyModel');
    }

    public function run()
    {
        $this->CompanyModel->setForeignKeyCheck(FALSE);
        $this->CompanyModel->truncate();

        $this->CompanyModel->insert([
            'registration_number' => '0123456789',
            'company_name' => 'COMPANY A',
            'company_email' => 'company_a@localhost.com',
            'phone_number' => '0123456789',
            'incorporate_date' => '1980-01-01',
            'setting_state_id' => 4,
            'company_logo' => DEFAULT_PROFILE_PIC,
            'is_new' => FALSE
        ]);

        $this->CompanyModel->insert([
            'registration_number' => '0123456789',
            'company_name' => 'COMPANY B',
            'company_email' => 'company_b@localhost.com',
            'phone_number' => '0123456789',
            'incorporate_date' => '1980-01-01',
            'setting_state_id' => 4,
            'company_logo' => DEFAULT_PROFILE_PIC,
            'is_new' => FALSE
        ]);

        $this->CompanyModel->setForeignKeyCheck(TRUE);
    }
}

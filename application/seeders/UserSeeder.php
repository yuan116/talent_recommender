<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Yuan116\Ci3\Enhance\Libraries\Password\Password;

class UserSeeder extends Seeder
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserModel');
    }

    public function run()
    {
        $this->UserModel->setForeignKeyCheck(FALSE);
        $this->UserModel->truncate();
        $password = (new Password())->hash('12345678');

        $this->UserModel->insert([
            'user_type_id' => USER_TYPE_ADMINISTRATOR,
            'email' => 'admin@localhost.com',
            'password' => $password,
            'activated' => TRUE,
            'name' => 'Administrator',
            'gender' => GENDER_MALE,
            'phone_number' => '0123456789',
            'date_of_birth' => '1980-01-01',
            'setting_state_id' => 4,
            'profile_pic' => DEFAULT_PROFILE_PIC
        ]);

        $this->UserModel->insert([
            'user_type_id' => USER_TYPE_STAFF,
            'email' => 'staff@localhost.com',
            'password' => $password,
            'activated' => TRUE,
            'name' => 'Staff A',
            'gender' => GENDER_MALE,
            'phone_number' => '0123456789',
            'date_of_birth' => '1980-01-01',
            'setting_state_id' => 4,
            'profile_pic' => DEFAULT_PROFILE_PIC
        ]);

        $this->UserModel->insert([
            'user_type_id' => USER_TYPE_EMPLOYER,
            'email' => 'user_a@localhost.com',
            'password' => $password,
            'activated' => TRUE,
            'name' => 'User A',
            'gender' => GENDER_MALE,
            'phone_number' => '0123456789',
            'date_of_birth' => '1980-01-01',
            'setting_state_id' => 4,
            'company_id' => 1,
            'profile_pic' => DEFAULT_PROFILE_PIC
        ]);

        $this->UserModel->insert([
            'user_type_id' => USER_TYPE_EMPLOYER,
            'email' => 'user_b@localhost.com',
            'password' => $password,
            'activated' => TRUE,
            'name' => 'User B',
            'gender' => GENDER_MALE,
            'phone_number' => '0123456789',
            'date_of_birth' => '1980-01-01',
            'setting_state_id' => 4,
            'company_id' => 1,
            'profile_pic' => DEFAULT_PROFILE_PIC
        ]);

        $this->UserModel->insert([
            'user_type_id' => USER_TYPE_EMPLOYER,
            'email' => 'user_c@localhost.com',
            'password' => $password,
            'activated' => TRUE,
            'name' => 'User C',
            'gender' => GENDER_MALE,
            'phone_number' => '0123456789',
            'date_of_birth' => '1980-01-01',
            'setting_state_id' => 4,
            'company_id' => 2,
            'profile_pic' => DEFAULT_PROFILE_PIC
        ]);

        $this->UserModel->setForeignKeyCheck(TRUE);
    }
}

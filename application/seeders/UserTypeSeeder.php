<?php

defined('BASEPATH') or exit('No direct script access allowed');

class UserTypeSeeder extends Seeder
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('UserTypeModel');
    }

    public function run()
    {
        $this->UserTypeModel->setForeignKeyCheck(FALSE);
        $this->UserTypeModel->truncate();

        $this->UserTypeModel->insert([
            'id' => USER_TYPE_ADMINISTRATOR,
            'description' => 'Administrator'
        ]);
        $this->UserTypeModel->insert([
            'id' => USER_TYPE_STAFF,
            'description' => 'Staff'
        ]);
        $this->UserTypeModel->insert([
            'id' => USER_TYPE_EMPLOYER,
            'description' => 'Employer'
        ]);

        $this->UserTypeModel->setForeignKeyCheck(TRUE);
    }
}

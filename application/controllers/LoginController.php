<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class LoginController extends NoAuthController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string')
            ->model(['CompanyModel', 'EmailVerificationModel', 'SettingStateModel', 'UserModel']);
    }

    public function signUp(): void
    {
        $this->active_menu = 'sign_up';
        $this->signUpValidation();
        $this->resendSignUpValidation();

        $data['content'] = $this->load->view('login/sign_up', NULL, TRUE);
        $this->load->view('template/outer', $data);
    }

    protected function signUpValidation(): void
    {
        if (!isPost('sign_up_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $data = $this->input->post(['company_name', 'name', 'email'], TRUE);
            $company_data['company_name'] = unsetReturn($data, 'company_name');
            $data['company_id'] = $this->CompanyModel->save($company_data);

            $data['user_type_id'] = USER_TYPE_EMPLOYER;
            $user_id = $this->UserModel->save($data);

            $this->EmailVerificationModel->insert($email_data = [
                'user_id' => $user_id,
                'type' => EMAIL_VERIFICATION_TYPE_ACTIVATION,
                'token' => random_string('md5')
            ]);

            $this->sendActivationEmail($data, $email_data);
        }
    }

    protected function resendSignUpValidation(): void
    {
        if (!isPost('resend_btn')) {
            return;
        }
        $data = $this->input->post(['name', 'email'], TRUE);

        $this->sendActivationEmail($data, $this->EmailVerificationModel->getEmailData(['b.email' => $data['email']]));
    }

    protected function sendActivationEmail(array $data, array $email_data): bool
    {
        $this->setFlashMessage([
            'type' => 'success',
            'message' => 'Please check your email to verify and create account. If you still not received the email, please click resend button to resend the email.'
        ]);
        $data['resend_btn'] = TRUE;
        $this->session->set_tempdata($data, TRUE, 600);

        return $this->sendEmail(
            [
                'subject' => EMAIL_VERIFICATION_LIST[EMAIL_VERIFICATION_TYPE_ACTIVATION],
                'to' => $data['email']
            ],
            'activation',
            [
                '{__TITLE__}' => $this->config->item('title'),
                '{__NAME__}' => $data['name'],
                '{__LINK__}' => site_url("verify_email?user_id={$email_data['user_id']}&token={$email_data['token']}")
            ]
        );
    }

    public function activate($user_id)
    {
        if (!$this->EmailVerificationModel->dataSeek([
            'user_id' => $user_id,
            'type' => EMAIL_VERIFICATION_TYPE_ACTIVATION
        ])) {
            show_404();
        }

        $this->activateValidation($user_id);

        $data['data'] = $this->UserModel->getData($user_id);
        $data['data']->date_of_birth = (Carbon::createFromFormat('Y-m-d', $data['data']->date_of_birth ?? date('Y-m-d')))->format('d-m-Y');
        $data['state_list'] = $this->SettingStateModel->getList();
        $data['content'] = $this->load->view('login/activate', $data, TRUE);
        $this->load->view('template/outer', $data);
    }

    protected function activateValidation($user_id): void
    {
        if (!isPost('activate_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $data = $this->input->post([
                'name', 'identity_number', 'gender', 'phone_number', 'date_of_birth',
                'address', 'setting_state_id', 'password', 'confirm_password'
            ], TRUE);
            unset($data['confirm_password']);
            $data['date_of_birth'] = (Carbon::createFromFormat('d-m-Y', $data['date_of_birth']))->format('Y-m-d');
            $data['password'] = $this->password->hash($data['password']);
            $data['activated'] = TRUE;
            $this->UserModel->save($data, $user_id);
            $this->EmailVerificationModel->delete(['user_id' => $user_id]);

            $this->setFlashMessage([
                'type' => 'success',
                'message' => 'Successfully verify and create account.'
            ]);
            redirect('sign_in');
        }
    }

    public function signIn(): void
    {
        $this->active_menu = 'sign_in';
        $this->signInValidation();

        $data['content'] = $this->load->view('login/sign_in', NULL, TRUE);
        $this->load->view('template/outer', $data);
    }

    protected function signInValidation(): void
    {
        if (!isPost('sign_in_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $data = $this->input->post(['email', 'password', 'remember_me'], TRUE);
            $user_data = $this->UserModel->getLoginData($data['email']);
            if ($this->password->verify($data['password'], $user_data['password'])) {
                $this->session->set_userdata($this->UserModel->getLoginSessiondata($user_data['id']));

                if ($this->password->needRehash($user_data['password'])) {
                    $this->UserModel->save(['password' => $this->password->hash($data['password'])], $this->session->user_id);
                }

                if ($data['remember_me'] === 'yes') {
                    $this->session->replaceCICookie();
                }

                redirect('home');
            }

            $this->setFlashMessage([
                'type' => 'danger',
                'message' => 'Incorrect Password.'
            ]);
        }
    }

    public function forgotPassword(): void
    {
        $this->forgotPasswordValidation();

        $data['content'] = $this->load->view('login/forgot_password', NULL, TRUE);
        $this->load->view('template/outer', $data);
    }

    protected function forgotPasswordValidation(): void
    {
        if (!isPost('forgot_password_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $user_data = $this->UserModel->select('id, email, name')->rowArray(['email' => $this->input->post('email', TRUE)]);

            $token = random_string('md5');
            $where = ['user_id' => $user_data['id']];
            if (!$this->EmailVerificationModel->dataSeek($where)) {
                $this->EmailVerificationModel->insert([
                    'user_id' => $user_data['id'],
                    'type' => EMAIL_VERIFICATION_TYPE_FORGOT_PASSWORD,
                    'token' => $token
                ]);
            } else {
                $token = $this->EmailVerificationModel->select('token')->row($where)->token;
            }

            $this->sendEmail(
                [
                    'subject' => EMAIL_VERIFICATION_LIST[EMAIL_VERIFICATION_TYPE_FORGOT_PASSWORD],
                    'to' => $user_data['email']
                ],
                'forgotPassword',
                [
                    '{__TITLE__}' => $this->config->item('title'),
                    '{__NAME__}' => $user_data['name'],
                    '{__LINK__}' => site_url("verify_email?user_id={$user_data['id']}&token={$token}")
                ]
            );

            $this->setFlashMessage([
                'type' => 'success',
                'message' => 'Please check your email to reset your password.'
            ]);
        }
    }

    public function resetPassword($user_id): void
    {
        if (!$this->EmailVerificationModel->dataSeek([
            'user_id' => $user_id,
            'type' => EMAIL_VERIFICATION_TYPE_FORGOT_PASSWORD
        ])) {
            show_404();
        }

        $this->resetPasswordValidation($user_id);

        $data['content'] = $this->load->view('login/reset_password', NULL, TRUE);
        $this->load->view('template/outer', $data);
    }

    protected function resetPasswordValidation($user_id): void
    {
        if (!isPost('reset_password_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $user_id = $this->UserModel->save(['password' => $this->password->hash($this->input->post('password', TRUE))], $user_id);
            $this->EmailVerificationModel->delete(['user_id' => $user_id]);

            $this->setFlashMessage([
                'type' => 'success',
                'message' => 'Successfully reset password.'
            ]);
            redirect('sign_in');
        }
    }
}

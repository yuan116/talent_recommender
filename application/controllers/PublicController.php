<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PublicController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(['AnnouncementModel', 'AnnouncementUserTypeRelModel', 'EmailVerificationModel', 'SettingContactUsModel', 'UserModel']);
    }

    public function home(): void
    {
        $this->active_menu = 'home';

        if ($this->session->loggedin) {
            $annnouncement_id_list = $this->AnnouncementUserTypeRelModel->getValuePairList('', 'announcement_id', ['user_type_id' => $this->session->user_type_id]);

            $data['announcement_list'] = $this->AnnouncementModel->select('title, content')->whereIn($annnouncement_id_list)->result(['publish' => TRUE]);
            $view = 'inner';
        } else {
            $data['announcement_list'] = $this->AnnouncementModel->select('title, content')->result(['publish_to_outside' => TRUE]);
            $view = 'outer';
        }

        if (empty($data['announcement_list'])) {
            $tmp_announcement = new stdClass();
            $tmp_announcement->title = 'No Announcement Title';
            $tmp_announcement->content = 'No Announcement Content';
            $data['announcement_list'] = [$tmp_announcement];
        }
        $data['announcement_index_list'] = array_keys($data['announcement_list']);

        $data['content'] = $this->load->view('public/home', $data, TRUE);
        $this->load->view('template/' . $view, $data);
    }

    public function contactUs(): void
    {
        $this->active_menu = 'contact_us';
        $data['contact_us_data'] = $this->SettingContactUsModel->getValuePairList('key', 'value');
        $data['content'] = $this->load->view('public/contact_us', $data, TRUE);
        $view = $this->session->loggedin ? 'inner' : 'outer';
        $this->load->view('template/' . $view, $data);
    }

    public function logout(): void
    {
        if ($this->session->loggedin) {
            $this->saveLastLogin();
        }

        $this->session->sess_destroy();
        redirect('home');
    }

    public function verifyEmail(): void
    {
        $where = $this->input->get(['user_id', 'token'], TRUE);
        $this->session->unset_tempdata(['resend_btn']);

        if ($data = $this->EmailVerificationModel->select('type')->row($where)) {
            switch ($data->type) {
                case EMAIL_VERIFICATION_TYPE_ACTIVATION:
                    $url = 'activate';
                    break;
                case EMAIL_VERIFICATION_TYPE_CHANGE_EMAIL:
                    $url = 'change_email';
                    break;
                case EMAIL_VERIFICATION_TYPE_FORGOT_PASSWORD:
                    $url = 'reset_password';
                    break;
                default:
                    show_404();
                    break;
            }

            redirect("{$url}/{$where['user_id']}");
        }

        show_404();
    }

    public function changeEmail($user_id): void
    {
        $where = ['user_id' => $user_id];
        $data = json_decode($this->EmailVerificationModel->select('data')->row($where)->data, TRUE);

        $this->UserModel->update($data, $user_id);
        $this->EmailVerificationModel->delete($where);

        $this->setFlashToastr('success', 'Successfully change email.');
        redirect('profile');
    }
}

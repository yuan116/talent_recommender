<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class CompanyProfileController extends AuthController
{
    protected array $allow_user_type = [USER_TYPE_EMPLOYER];
    protected $company_id;
    public string $link = 'company_profile/';
    protected string $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string')
            ->model(['CompanyModel', 'EmailVerificationModel', 'SettingStateModel', 'UserModel']);
        $this->company_id = $this->session->company_id;
        $this->file_path = 'uploads' . DIRECTORY_SEPARATOR . 'company' . DIRECTORY_SEPARATOR .  $this->company_id . DIRECTORY_SEPARATOR;
    }

    public function index(): void
    {
        $this->companyLogoValidation();
        $this->companyProfileValidation();

        $data['data'] = $this->CompanyModel->getData($this->company_id);
        $data['data']->incorporate_date = (Carbon::createFromFormat('Y-m-d', $data['data']->incorporate_date ?? date('Y-m-d')))->format('d-m-Y');
        $data['data']->company_logo = $data['data']->company_logo ?? DEFAULT_PROFILE_PIC;
        $data['state_list'] = $this->SettingStateModel->getList();
        $data['user_list'] = $this->UserModel->select('name, email, phone_number')->result(['company_id' => $this->company_id]);
        $data['content'] = $this->load->view('company_profile/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    protected function companyLogoValidation(): void
    {
        if (!isPost('save_picture_btn')) {
            return;
        }

        $this->loadUpload([
            'upload_path' => $this->file_path,
            'allowed_types' => 'png'
        ]);

        if ($this->upload->do_upload('company_logo')) {
            $data['company_logo'] = $this->file_path . $this->upload->data('file_name');

            $this->CompanyModel->update($data, $this->company_id);

            $this->successSaveData('Upload Successfully');
        } else {
            $this->setFlashToastr('error', $this->upload->display_errors('', ''));
        }
    }

    protected function companyProfileValidation(): void
    {
        if (!isPost('save_profile_btn')) {
            return;
        }

        if ($this->form_validation->setRulesData('company_id', $this->company_id)->run()) {
            $data = $this->input->post([
                'company_name', 'company_email', 'phone_number',
                'incorporate_date', 'address', 'setting_state_id', 'other_info'
            ], TRUE);

            $data['incorporate_date'] = (Carbon::createFromFormat('d-m-Y', $data['incorporate_date']))->format('Y-m-d');
            $data['is_new'] = FALSE;
            $this->CompanyModel->update($data, $this->company_id);

            $this->successSaveData();
        }
    }

    public function inviteUser(): void
    {
        $this->checkAjax();

        if ($this->form_validation->run()) {
            $data = $this->input->post(['name', 'email'], TRUE);
            $data['user_type_id'] = USER_TYPE_EMPLOYER;
            $data['company_id'] = $this->company_id;
            $user_id = $this->UserModel->save($data);

            $this->EmailVerificationModel->insert($email_data = [
                'user_id' => $user_id,
                'type' => EMAIL_VERIFICATION_TYPE_ACTIVATION,
                'token' => random_string('md5')
            ]);

            $this->sendActivationEmail($data, $email_data);

            $this->successSaveData('Successfully invite user.');
        }

        $this->badRequest();
    }

    protected function sendActivationEmail(array $data, array $email_data): bool
    {
        $data['resend_btn'] = TRUE;
        $this->session->set_tempdata($data, TRUE, 600);

        return $this->sendEmail(
            [
                'subject' => EMAIL_VERIFICATION_LIST[EMAIL_VERIFICATION_TYPE_ACTIVATION],
                'to' => $data['email']
            ],
            'activation',
            [
                '{__TITLE__}' => $this->config->item('title'),
                '{__NAME__}' => $data['name'],
                '{__LINK__}' => site_url("verify_email?user_id={$email_data['user_id']}&token={$email_data['token']}")
            ]
        );
    }
}

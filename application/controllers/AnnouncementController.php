<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AnnouncementController extends AuthController
{
    protected array $allow_user_type = [USER_TYPE_ADMINISTRATOR, USER_TYPE_STAFF];

    public string $active_menu = 'announcement';
    public string $link = 'announcement/';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['AnnouncementModel', 'AnnouncementUserTypeRelModel', 'UserTypeModel']);
    }

    public function index(?int $announcement_id = NULL): void
    {
        $data['data'] = $announcement_id
            ? $this->AnnouncementModel->select('id, title, content, publish, publish_to_outside')->row($announcement_id)
            : NULL;

        $this->checkData($announcement_id, $data['data']);
        $this->formValidation($announcement_id);

        $data['tab'] = ($announcement_id || isPost('save_btn')) ? 2 : 1;
        $data['user_type_list'] = $this->UserTypeModel->result();
        $data['content'] = $this->load->view('announcement/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    public function getList(): void
    {
        $this->checkAjax();

        $output = $this->AnnouncementModel->datatable($this->input->get(['draw', 'columns', 'order', 'length', 'start', 'search'], TRUE))
            ->dtSelect('id')
            ->softDelete()
            ->dtGet()
            ->map(function ($row) {
                $row->action = '';

                $row->action .= '<a href="' . $this->site_url . $row->id . '" class="btn btn-xs btn-info title-tooltip" title="Edit"><span class="fas fa-edit"></span></a>&nbsp;';

                $row->action .= '<a href="' . $this->site_url . $row->id . '" class="btn btn-xs btn-danger list-delete-btn title-tooltip" title="Edit"><span class="fas fa-trash-alt"></span></a>&nbsp;';

                return $row;
            })
            ->toArray();

        $this->returnResponse($output);
    }

    protected function formValidation($announcement_id): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $data = $this->input->post(['title', 'publish', 'publish_to_outside', 'user_type_id'], TRUE);
            $data['content'] = $this->input->post('content');

            $user_type_id_list = (array) unsetReturn($data, 'user_type_id');
            $data['publish'] = $data['publish'] ?? 0;
            $data['publish_to_outside'] = $data['publish_to_outside'] ?? 0;
            $announcement_id = $this->AnnouncementModel->save($data, $announcement_id);

            $this->AnnouncementUserTypeRelModel->delete(['announcement_id' => $announcement_id]);
            foreach ($user_type_id_list as $user_type_id) {
                $this->AnnouncementUserTypeRelModel->insert([
                    'announcement_id' => $announcement_id,
                    'user_type_id' => $user_type_id
                ]);
            }

            $this->successSaveData();
        }
    }

    public function delete($announcement_id): void
    {
        $this->checkData($announcement_id, $this->AnnouncementModel->dataSeek($announcement_id));

        $this->AnnouncementModel->delete($announcement_id);
        $this->AnnouncementUserTypeRelModel->delete(['announcement_id' => $announcement_id]);

        $this->successDeleteData();
    }
}

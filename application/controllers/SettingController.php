<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SettingController extends AuthController
{
    protected array $allow_user_type = [USER_TYPE_ADMINISTRATOR, USER_TYPE_STAFF];

    public string $active_menu = 'setting';
    public string $link = 'setting/';
    public string $setting_tab = '';

    public function setting(): void
    {
        $data['content'] = $this->load->view('setting/template', NULL, TRUE);
        $this->load->view('template/inner', $data);
    }
}

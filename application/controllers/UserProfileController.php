<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class UserProfileController extends AuthController
{
    protected array $allow_user_type = [USER_TYPE_ADMINISTRATOR, USER_TYPE_STAFF, USER_TYPE_EMPLOYER];
    protected $user_id;
    public string $link = 'user_profile/';
    protected string $file_path;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string')
            ->model(['EmailVerificationModel', 'SettingStateModel', 'UserModel']);
        $this->user_id = $this->session->user_id;
        $this->file_path = 'uploads' . DIRECTORY_SEPARATOR . 'user' . DIRECTORY_SEPARATOR .  $this->user_id . DIRECTORY_SEPARATOR;
    }

    public function index()
    {
        $this->profilePictureValidation();
        $this->profileValidation();
        $this->emailValidation();
        $this->resendChangeEmailValidation();
        $this->passwordValidation();

        $data['data'] = $this->UserModel->getData($this->user_id);
        $data['data']->date_of_birth = (Carbon::createFromFormat('Y-m-d', $data['data']->date_of_birth ?? date('Y-m-d')))->format('d-m-Y');
        $data['state_list'] = $this->SettingStateModel->getList();
        $data['content'] = $this->load->view('user_profile/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    protected function profilePictureValidation(): void
    {
        if (!isPost('save_picture_btn')) {
            return;
        }

        $this->loadUpload([
            'upload_path' => $this->file_path,
            'allowed_types' => 'jpg|jpeg'
        ]);

        if ($this->upload->do_upload('profile_picture')) {
            $data['profile_pic'] = $this->file_path . $this->upload->data('file_name');

            $this->UserModel->update($data, $this->user_id);

            $this->successSaveData('Upload Successfully');
        } else {
            $this->setFlashToastr('error', $this->upload->display_errors());
        }
    }

    protected function profileValidation(): void
    {
        if (!isPost('save_profile_btn')) {
            return;
        }

        if ($this->form_validation->run('profile')) {
            $data = $this->input->post([
                'name', 'identity_number', 'gender', 'phone_number',
                'date_of_birth', 'address', 'setting_state_id'
            ], TRUE);

            $data['date_of_birth'] = (Carbon::createFromFormat('d-m-Y', $data['date_of_birth']))->format('Y-m-d');
            $this->UserModel->update($data, $this->user_id);

            $this->successSaveData();
        }
    }

    protected function emailValidation(): void
    {
        if (!isPost('save_email_btn')) {
            return;
        }

        if ($this->form_validation->run('emailPassword')) {
            $data = $this->input->post(['email'], TRUE);

            $this->EmailVerificationModel->insert([
                'user_id' => $this->user_id,
                'type' => EMAIL_VERIFICATION_TYPE_CHANGE_EMAIL,
                'token' => random_string('md5'),
                'data' => json_encode($data)
            ]);
            $this->sendChangeEmail($data['email']);
            $this->successSaveData('Please check your email to verify the new email. If you still not received the email, please click resend button to resend the email.');
        }
    }

    protected function resendChangeEmailValidation(): void
    {
        if (!isPost('resend_btn')) {
            return;
        }

        if ($this->form_validation->run('email')) {
            $data = $this->input->post(['email'], TRUE);
            $this->EmailVerificationModel->update(['data' => json_encode($data)], ['user_id' => $this->user_id]);

            $this->sendChangeEmail($data['email']);
        }
    }

    protected function sendChangeEmail($email)
    {
        $data = $this->EmailVerificationModel->getEmailData(['a.user_id' => $this->user_id]);
        $data['resend_btn'] = TRUE;
        $data['email'] = $email;
        $this->session->set_tempdata($data, TRUE, 600);

        return $this->sendEmail(
            [
                'subject' => EMAIL_VERIFICATION_LIST[EMAIL_VERIFICATION_TYPE_CHANGE_EMAIL],
                'to' => $email
            ],
            'changeEmail',
            [
                '{__TITLE__}' => $this->config->item('title'),
                '{__NAME__}' => $data['name'],
                '{__LINK__}' => site_url("verify_email?user_id={$this->user_id}&token={$data['token']}")
            ]
        );
    }

    protected function passwordValidation(): void
    {
        if (!isPost('save_password_btn')) {
            return;
        }

        if ($this->form_validation->run('password')) {
            $this->UserModel->update(['password' => $this->password->hash($this->input->post('new_password', TRUE))], $this->user_id);

            $this->successSaveData('Successfully change password');
        }
    }

    public function checkCurrentPassword($password): bool
    {
        $hash_password = $this->UserModel->getPassword(['id' => $this->user_id]);

        if ($this->password->verify($password, $hash_password)) {
            return TRUE;
        }

        $this->form_validation->set_message(__FUNCTION__, 'Incorrect Password.');
        return FALSE;
    }
}

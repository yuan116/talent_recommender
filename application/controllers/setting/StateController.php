<?php

defined('BASEPATH') or exit('No direct script access allowed');

class StateController extends SettingController
{
    public string $link = 'setting/state/';
    public string $setting_tab = 'state';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['SettingStateModel']);
    }

    public function index(?int $state_id = NULL): void
    {
        $data['data'] = $state_id ? $this->SettingStateModel->select('id, description')->row($state_id) : NULL;

        $this->checkData($state_id, $data['data']);
        $this->formValidation($state_id);

        $data['tab'] = ($state_id || isPost('save_btn')) ? 2 : 1;
        $data['state_list'] = $this->SettingStateModel->getList();
        $data['content'] = $this->load->view('setting/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    protected function formValidation($state_id): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $state_id = $this->SettingStateModel->save($this->input->post(['description'], TRUE), $state_id);

            $this->successSaveData();
        }
    }

    public function delete($state_id): void
    {
        $this->checkData($state_id, $this->SettingStateModel->dataSeek($state_id));

        $this->SettingStateModel->delete($state_id);

        $this->successDeleteData();
    }
}

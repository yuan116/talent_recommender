<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FieldOfStudyController extends SettingController
{
    public string $link = 'setting/field_of_study/';
    public string $setting_tab = 'field_of_study';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['SettingFieldOfStudyModel', 'SettingFieldOfStudySkillRelModel', 'SettingSkillModel']);
    }

    public function index(?int $field_of_study_id = NULL): void
    {
        $data['data'] = $field_of_study_id ? $this->SettingFieldOfStudyModel->select('id, description')->row($field_of_study_id) : NULL;
        $data['skill_data'] = $this->SettingFieldOfStudySkillRelModel->getValuePairList('', 'setting_skill_id', ['setting_field_of_study_id' => $field_of_study_id]);

        $this->checkData($field_of_study_id, $data['data']);
        $this->formValidation($field_of_study_id);

        $data['tab'] = ($field_of_study_id || isPost('save_btn')) ? 2 : 1;
        $data['field_of_study_list'] = $this->SettingFieldOfStudyModel->getList();
        $data['skill_list'] = $this->SettingSkillModel->getList();
        $data['content'] = $this->load->view('setting/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    protected function formValidation($field_of_study_id): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $field_of_study_id = $this->SettingFieldOfStudyModel->save($this->input->post(['description'], TRUE), $field_of_study_id);

            $this->SettingFieldOfStudySkillRelModel->delete(['setting_field_of_study_id' => $field_of_study_id]);
            $setting_skill_list = (array) $this->input->post('setting_skill_id', TRUE);
            foreach ($setting_skill_list as $skill_id) {
                $this->SettingFieldOfStudySkillRelModel->insert([
                    'setting_field_of_study_id' => $field_of_study_id,
                    'setting_skill_id' => $skill_id
                ]);
            }

            $this->successSaveData();
        }
    }

    public function delete($field_of_study_id): void
    {
        $this->checkData($field_of_study_id, $this->SettingFieldOfStudyModel->dataSeek($field_of_study_id));

        $this->SettingFieldOfStudyModel->delete($field_of_study_id);
        $this->SettingFieldOfStudySkillRelModel->delete(['setting_field_of_study_id' => $field_of_study_id]);

        $this->successDeleteData();
    }
}

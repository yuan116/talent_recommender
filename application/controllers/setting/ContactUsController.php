<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ContactUsController extends SettingController
{
    public string $link = 'setting/contact_us/';
    public string $setting_tab = 'contact_us';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['SettingContactUsModel']);
    }

    public function index(): void
    {
        $this->formValidation();

        $data['data'] = $this->SettingContactUsModel->getValuePairList('key', 'value');
        $data['content'] = $this->load->view('setting/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    protected function formValidation(): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            foreach ($this->input->post(['phone_number', 'email', 'address', 'address_iframe'], TRUE) as $key => $value) {
                $key_data = ['key' => $key];
                $value_data = ['value' => $value];
                if ($this->SettingContactUsModel->dataSeek($key_data)) {
                    $this->SettingContactUsModel->update($value_data, $key_data);
                } else {
                    $this->SettingContactUsModel->insert($key_data + $value_data);
                }
            }

            $this->successSaveData();
        }
    }
}

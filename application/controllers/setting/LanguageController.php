<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LanguageController extends SettingController
{
    public string $link = 'setting/language/';
    public string $setting_tab = 'language';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['SettingLanguageModel']);
    }

    public function index(?int $language_id = NULL): void
    {
        $data['data'] = $language_id ? $this->SettingLanguageModel->select('id, description')->row($language_id) : NULL;

        $this->checkData($language_id, $data['data']);
        $this->formValidation($language_id);

        $data['tab'] = ($language_id || isPost('save_btn')) ? 2 : 1;
        $data['state_list'] = $this->SettingLanguageModel->getList();
        $data['content'] = $this->load->view('setting/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    protected function formValidation($language_id): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $language_id = $this->SettingLanguageModel->save($this->input->post(['description'], TRUE), $language_id);

            $this->successSaveData();
        }
    }

    public function delete($language_id): void
    {
        $this->checkData($language_id, $this->SettingLanguageModel->dataSeek($language_id));

        $this->SettingLanguageModel->delete($language_id);

        $this->successDeleteData();
    }
}

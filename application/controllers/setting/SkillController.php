<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SkillController extends SettingController
{
    public string $link = 'setting/skill/';
    public string $setting_tab = 'skill';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['SettingFieldOfStudyModel', 'SettingFieldOfStudySkillRelModel', 'SettingSkillModel']);
    }

    public function index(?int $skill_id = NULL): void
    {
        $data['data'] = $skill_id ? $this->SettingSkillModel->select('id, description')->row($skill_id) : NULL;
        $data['field_of_study_data'] = $this->SettingFieldOfStudySkillRelModel->getValuePairList('', 'setting_field_of_study_id', ['setting_skill_id' => $skill_id]);

        $this->checkData($skill_id, $data['data']);
        $this->formValidation($skill_id);

        $data['tab'] = ($skill_id || isPost('save_btn')) ? 2 : 1;
        $data['field_of_study_list'] = $this->SettingFieldOfStudyModel->getList();
        $data['skill_list'] = $this->SettingSkillModel->getList();
        $data['content'] = $this->load->view('setting/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    protected function formValidation($skill_id): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->run()) {
            $skill_id = $this->SettingSkillModel->save($this->input->post(['description'], TRUE), $skill_id);

            $this->SettingFieldOfStudySkillRelModel->delete(['setting_skill_id' => $skill_id]);
            $setting_field_of_study_list = (array) $this->input->post('setting_field_of_study_id', TRUE);
            foreach ($setting_field_of_study_list as $field_of_study_id) {
                $this->SettingFieldOfStudySkillRelModel->insert([
                    'setting_field_of_study_id' => $field_of_study_id,
                    'setting_skill_id' => $skill_id
                ]);
            }

            $this->successSaveData();
        }
    }

    public function delete($skill_id): void
    {
        $this->checkData($skill_id, $this->SettingSkillModel->dataSeek($skill_id));

        $this->SettingSkillModel->delete($skill_id);

        $this->successDeleteData();
    }
}

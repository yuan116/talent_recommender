<?php

defined('BASEPATH') or exit('No direct script access allowed');

class LogController extends \Yuan116\Ci3\Enhance\Controllers\LogViewerController
{
    protected bool $auth = FALSE;
    protected int $auth_timeout = 600; //10 minutes
    public string $route_url = 'support_log';
}

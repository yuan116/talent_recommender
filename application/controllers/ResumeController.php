<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Rubix\ML\Classifiers\KDNeighbors;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Graph\Trees\KDTree;
use Rubix\ML\Kernels\Distance\Euclidean;
use Smalot\PdfParser\Parser;
use voku\helper\StopWords;
use Wamania\Snowball\StemmerFactory;

class ResumeController extends AuthController
{
    protected array $allow_user_type = [USER_TYPE_EMPLOYER];
    protected $company_id;
    protected Parser $parser;

    public string $active_menu = 'user';
    public string $link = 'resume/';
    public string $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('text')
            ->model([
                'ResumeLanguageRelModel', 'ResumeModel', 'ResumeSettingSkillRelModel',
                'SettingFieldOfStudyModel', 'SettingFieldOfStudySkillRelModel', 'SettingLanguageModel', 'SettingSkillModel'
            ]);
        $this->company_id = $this->session->company_id;
        $this->file_path = 'uploads' . DIRECTORY_SEPARATOR . 'company' . DIRECTORY_SEPARATOR .  $this->company_id . DIRECTORY_SEPARATOR;
        $this->parser = new Parser();
    }

    public function index(): void
    {
        $data['field_of_study_list'] = $this->SettingFieldOfStudyModel->getValuePairList('id', 'description');

        $data['skill_list'] = [];
        foreach ($data['field_of_study_list'] as $field_of_study_id => $field_of_study_description) {
            $data['skill_list'][$field_of_study_id] = $this->SettingSkillModel->getListByFieldOfStudy($field_of_study_id);
        }

        $data['language_list'] = $this->SettingLanguageModel->getList();
        $data['content'] = $this->load->view('resume/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    public function getList(): void
    {
        $this->checkAjax();

        $get = $this->input->get([
            'draw', 'columns', 'order', 'length', 'start', 'search',
            'cgpa', 'setting_field_of_study_id', 'setting_skill_id', 'setting_language_id', 'has_referee', 'filter_button_click', 'resume_id'
        ], TRUE);

        $where = [
            'a.company_id' => $this->company_id
        ];
        $where_in = [];
        $order_by = [];

        if ($get['filter_button_click'] === 'true') {
            if (!empty($get['cgpa'])) {
                $cgpa = explode(',', $get['cgpa']);
                if ($cgpa[0] != 2 || $cgpa[1] != 4) {
                    $where['a.cgpa >='] = $cgpa[0];
                    $where['a.cgpa <='] = $cgpa[1];
                }
            }

            if (!empty($get['setting_field_of_study_id'])) {
                $where_in['a.setting_field_of_study_id'] = $get['setting_field_of_study_id'];
            }

            if (!empty($get['setting_skill_id'])) {
                $where_in['c.setting_skill_id'] = array_unique($get['setting_skill_id']);
                $order_by[] = 'COUNT(DISTINCT c.setting_skill_id)';
            }

            if (!empty($get['setting_language_id'])) {
                $where_in['d.setting_language_id'] = $get['setting_language_id'];
                $order_by[] = 'COUNT(DISTINCT d.setting_language_id)';
            }

            if ($get['has_referee'] === 'true') {
                $where['JSON_EXTRACT(a.data, "$.referee") = 1'] = NULL;
            }
        }

        $datatable = $this->ResumeModel->datatable($get)
            ->dtSelect('a.id, a.filepath')
            ->dtFrom($this->ResumeModel->getTable() . ' AS a')
            ->dtJoin($this->SettingFieldOfStudyModel->getTable() . ' AS b', 'a.setting_field_of_study_id = b.' . $this->SettingFieldOfStudyModel->getPrimaryKey(), 'left')
            ->dtJoin($this->ResumeSettingSkillRelModel->getTable() . ' AS c', 'a.' . $this->ResumeModel->getPrimaryKey() . ' = c.resume_id', 'left')
            ->dtJoin($this->ResumeLanguageRelModel->getTable() . ' AS d', 'a.' . $this->ResumeModel->getPrimaryKey() . ' = d.resume_id', 'left')
            ->dtWhere($where)
            ->dtGroupBy('a.id');

        foreach ($this->SettingFieldOfStudyModel->getList() as $row) {
            $datatable->dtSelect('ROUND(IFNULL(a.data->>\'$.weightage."' . $row->id . '"\', 0), 2) AS ' . str_replace(' ', '', strtolower($row->description)) . '_weightage', FALSE);
        }

        foreach ($where_in as $key => $value) {
            $datatable->dtWhereIn($key, $value);
        }

        foreach ($order_by as $column) {
            $datatable->dtPrependOrderBy($column, 'desc');
        }

        $get['resume_id'] = (array) $get['resume_id'];

        $output = $datatable->softDelete('a')->dtGet()
            ->map(function ($row) use ($get) {
                $check = '';
                if (in_array($row->id, $get['resume_id'])) {
                    $check = 'checked';
                }
                $row->checkbox = "<input type='checkbox' class='resume_checkbox' name='resume_id[]' value='{$row->id}' {$check} />";

                $row->action = '';

                $row->action .= '<a href="' . $this->base_url . $row->filepath . '" class="btn btn-xs btn-info title-tooltip" title="Preview" target="_blank"><span class="fas fa-external-link-alt"></span></a>&nbsp;';

                $row->action .= '<a href="' . $this->base_url . $row->filepath . '" class="btn btn-xs btn-info title-tooltip" title="Download" download="' . $row->filename . '"><span class="fas fa-download"></span></a>&nbsp;';

                $row->action .= '<a href="#" class="btn btn-xs btn-info title-tooltip" title="Edit" onclick="edit(' . $row->id . ', \'' . $row->filename . '\');"><span class="fas fa-edit"></span></a>&nbsp;';

                $row->action .= '<a href="' . $this->site_url . $row->id . '" class="btn btn-xs btn-danger list-delete-btn title-tooltip" title="Delete"><span class="fas fa-trash-alt"></span></a>';

                return $row;
            })
            ->toArray();

        $this->returnResponse($output);
    }

    public function update(): void
    {
        $data = $this->input->post(['id', 'filename'], TRUE);
        $resume_id = unsetReturn($data, 'id');

        $file_data = $this->ResumeModel->select('company_id')->row($resume_id);
        $this->checkData($resume_id, $file_data);

        if ($file_data->company_id != $this->company_id) {
            $this->forbiddenAccess('Cannot edit other company\'s file.');
        }

        $this->ResumeModel->save($data, $resume_id);

        $this->successSaveData();
    }

    public function upload(): void
    {
        $this->checkAjax();

        $this->loadUpload([
            'upload_path' => $this->file_path,
            'allowed_types' => 'pdf'
        ]);

        if ($this->upload->do_upload('file')) {
            $data = $this->upload->data(['file_name', 'orig_name', 'file_ext']);
            $data['filename'] = str_ireplace($data['file_ext'], '', unsetReturn($data, 'orig_name'));
            $data['filepath'] = $this->file_path . unsetReturn($data, 'file_name');
            unset($data['file_ext']);

            $data['company_id'] = $this->company_id;
            $resume_id = $this->ResumeModel->save($data);

            $this->setResumeData($resume_id);

            $this->successSaveData('Upload Successfully');
        }

        $this->returnResponse(['message' => $this->upload->display_errors('', '')]);
    }

    protected function setResumeData($resume_id): void
    {
        $filepath = $this->ResumeModel->select('filepath')->row($resume_id)->filepath;
        $pdf = $this->parser->parseFile(FCPATH . $filepath);
        $resume_content = trim(preg_replace('/\t+/', '', $pdf->getText()));
        $field_of_study_list = $this->SettingFieldOfStudyModel->getValuePairList('id', 'description');
        $language_list = $this->SettingLanguageModel->getList();
        $match_skill_list = [];
        $skill_list = [];
        $total_match_skill = 0;
        $cgpa_match_list = [];
        $match_language_list = [];
        $has_referee = FALSE;

        $this->getResumeData($resume_content, $field_of_study_list, $match_skill_list, $skill_list, $total_match_skill, $cgpa_match_list, $has_referee);
        $this->getLanguageData($resume_content, $language_list, $match_language_list);

        //rearrange the content and search again, because of some pdf encoding issue
        $resume_content = $this->beautifyContent($pdf->getText());
        $this->getResumeData($resume_content, $field_of_study_list, $match_skill_list, $skill_list, $total_match_skill, $cgpa_match_list, $has_referee);
        $this->getLanguageData($resume_content, $language_list, $match_language_list);

        //remove all whitespaces and search again
        $resume_content = $this->removeStopWord($this->stemContent($resume_content));
        $resume_content = trim(preg_replace('/\s+/', '', $resume_content));
        $this->getResumeData($resume_content, $field_of_study_list, $match_skill_list, $skill_list, $total_match_skill, $cgpa_match_list, $has_referee, FALSE);
        $this->getLanguageData($resume_content, $language_list, $match_language_list);

        $data = [
            'setting_field_of_study_id' => NULL,
            'cgpa' => $cgpa_match_list[0] ?? NULL,
            'data' => ['weightage' => [], 'referee' => intval($has_referee)]
        ];
        $skill_data = [];

        if (!empty($match_skill_list)) {
            //remove all duplicate values
            foreach ($match_skill_list as $skill_id_list) {
                $skill_data = array_merge($skill_data, $skill_id_list);
            }
            $skill_data = array_unique($skill_data);

            $classify_result = $this->getClassifyResult($field_of_study_list, $skill_data);

            //calculate weightage
            $total_predicted_result = array_sum($classify_result);
            foreach ($field_of_study_list as $field_of_study_id => $field_of_study_description) {
                if (array_key_exists($field_of_study_id, $classify_result)) {
                    $data['data']['weightage'][$field_of_study_id] = round($classify_result[$field_of_study_id] / $total_predicted_result * 100, 2);
                }
            }

            //get field of study
            $data['setting_field_of_study_id'] = array_keys($classify_result, max($classify_result))[0];
        }

        $data['data'] = json_encode($data['data']);
        $this->ResumeModel->update($data, $resume_id);

        foreach ($skill_data as $skill_id) {
            $this->ResumeSettingSkillRelModel->insert([
                'resume_id' => $resume_id,
                'setting_skill_id' => $skill_id
            ]);
        }

        foreach ($match_language_list as $language_id) {
            $this->ResumeLanguageRelModel->insert([
                'resume_id' => $resume_id,
                'setting_language_id' => $language_id
            ]);
        }
    }

    protected function getResumeData(
        string $resume_content,
        array $field_of_study_list,
        array &$match_skill_list,
        array &$skill_list,
        int &$total_match_skill,
        array &$cgpa_match_list,
        bool &$has_referee,
        bool $sensitive_check = TRUE
    ): void {
        foreach ($field_of_study_list as $field_of_study_id => $field_of_study_description) {
            $match_skill_list[$field_of_study_id] = $match_skill_list[$field_of_study_id] ?? [];
            $skill_list[$field_of_study_id] = $skill_list[$field_of_study_id] ?? $this->SettingSkillModel->getListByFieldOfStudy($field_of_study_id);

            foreach ($skill_list[$field_of_study_id] as $skill_id => $skill_description) {
                $str_length = strlen($skill_description);

                if ($sensitive_check) {
                    if ($str_length < 3) {
                        $skill_description = " {$skill_description} ";
                    }

                    if (str_contains($resume_content, $skill_description)) {
                        $match_skill_list[$field_of_study_id][] = $skill_id;
                        unset($skill_list[$field_of_study_id][$skill_id]);
                        $total_match_skill++;
                    }
                } else {
                    if ($str_length > 3) {
                        $skill_description = preg_replace('/\s+/', '', $skill_description);

                        if (stripos($resume_content, $skill_description) !== FALSE) {
                            $match_skill_list[$field_of_study_id][] = $skill_id;
                            unset($skill_list[$field_of_study_id][$skill_id]);
                            $total_match_skill++;
                        }
                    }
                }
            }

            //Remove the index of array if the specific index is empty value
            if (empty($match_skill_list[$field_of_study_id])) {
                unset($match_skill_list[$field_of_study_id]);
            }
        }

        if (empty($cgpa_match_list)) {
            preg_match('/[2-4]\.\d{1,2}/', $resume_content, $cgpa_match_list, PREG_UNMATCHED_AS_NULL);
        }

        if (!$has_referee) {
            $has_referee = str_contains($resume_content, 'referee');
        }

        if (!$has_referee) {
            $has_referee = str_contains($resume_content, 'reference');
        }
    }

    protected function getLanguageData(string $resume_content, array &$langauge_list, array &$match_language_list): void
    {
        foreach ($langauge_list as $index => $langauge_row) {
            if (str_contains($resume_content, $langauge_row->description)) {
                $match_language_list[] = $langauge_row->id;
                unset($langauge_list[$index]);
            }
        }
    }

    public function compare(): void
    {
        $resume_id = $this->input->get('resume_id', TRUE);

        $resume_list = $this->ResumeModel->select('filepath')->whereIn($resume_id)->result(['company_id' => $this->company_id]);

        if (count($resume_list) != 2) {
            $this->forbiddenAccess('Cannot compare other company\'s file');
        }

        $beautify_content = [];

        foreach ($resume_list as $index => $row) {
            $content = $this->parser->parseFile(FCPATH . $row->filepath)->getText();
            $beautify_content[$index] = explode(' ', $this->beautifyContent($content, TRUE));
            $beautify_content[$index] = array_filter($beautify_content[$index], 'trim');
        }

        $intersect_content = array_intersect(...$beautify_content);
        $intersect_content = array_unique($intersect_content);

        $this->returnResponse(['data' => $intersect_content]);
    }

    protected function beautifyContent(string $content, bool $remove_non_char = FALSE): string
    {
        //remove html entities
        $content = preg_replace('/\[|(&.+;)/u', '', htmlentities(remove_invisible_characters($content)));
        //remove '|', utf8_decode for decode unicode character (possbile icon issue)
        $content = preg_replace('/\|/u', '', utf8_decode(utf8_decode($content)));

        if ($remove_non_char) {
            //remove all numeric, punctuation
            $content = preg_replace('/\d|\p{P}/u', '', $content);
        }

        $tmp_content_array = explode("\n", $content);

        $content_array = [];
        foreach ($tmp_content_array as $content) {
            $content_array = array_merge($content_array, preg_split('/\s\s+/', $content));
        }

        $content_array = array_filter(array_map(function ($value) {
            $value = trim(preg_replace('/\s+/u', '', $value));
            $value_array = preg_split('/(^[a-z]+|[A-Z][^A-Z]+)/u', $value, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

            return trim(implode(' ', $value_array));
        }, $content_array));

        return implode(' ', $content_array);
    }

    protected function stemContent(string $content): string
    {
        return StemmerFactory::create('english')->stem($content);
    }

    protected function removeStopWord(string $content): string
    {
        $stop_word_list = (new StopWords())->getStopWordsFromLanguage('en');

        $content_list = explode(' ', $content);
        $tmp_content_list = [];
        foreach ($content_list as $text) {
            if (!in_array($text, $stop_word_list)) {
                $tmp_content_list[] = $text;
            }
        }

        return implode(' ', $tmp_content_list);
    }

    protected function getClassifyResult(array $field_of_study_list, array $match_skill_list): array
    {
        $train_dataset = [];
        foreach ($field_of_study_list as $field_of_study_id => $field_of_study_description) {
            $skill_list = $this->SettingFieldOfStudySkillRelModel->getValuePairList('', 'setting_skill_id', ['setting_field_of_study_id' => $field_of_study_id]);
            //Convert all the values to int and make everything in array size of 2
            $skill_list = array_map('intval', $skill_list);
            foreach (array_chunk($skill_list, 2) as $list) {
                if (count($list) < 2) {
                    $list[] = 0;
                }

                $train_dataset[] = [
                    'label_field_description' => $field_of_study_description,
                    'sample_skill_id' => $list
                ];
            }
        }

        $predict_dataset = [];
        //Convert all the values to int and make everything in array size of 2
        $match_skill_list = array_map('intval', $match_skill_list);
        foreach (array_chunk($match_skill_list, 2) as $list) {
            if (count($list) < 2) {
                $list[] = 0;
            }

            $predict_dataset[] = $list;
        }

        $label_dataset = new Labeled(array_column($train_dataset, 'sample_skill_id'), array_column($train_dataset, 'label_field_description'));
        $unlabel_dataset = new Unlabeled($predict_dataset);

        $result = [];
        for ($count = 1; $count <= 6; $count++) {
            $estimator = new KDNeighbors($count, TRUE, new KDTree(30, new Euclidean()));
            $estimator->train($label_dataset);
            $predict_result = $estimator->predict($unlabel_dataset);
            $proba_result = $estimator->proba($unlabel_dataset);
            foreach ($predict_result as $index => $field_of_study_description) {
                //only get accuracy more than 80%
                if ((round(max($proba_result[$index]), 2) * 100) <= 80) {
                    unset($predict_result[$index]);
                }
            }
            $result = array_merge($result, $predict_result);
        }

        /*
        array_count_values return Description and Value Pair result: 
        [
            'Management' => 5,
            'Information Technology' => 13,
            etc...
        ]
        */
        $field_description_result = array_count_values($result);
        //Convert Description to ID
        $field_id_result = [];
        foreach ($field_description_result as $field_of_study_description => $field_result) {
            if (in_array($field_of_study_description, $field_of_study_list)) {
                $field_id_result[array_search($field_of_study_description, $field_of_study_list)] = $field_result;
            }
        }

        return $field_id_result;
    }

    public function delete($resume_id): void
    {
        $data = $this->ResumeModel->select('company_id')->row($resume_id);
        $this->checkData($resume_id, $data);

        if ($data->company_id != $this->company_id) {
            $this->forbiddenAccess('Cannot delete other company\'s file.');
        }

        $this->ResumeModel->delete($resume_id);
        $this->ResumeSettingSkillRelModel->delete(['resume_id' => $resume_id]);

        $this->successDeleteData();
    }
}

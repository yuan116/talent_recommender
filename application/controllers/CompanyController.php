<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class CompanyController extends AuthController
{
    protected array $allow_user_type = [USER_TYPE_ADMINISTRATOR, USER_TYPE_STAFF];

    public string $active_menu = 'company';
    public string $link = 'company/';

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['CompanyModel', 'SettingStateModel', 'UserModel']);
    }

    public function index(?int $company_id = NULL): void
    {
        $data['data'] = $company_id ? $this->CompanyModel->getData($company_id) : NULL;

        $this->checkData($company_id, $data['data']);
        $this->formValidation($company_id);

        if (!empty($data['data'])) {
            $data['data']->incorporate_date = (Carbon::createFromFormat('Y-m-d', $data['data']->incorporate_date ?? date('Y-m-d')))->format('d-m-Y');
            $data['user_list'] = $this->UserModel->select('name, email, phone_number')->result(['company_id' => $company_id]);
        }

        $data['tab'] = ($company_id || isPost('save_btn')) ? 2 : 1;
        $data['state_list'] = $this->SettingStateModel->getList();
        $data['content'] = $this->load->view('company/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    public function getList(): void
    {
        $this->checkAjax();

        $get = $this->input->get(['draw', 'columns', 'order', 'length', 'start', 'search'], TRUE);

        $output = $this->CompanyModel->datatable($get)
            ->dtSelect('a.id')
            ->dtFrom($this->CompanyModel->getTable() . ' AS a')
            ->dtJoin($this->SettingStateModel->getTable() . ' AS b', 'a.setting_state_id = b.' . $this->SettingStateModel->getPrimaryKey(), 'left')
            ->softDelete('a')
            ->dtGet()
            ->map(function ($row) {
                $row->action = '';

                $row->action .= '<a href="' . $this->site_url . $row->id . '" class="btn btn-xs btn-info title-tooltip" title="Edit"><span class="fas fa-edit"></span></a>&nbsp;';

                $row->action .= '<a href="' . $this->site_url . $row->id . '" class="btn btn-xs btn-danger list-delete-btn title-tooltip" title="Edit"><span class="fas fa-trash-alt"></span></a>&nbsp;';

                return $row;
            })
            ->toArray();

        $this->returnResponse($output);
    }

    protected function formValidation($company_id): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->setRulesData('company_id', $company_id)->run()) {
            $data = $this->input->post([
                'company_name', 'company_email', 'phone_number',
                'incorporate_date', 'address', 'setting_state_id', 'other_info'
            ], TRUE);

            if (!empty($data['incorporate_date'])) {
                $data['incorporate_date'] = (Carbon::createFromFormat('d-m-Y', $data['incorporate_date']))->format('Y-m-d');
            }

            $data['company_name'] = strtoupper($data['company_name']);
            $data['is_new'] = FALSE;
            $company_id = $this->CompanyModel->save($data, $company_id);

            $this->successSaveData();
        }
    }

    public function delete($company_id): void
    {
        $this->checkData($company_id, $this->CompanyModel->dataSeek($company_id));

        $this->CompanyModel->delete($company_id);

        $this->successDeleteData();
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class UserController extends AuthController
{
    protected array $allow_user_type = [USER_TYPE_ADMINISTRATOR, USER_TYPE_STAFF];

    public string $active_menu = 'user';
    public string $link = 'user/';

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('string')
            ->model(['CompanyModel', 'EmailVerificationModel', 'SettingStateModel', 'UserModel', 'UserTypeModel']);
    }

    public function index(?int $user_id = NULL)
    {
        $data['data'] = $user_id ? $this->UserModel->getData($user_id) : NULL;

        $this->checkData($user_id, $data['data']);
        $this->formValidation($user_id, !empty($data['data']->company_id));

        if (!empty($data['data'])) {
            $data['data']->date_of_birth = (Carbon::createFromFormat('Y-m-d', $data['data']->date_of_birth ?? date('Y-m-d')))->format('d-m-Y');
            $data['company_list'] = $this->CompanyModel->select('id, company_name')->result();
        }

        $data['tab'] = ($user_id || isPost('save_btn')) ? 2 : 1;
        $data['state_list'] = $this->SettingStateModel->getList();
        $data['content'] = $this->load->view('user/template', $data, TRUE);
        $this->load->view('template/inner', $data);
    }

    public function getList()
    {
        $this->checkAjax();

        $output = $this->UserModel->datatable($this->input->get(['draw', 'columns', 'order', 'length', 'start', 'search'], TRUE))
            ->dtSelect('a.id, a.user_type_id')
            ->dtFrom($this->UserModel->getTable() . ' AS a')
            ->dtJoin($this->UserTypeModel->getTable() . ' AS b', 'a.user_type_id = b.' . $this->UserTypeModel->getPrimaryKey())
            ->dtJoin($this->SettingStateModel->getTable() . ' AS c', 'a.setting_state_id = c.' . $this->SettingStateModel->getPrimaryKey(), 'left')
            ->dtJoin($this->CompanyModel->getTable() . ' AS d', 'a.company_id = d.' . $this->CompanyModel->getPrimaryKey(), 'left')
            ->softDelete('a')
            ->dtGet()
            ->map(function ($row) {
                $row->action = '';

                if ($row->id != $this->session->user_id && $row->user_type_id != USER_TYPE_ADMINISTRATOR) {
                    $row->action .= '<a href="' . $this->site_url . $row->id . '" class="btn btn-xs btn-info title-tooltip" title="Edit"><span class="fas fa-edit"></span></a>&nbsp;';

                    $row->action .= '<a href="' . $this->site_url . $row->id . '" class="btn btn-xs btn-danger list-delete-btn title-tooltip" title="Delete"><span class="fas fa-trash-alt"></span></a>';
                }

                return $row;
            })
            ->toArray();

        $this->returnResponse($output);
    }

    protected function formValidation($user_id, bool $is_employer): void
    {
        if (!isPost('save_btn')) {
            return;
        }

        if ($this->form_validation->setRulesData('user_id', $user_id)->run($is_employer ? 'employer' : 'user')) {
            $data = $this->input->post([
                'name', 'email', 'identity_number', 'gender', 'phone_number',
                'date_of_birth', 'address', 'setting_state_id', 'company_id'
            ], TRUE);
            $is_new_user = empty($user_id);

            if ($is_new_user) {
                $data['user_type_id'] = USER_TYPE_STAFF;
            }

            $data['date_of_birth'] = (Carbon::createFromFormat('d-m-Y', $data['date_of_birth']))->format('Y-m-d');
            $user_id = $this->UserModel->save($data, $user_id);

            if ($is_new_user) {
                $token = random_string('md5');
                $this->EmailVerificationModel->insert($email_data = [
                    'user_id' => $user_id,
                    'type' => EMAIL_VERIFICATION_TYPE_ACTIVATION,
                    'token' => $token
                ]);
                $this->sendActivationEmail($data, $email_data);
            }

            $this->successSaveData();
        }
    }

    protected function sendActivationEmail(array $data, array $email_data): bool
    {
        return $this->sendEmail(
            [
                'subject' => EMAIL_VERIFICATION_LIST[EMAIL_VERIFICATION_TYPE_ACTIVATION],
                'to' => $data['email']
            ],
            'activation',
            [
                '{__TITLE__}' => $this->config->item('title'),
                '{__NAME__}' => $data['name'],
                '{__LINK__}' => site_url("verify_email?user_id={$email_data['user_id']}&token={$email_data['token']}")
            ]
        );
    }

    public function delete($user_id): void
    {
        $data = $this->UserModel->select('id, user_type_id')->row($user_id);
        $this->checkData($user_id, $data);

        if ($data->user_type_id == USER_TYPE_ADMINISTRATOR) {
            $this->forbiddenAccess('Cannot delete adminstrator account');
        }

        if ($data->id == $this->session->user_id) {
            $this->forbiddenAccess('Cannot delete yourself');
        }

        $this->UserModel->delete($user_id);

        $this->successDeleteData();
    }
}

<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Rubix\ML\Classifiers\KDNeighbors;
use Rubix\ML\Datasets\Labeled;
use Rubix\ML\Datasets\Unlabeled;
use Rubix\ML\Graph\Trees\KDTree;
use Rubix\ML\Kernels\Distance\Euclidean;
use Smalot\PdfParser\Parser;
use voku\helper\StopWords;
use Wamania\Snowball\StemmerFactory;

class Test extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['text']);
        $this->load->model(['SettingFieldOfStudyModel', 'SettingSkillModel', 'SettingFieldOfStudySkillRelModel', 'ResumeModel', 'ResumeSettingSkillRelModel']);
    }

    public function classify(int $id = NULL)
    {
        $field_list = $this->SettingFieldOfStudyModel->getValuePairList('id', 'description');
        $train_dataset = [];
        foreach ($field_list as $field_id => $field_description) {
            $skill_list = $this->SettingFieldOfStudySkillRelModel->getValuePairList('', 'setting_skill_id', ['setting_field_of_study_id' => $field_id]);
            $skill_list = array_map('intval', $skill_list);

            foreach (array_chunk($skill_list, 2) as $list) {
                if (count($list) < 2) {
                    $list[] = 0;
                }

                $train_dataset[] = [
                    'label_field_description' => $field_description,
                    'sample_skill_id' => $list
                ];
            }
        }

        $resume_skill_list = $this->ResumeSettingSkillRelModel->getValuePairList('', 'setting_skill_id', ['resume_id' => $id]);
        if (empty($resume_skill_list)) {
            throw new RuntimeException('Unknown. Unable to predict field.');
        }

        $predict_dataset = [];
        $resume_skill_list = array_map('intval', $resume_skill_list);
        foreach (array_chunk($resume_skill_list, 2) as $list) {
            if (count($list) < 2) {
                $list[] = 0;
            }

            $predict_dataset[] = $list;
        }

        $label_dataset = new Labeled(array_column($train_dataset, 'sample_skill_id'), array_column($train_dataset, 'label_field_description'));
        $unlabel_dataset = new Unlabeled($predict_dataset);

        $result = [];
        $finalize_result = [];
        for ($count = 1; $count <= 6; $count++) {
            $estimator = new KDNeighbors($count, TRUE, new KDTree(30, new Euclidean()));
            $estimator->train($label_dataset);

            $predict_result = $estimator->predict($unlabel_dataset);
            $tmp_proba_result = $estimator->proba($unlabel_dataset);
            foreach ($predict_result as $index => $field_description) {
                if ((round(max($tmp_proba_result[$index]), 2) * 100) <= 80) {
                    unset($predict_result[$index]);
                }
            }

            $result = array_merge($result, $predict_result);

            foreach ($predict_result as $index => $field_description) {
                $finalize_result["k = {$count}"][$index] = "{$field_description} : " . (round(max($tmp_proba_result[$index]), 2) * 100) . '%';
            }
        }

        $field_description_result = array_count_values($result);
        $field_id_result = [];
        foreach ($field_description_result as $field_description => $field_result) {
            if (in_array($field_description, $field_list)) {
                $field_id_result[array_search($field_description, $field_list)] = $field_result;
            }
        }

        echo '<pre>';
        echo '<br />Field Description Result : <br />';
        print_r($field_description_result);

        echo '<br />Field ID Result : <br />';
        print_r($field_id_result);

        echo '<br />Field Result With K & Probability : <br />';
        print_r($finalize_result);
    }

    public function content(int $id = NULL)
    {
        $filepath = @$this->ResumeModel->row(['id' => $id])->filepath;

        if (empty($filepath)) {
            throw new RuntimeException('No data found.');
        }

        $file = FCPATH . $filepath;

        echo '<pre>';
        $resume_content = (new Parser())->parseFile($file)->getText();

        echo 'Original Content:';
        echo '<br>---<br>';
        echo $resume_content;
        echo '<br>---<br>';

        $resume_content = $this->beautifyContent($resume_content);
        echo 'Beautify Content:';
        echo '<br>---<br>';
        echo $resume_content;
        echo '<br>---<br>';

        $resume_content = StemmerFactory::create('english')->stem($resume_content);
        $stop_word_list = (new StopWords())->getStopWordsFromLanguage('en');

        $resume_content_array = explode(' ', $resume_content);
        $tmp_content_array = [];
        foreach ($resume_content_array as $content) {
            if (!in_array($content, $stop_word_list)) {
                $tmp_content_array[] = $content;
            }
        }

        $resume_content = preg_replace('/\s/', '', implode(' ', $tmp_content_array));
        echo 'Stem, Remove Stop Word and whitespaces:';
        echo '<br>---<br>';
        echo $resume_content;
        echo '<br>---<br>';
    }

    protected function beautifyContent(string $content, bool $remove_non_char = FALSE): string
    {
        //remove html entities
        $content = preg_replace('/\[|(&.+;)/u', '', htmlentities(remove_invisible_characters($content)));
        //remove '|', utf8_decode for decode unicode character (possbile icon issue)
        $content = preg_replace('/\|/u', '', utf8_decode(utf8_decode($content)));

        if ($remove_non_char) {
            //remove all numeric, punctuation
            $content = preg_replace('/\d|\p{P}/u', '', $content);
        }

        $tmp_content_array = explode("\n", $content);

        $content_array = [];
        foreach ($tmp_content_array as $content) {
            $content_array = array_merge($content_array, preg_split('/\s\s+/', $content));
        }

        $content_array = array_filter(array_map(function ($value) {
            $value = trim(preg_replace('/\s+/u', '', $value));
            $value_array = preg_split('/(^[a-z]+|[A-Z][^A-Z]+)/u', $value, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

            return trim(implode(' ', $value_array));
        }, $content_array));

        return implode(' ', $content_array);
    }
}
